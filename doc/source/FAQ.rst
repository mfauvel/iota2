FAQ
===


Facing the "Too many open files" error
--------------------------------------

iota2 may open a lot of files at the same time to complete a run. With some distribution configuration, the error ``Too many open files`` the solution is to increase the number of files that can be opened at the same time thanks to the command :

.. code-block:: bash

    ulimit -u #inform you about the current limit (ie : 4096)
    ulimit -u 5000 # increase user limit to 5000

More information at : https://linuxhint.com/permanently_set_ulimit_value/
