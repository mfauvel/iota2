Disk full of space
##################

First run
*********

iota2 requires original image data for the first run as mandatory information is extracted from them.

Like:
- original projection
- original resolution
- ...

Next run
********

iota2 can use already preprocessed data.
This requires that interpolation parameters not changes: same spectral resolution, same projection.
Only the spatial resampled data are stored and required, then the temporal resolution can be changed whithout issues.

...
