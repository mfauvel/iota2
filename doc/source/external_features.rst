
.. _external-features:

External Features
=================

.. Warning:: New features have been added. Previous user functions can no longer work. Please refer to the data access section to take into account the changes.

What is the external features module ?
--------------------------------------

This module proposes to use user-provided code to compute additional features which will be used for training and prediction.

By default, and if possible depending on sensors, iota2 computes three spectral indices: NDVI, NDWI and Brightness. Additional radiometric features can also be provided using the ``additionalFeatures`` field in the configuration file.

However, features more complex than simple radiometric indices may need the ability to write some code. This module allows to compute these kind of features.

The main idea is to extract the pixel values as a numpy array, so it is easy to compute everything you want.

Which data can be used with external features ?
-----------------------------------------------

iota2 can provides two time series:
- the output of iota2 workflow, the gapfilled (or not) reflectances and spectral indices (NDVI, NDWI, Brightness for Sentinel2)
- the raw time series, with no kind of interpolation

In the case of multiple tiles, the raw time series of each tiles contains different dates which cannot be handled by almost of the machine learning algorithm.
To this end, an option was added to fill missing the missing date in each tiles, considering all the dates of all tiles used.

In addition to the raw time serie, the mask time serie is available to indicate which pixels are valid, cloudy or edge or saturated and a new flag for non acquired dates.

The list of dates can also be accessed and used in the user code.

An exogeneous data can also be used. The term exogeneous here refers to an image, covering the entire area of interest, which can be used for external features computation. For instance, a temperature map or a LAI.
This data can be a multiband or monoband image.

How use it ?
------------

The external features module requires only one python file, containing one or more functions.

The user must provide, using the configuration file, the full path of his code, and the list of functions to be computed.


In the configuration file, two blocks are required for using external features:

- `external_features <i2_classification.external_features>`
- `python_data_managing <i2_classification.python_data_managing>`

To activate the external features mode, there are two mandatory parameters in `external_features` section:

``module``
    the full path to the file containing source code. A ".py" extension is mandatory

``functions``
    the list of functions to be computed, separated by spaces


iota2 provides several external functions ready to use. The complete list of functions is available in :mod:`external_code <iota2.Common.external_code>`.

Some of them are documented, explaining how manage data and the output requirements:

.. currentmodule:: iota2.Common.external_code

.. autofunction:: get_soi


The workflow try to find functions first in the ``module`` user code source, then it looks in the source code.
If a function in ``functions`` is not found, the chain raise an exception and not start.



There are several optional parameters, initialized by default, the most relevant to manage data are:

``chunk_size_mode``
    The split image mode. It can be ``split_number`` for dividing the image according to the number given or ``user_fixed`` for working with the chunk size indicated by parameters
``number_of_chunks``
    the number of chunks used to split input data and avoid memory overflow. Mode ``split_number`` only
``chunk_size_x`` and ``chunk_size_y``
    give a chunk size according to axis x and y

``data_mode_access``
    Three values are allowed: `raw` to enable only the raw data and masks, `gapfilled` to enable only the ouput of iota2 workflow, and `both` to enable both access. The default value is `gapfilled`

``fill_missing_dates``
    This option works only with `raw` or `both` mode enabled. It allow iota2 to fill with 0 all dates not acquired. Then each tiles have the same dates. Each filled date has a corresponding mask with a dedicated value.

.. warning::
   According `issue #323 <https://framagit.org/iota2-project/iota2/-/issues/323>`_ only `number_of_chunks` option is usable currently.
	
Once this field is correctly filled, iota2 can be run as usual.

Coding external features functions:
-----------------------------------
Before explaining how external features must be coded, some explainations about the available tools.

iota2 provides two classes used for external features:

``external_numpy_features``
    the high level class, used in the iota2 processing. This class uses the configuration file parameters to apply the user provided functions. For a standard use, the class has no interest for the user.

``data_container``
    this class provides functions to access data. The available methods change according to the sensors used.

To have an exhaustive list of available functions, it is possible to instanciate the ``data_container`` class, using a sensors parameters dictionnary and a tile name.

All functions contained in ``data_container`` are named as : ``get_mode_sensors-name_Bx``, where ``x`` is the name of bands available in each sensor, and ``mode`` is ``raw`` or ``interpolated``.

For instance for Sentinel2 (Theia format) it is possible to get all pixels corresponding to the band 2 with ``get_interpolated_Sentinel2_B2()`` function. The returned numpy array is the complete time series for the corresponding spectral band. 
It is also possible to use iota2 default spectral indices, like NDVI. In this case, the get function is named ``get_interpolated_Sentinel2_NDVI``.

.. Note::
   There is no multiplication factor for the reflectance values returned by the get functions. Please refer to the supplier's website for more information.
   For example theia S2 data is provided as int16 with a nodata value of -10000. 

.. warning::
   The spectral indices generated by iota2 like NDVI, NDWI and Brightness are converted in integer using a factor of 1000. 
   
Then, the function provided by the user must use these functions to get and produce a numpy array. The function must return also a list of label names (or empty for default naming). See examples below for a better understanding.

Recommendations:
----------------
A simple way to know which ``get`` functions are available, is to create a data container object like in the folowing example.

.. literalinclude:: examples/find_functions_external_features.py
       :pyobject: print_get_methods
				  

Example:
--------
A full example for using external features, using Sentinel2.

First, create a python file, named ``my_module.py`` containing one function:

.. code-block:: python
				
        def get_soi(self):
            """
            compute the Soil Composition Index
            """
            coef = (self.get_interpolated_Sentinel2_B11() - self.get_interpolated_Sentinel2_B8()) / (
            self.get_interpolated_Sentinel2_B11() + self.get_interpolated_Sentinel2_B8())
            labels = [f"soi_{i+1}" for i in range(coef.shape[2])]
            return coef, labels



In this case, the output type is `float`, it is also possible to convert it as `int` by applying a coefficient (1000 for instance).

It is possible to use `raw` data instead of `interpolated` data, if the parameters are well set.


A minimal configuration example:
			  
.. code-block:: python
		
	...
    external_features:
    {
        module:"path/to/module/my_module.py"
        functions:"get_soi"
      
    }
	python_data_managing:
	{
	    chunk_size_mode:"split_number"
        number_of_chunks:50
	}

Limitations:
------------

.. warning::
    External features can not be used with ``userFeatures`` sensors.
	Indeed, it is mandatory to have the bands information to provide the ``get`` methods.

	Additionnaly, scikit-learn models can not be use with this feature as well as the auto-context workflow.

Using raw and interpolated data together can lead to ram issue. In this case, the main option is to increase the total number of chunks used.

According to the sqlite limitations, the total number of features cannot exceed 1000.
