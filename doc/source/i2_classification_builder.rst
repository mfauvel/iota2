i2_classification
#################

.. _i2_classification.Landsat5_old:

Landsat5_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.Landsat5_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_classification.Landsat5_old.end_date:
      * - end_date
        - 
        - The end date of interpolated image time series
        - str
        - False
        - end_date


          .. _i2_classification.Landsat5_old.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_classification.Landsat5_old.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series
        - str
        - False
        - start_date


          .. _i2_classification.Landsat5_old.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolation
        - int
        - False
        - temporal_resolution





.. _i2_classification.Landsat8:

Landsat8
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.Landsat8.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_classification.Landsat8.end_date:
      * - end_date
        - 
        - The end date of interpolated image time series
        - str
        - False
        - end_date


          .. _i2_classification.Landsat8.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_classification.Landsat8.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series
        - str
        - False
        - start_date


          .. _i2_classification.Landsat8.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolation
        - int
        - False
        - temporal_resolution


          .. _i2_classification.Landsat8.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - Enable the write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_classification.Landsat8_old:

Landsat8_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.Landsat8_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_classification.Landsat8_old.end_date:
      * - end_date
        - 
        - The end date of interpolated image time series
        - str
        - False
        - end_date


          .. _i2_classification.Landsat8_old.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_classification.Landsat8_old.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series
        - str
        - False
        - start_date


          .. _i2_classification.Landsat8_old.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolation
        - int
        - False
        - temporal_resolution


          .. _i2_classification.Landsat8_old.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - Enable the write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_classification.Sentinel_2:

Sentinel_2
**********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.Sentinel_2.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_classification.Sentinel_2.end_date:
      * - end_date
        - 
        - The end date of interpolated image time series
        - str
        - False
        - end_date


          .. _i2_classification.Sentinel_2.keep_bands:
      * - keep_bands
        - ['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B11', 'B12']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_classification.Sentinel_2.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series
        - str
        - False
        - start_date


          .. _i2_classification.Sentinel_2.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolation
        - int
        - False
        - temporal_resolution


          .. _i2_classification.Sentinel_2.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - Enable the write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_classification.Sentinel_2_L3A:

Sentinel_2_L3A
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.Sentinel_2_L3A.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_classification.Sentinel_2_L3A.end_date:
      * - end_date
        - 
        - The end date of interpolated image time series
        - str
        - False
        - end_date


          .. _i2_classification.Sentinel_2_L3A.keep_bands:
      * - keep_bands
        - ['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B11', 'B12']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_classification.Sentinel_2_L3A.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series
        - str
        - False
        - start_date


          .. _i2_classification.Sentinel_2_L3A.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolation
        - int
        - False
        - temporal_resolution


          .. _i2_classification.Sentinel_2_L3A.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - Enable the write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_classification.Sentinel_2_S2C:

Sentinel_2_S2C
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.Sentinel_2_S2C.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_classification.Sentinel_2_S2C.end_date:
      * - end_date
        - 
        - The end date of interpolated image time series
        - str
        - False
        - end_date


          .. _i2_classification.Sentinel_2_S2C.keep_bands:
      * - keep_bands
        - ['B02', 'B03', 'B04', 'B05', 'B06', 'B07', 'B08', 'B8A', 'B11', 'B12']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_classification.Sentinel_2_S2C.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series
        - str
        - False
        - start_date


          .. _i2_classification.Sentinel_2_S2C.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolation
        - int
        - False
        - temporal_resolution


          .. _i2_classification.Sentinel_2_S2C.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - Enable the write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_classification.arg_classification:

arg_classification
******************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.arg_classification.classif_mode:
      * - :ref:`classif_mode <desc_i2_classification.arg_classification.classif_mode>`
        - separate
        - 'separate' or 'fusion'.
        - str
        - False
        - :ref:`classif_mode <desc_i2_classification.arg_classification.classif_mode>`


          .. _i2_classification.arg_classification.dempstershafer_mob:
      * - :ref:`dempstershafer_mob <desc_i2_classification.arg_classification.dempstershafer_mob>`
        - precision
        - Choose the dempster shafer mass of belief estimation method
        - str
        - False
        - :ref:`dempstershafer_mob <desc_i2_classification.arg_classification.dempstershafer_mob>`


          .. _i2_classification.arg_classification.enable_probability_map:
      * - :ref:`enable_probability_map <desc_i2_classification.arg_classification.enable_probability_map>`
        - False
        - Produce the probability map
        - bool
        - False
        - :ref:`enable_probability_map <desc_i2_classification.arg_classification.enable_probability_map>`


          .. _i2_classification.arg_classification.fusion_options:
      * - fusion_options
        -  -nodatalabel 0 -method majorityvoting
        - OTB FusionOfClassification options for voting method involved if classif_mode is set to 'fusion'
        - str
        - False
        - fusion_options


          .. _i2_classification.arg_classification.fusionofclassification_all_samples_validation:
      * - :ref:`fusionofclassification_all_samples_validation <desc_i2_classification.arg_classification.fusionofclassification_all_samples_validation>`
        - False
        - Enable the use of all reference data
        - bool
        - False
        - :ref:`fusionofclassification_all_samples_validation <desc_i2_classification.arg_classification.fusionofclassification_all_samples_validation>`


          .. _i2_classification.arg_classification.keep_runs_results:
      * - :ref:`keep_runs_results <desc_i2_classification.arg_classification.keep_runs_results>`
        - True
        - 
        - bool
        - False
        - :ref:`keep_runs_results <desc_i2_classification.arg_classification.keep_runs_results>`


          .. _i2_classification.arg_classification.merge_final_classifications:
      * - merge_final_classifications
        - False
        - Enable the fusion of classifications mode, merging all run in a unique result.
        - bool
        - False
        - merge_final_classifications


          .. _i2_classification.arg_classification.merge_final_classifications_method:
      * - merge_final_classifications_method
        - majorityvoting
        - Indicate the fusion of classification method: 'majorityvoting' or 'dempstershafer'
        - str
        - False
        - merge_final_classifications_method


          .. _i2_classification.arg_classification.merge_final_classifications_ratio:
      * - merge_final_classifications_ratio
        - 0.1
        - Percentage of samples to use in order to evaluate the fusion raster
        - float
        - False
        - merge_final_classifications_ratio


          .. _i2_classification.arg_classification.merge_final_classifications_undecidedlabel:
      * - merge_final_classifications_undecidedlabel
        - 255
        - Indicate the label for undecision case during fusion
        - int
        - False
        - merge_final_classifications_undecidedlabel


          .. _i2_classification.arg_classification.no_label_management:
      * - no_label_management
        - maxConfidence
        - Method for choosing a label in case of fusion
        - str
        - False
        - no_label_management





Notes
=====

.. _desc_i2_classification.arg_classification.classif_mode:

:ref:`classif_mode <i2_classification.arg_classification.classif_mode>`
-----------------------------------------------------------------------
 If 'fusion' : too huge models will be devided into smaller ones and will classify the same pixels. The treshold between small/big models is define by the parameter 'mode_outside_regionsplit'

.. _desc_i2_classification.arg_classification.dempstershafer_mob:

:ref:`dempstershafer_mob <i2_classification.arg_classification.dempstershafer_mob>`
-----------------------------------------------------------------------------------
Two kind of indexes can be used:
* Global: `accuracy` or `kappa`
* Per class: `precision` or `recall`


.. _desc_i2_classification.arg_classification.enable_probability_map:

:ref:`enable_probability_map <i2_classification.arg_classification.enable_probability_map>`
-------------------------------------------------------------------------------------------
A probability map is a image with N bands , where N is the number of classes in the nomenclature file. The bands are sorted in ascending order more information more information :doc:`here <probability_maps>`

.. _desc_i2_classification.arg_classification.fusionofclassification_all_samples_validation:

:ref:`fusionofclassification_all_samples_validation <i2_classification.arg_classification.fusionofclassification_all_samples_validation>`
-----------------------------------------------------------------------------------------------------------------------------------------
If the fusion mode is enabled, enable the use of all reference data samples for validation

.. _desc_i2_classification.arg_classification.keep_runs_results:

:ref:`keep_runs_results <i2_classification.arg_classification.keep_runs_results>`
---------------------------------------------------------------------------------
If in fusion mode, two final reports can be provided. One for each seed, and one for the classification fusion

.. _i2_classification.arg_train:

arg_train
*********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.arg_train.a_crop_label_replacement:
      * - a_crop_label_replacement
        - ['10', 'annual_crop']
        - Replace a label by a string
        - list
        - False
        - a_crop_label_replacement


          .. _i2_classification.arg_train.annual_classes_extraction_source:
      * - annual_classes_extraction_source
        - None
        - 
        - str
        - False
        - annual_classes_extraction_source


          .. _i2_classification.arg_train.annual_crop:
      * - annual_crop
        - ['11', '12']
        - The list of classes to be replaced by previous data
        - list
        - False
        - annual_crop


          .. _i2_classification.arg_train.autocontext_iterations:
      * - autocontext_iterations
        - 3
        - Number of iterations in auto-context.
        - int
        - False
        - autocontext_iterations


          .. _i2_classification.arg_train.classifier:
      * - classifier
        - rf
        - Choose the classification algorithm
        - str
        - False
        - classifier


          .. _i2_classification.arg_train.crop_mix:
      * - crop_mix
        - False
        - Enable crop mix option
        - bool
        - False
        - crop_mix


          .. _i2_classification.arg_train.dempster_shafer_sar_opt_fusion:
      * - :ref:`dempster_shafer_sar_opt_fusion <desc_i2_classification.arg_train.dempster_shafer_sar_opt_fusion>`
        - False
        - Enable the use of both SAR and optical data to train a model.
        - bool
        - False
        - :ref:`dempster_shafer_sar_opt_fusion <desc_i2_classification.arg_train.dempster_shafer_sar_opt_fusion>`


          .. _i2_classification.arg_train.enable_autocontext:
      * - enable_autocontext
        - False
        - Enable the auto-context processing
        - bool
        - False
        - enable_autocontext


          .. _i2_classification.arg_train.features:
      * - :ref:`features <desc_i2_classification.arg_train.features>`
        - ['NDVI', 'NDWI', 'Brightness']
        - List of additional features computed
        - list
        - False
        - :ref:`features <desc_i2_classification.arg_train.features>`


          .. _i2_classification.arg_train.features_from_raw_dates:
      * - features_from_raw_dates
        - False
        - learn model from raw sensor's date (no interpolations)
        - bool
        - False
        - features_from_raw_dates


          .. _i2_classification.arg_train.force_standard_labels:
      * - :ref:`force_standard_labels <desc_i2_classification.arg_train.force_standard_labels>`
        - False
        - Standardize labels for feature extraction
        - bool
        - False
        - :ref:`force_standard_labels <desc_i2_classification.arg_train.force_standard_labels>`


          .. _i2_classification.arg_train.mode_outside_regionsplit:
      * - :ref:`mode_outside_regionsplit <desc_i2_classification.arg_train.mode_outside_regionsplit>`
        - 0.1
        - Fix the threshold for split huge model
        - float
        - False
        - :ref:`mode_outside_regionsplit <desc_i2_classification.arg_train.mode_outside_regionsplit>`


          .. _i2_classification.arg_train.otb_classifier_options:
      * - :ref:`otb_classifier_options <desc_i2_classification.arg_train.otb_classifier_options>`
        - None
        - OTB option for classifier. If None, the OTB default values are used.
        - dict
        - False
        - :ref:`otb_classifier_options <desc_i2_classification.arg_train.otb_classifier_options>`


          .. _i2_classification.arg_train.output_prev_features:
      * - output_prev_features
        - None
        - Path to previous features for crop mix
        - str
        - False
        - output_prev_features


          .. _i2_classification.arg_train.prev_features:
      * - :ref:`prev_features <desc_i2_classification.arg_train.prev_features>`
        - None
        - Path to a configuration file used to produce previous features
        - str
        - False
        - :ref:`prev_features <desc_i2_classification.arg_train.prev_features>`


          .. _i2_classification.arg_train.random_seed:
      * - :ref:`random_seed <desc_i2_classification.arg_train.random_seed>`
        - None
        - Fix the random seed for random split of reference data
        - int
        - False
        - :ref:`random_seed <desc_i2_classification.arg_train.random_seed>`


          .. _i2_classification.arg_train.ratio:
      * - ratio
        - 0.5
        - Should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the train split.
        - float
        - False
        - ratio


          .. _i2_classification.arg_train.runs:
      * - :ref:`runs <desc_i2_classification.arg_train.runs>`
        - 1
        - Number of independant runs processed.
        - int
        - False
        - :ref:`runs <desc_i2_classification.arg_train.runs>`


          .. _i2_classification.arg_train.sample_augmentation:
      * - :ref:`sample_augmentation <desc_i2_classification.arg_train.sample_augmentation>`
        - {'activate': False}
        - OTB parameters for sample augmentation
        - dict
        - False
        - :ref:`sample_augmentation <desc_i2_classification.arg_train.sample_augmentation>`


          .. _i2_classification.arg_train.sample_management:
      * - :ref:`sample_management <desc_i2_classification.arg_train.sample_management>`
        - None
        - Absolute path to a CSV file containing samples transfert strategies
        - str
        - False
        - :ref:`sample_management <desc_i2_classification.arg_train.sample_management>`


          .. _i2_classification.arg_train.sample_selection:
      * - :ref:`sample_selection <desc_i2_classification.arg_train.sample_selection>`
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sample selection
        - dict
        - False
        - :ref:`sample_selection <desc_i2_classification.arg_train.sample_selection>`


          .. _i2_classification.arg_train.sample_validation:
      * - :ref:`sample_validation <desc_i2_classification.arg_train.sample_validation>`
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sampling the validation set
        - dict
        - False
        - :ref:`sample_validation <desc_i2_classification.arg_train.sample_validation>`


          .. _i2_classification.arg_train.samples_classif_mix:
      * - samples_classif_mix
        - False
        - Enable the second step of crop mix
        - bool
        - False
        - samples_classif_mix


          .. _i2_classification.arg_train.sampling_validation:
      * - sampling_validation
        - False
        - Enable sampling validation
        - bool
        - False
        - sampling_validation


          .. _i2_classification.arg_train.split_ground_truth:
      * - :ref:`split_ground_truth <desc_i2_classification.arg_train.split_ground_truth>`
        - True
        - Enable the split of reference data 
        - bool
        - False
        - :ref:`split_ground_truth <desc_i2_classification.arg_train.split_ground_truth>`


          .. _i2_classification.arg_train.validity_threshold:
      * - validity_threshold
        - 1
        - 
        - int
        - False
        - validity_threshold





Notes
=====

.. _desc_i2_classification.arg_train.dempster_shafer_sar_opt_fusion:

:ref:`dempster_shafer_sar_opt_fusion <i2_classification.arg_train.dempster_shafer_sar_opt_fusion>`
--------------------------------------------------------------------------------------------------
Enable the use of both SAR and optical data to train a model. If True then two models are trained.more documentation is avalailalbe :doc:`here <SAR_Opt_postClassif_fusion>`

.. _desc_i2_classification.arg_train.features:

:ref:`features <i2_classification.arg_train.features>`
------------------------------------------------------
This parameter enable the computation of the three indices if available for the sensor used.There is no choice for using only one of them.

.. _desc_i2_classification.arg_train.force_standard_labels:

:ref:`force_standard_labels <i2_classification.arg_train.force_standard_labels>`
--------------------------------------------------------------------------------
The chain label each features by the sensors name, the spectral band or indice and the date. If activated this parameter use the OTB default value (`value_X`)

.. _desc_i2_classification.arg_train.mode_outside_regionsplit:

:ref:`mode_outside_regionsplit <i2_classification.arg_train.mode_outside_regionsplit>`
--------------------------------------------------------------------------------------
This parameter is available if regionPath is used and arg_classification.classif_mode is set to fusion. It represents the maximum size covered by a region. If the regions are larger than this threshold, then N models are built by randomly selecting features inside the region.

.. _desc_i2_classification.arg_train.otb_classifier_options:

:ref:`otb_classifier_options <i2_classification.arg_train.otb_classifier_options>`
----------------------------------------------------------------------------------
This parameter is a dictionnary which accepts all OTB application parameters. To know the exhaustive parameter list  use `otbcli_TrainVectorClassifier` in a terminal or look at the OTB documentation

.. _desc_i2_classification.arg_train.prev_features:

:ref:`prev_features <i2_classification.arg_train.prev_features>`
----------------------------------------------------------------
This config file must be launchable by iota2 (needed for crop mix)

.. _desc_i2_classification.arg_train.random_seed:

:ref:`random_seed <i2_classification.arg_train.random_seed>`
------------------------------------------------------------
Fix the random seed used for random split of reference data If set, the results must be the same for a given classifier

.. _desc_i2_classification.arg_train.runs:

:ref:`runs <i2_classification.arg_train.runs>`
----------------------------------------------
Number of independant runs processed. Each run has his own learning samples. Must be an integer greater than 0

.. _desc_i2_classification.arg_train.sample_augmentation:

:ref:`sample_augmentation <i2_classification.arg_train.sample_augmentation>`
----------------------------------------------------------------------------
In supervised classification the balance between class samples is important. There are any ways to manage class balancing in iota2, using :ref:`refSampleSelection` or the classifier's options to limit the number of samples by class.
An other approch is to generate synthetic samples. It is the purpose of thisfunctionality, which is called 'sample augmentation'.

    .. code-block:: python

        {'activate':False}

Example
^^^^^^^

    .. code-block:: python

        sample_augmentation : {'target_models':['1', '2'],
                              'strategy' : 'jitter',
                              'strategy.jitter.stdfactor' : 10,
                              'strategy.smote.neighbors'  : 5,
                              'samples.strategy' : 'balance',
                              'activate' : True
                              }


iota2 implements an interface to the OTB `SampleAugmentation <https://www.orfeo-toolbox.org/CookBook/Applications/app_SampleSelection.html>`_ application.
There are three methods to generate samples : replicate, jitter and smote.The documentation :doc:`here <sampleAugmentation_explain>` explains the difference between these approaches.

 ``samples.strategy`` specifies how many samples must be created.There are 3 different strategies:    - minNumber
        To set the minimum number of samples by class required
    - balance
        balance all classes with the same number of samples as the majority one
    - byClass
        augment only some of the classes
Parameters related to ``minNumber`` and ``byClass`` strategies are    - samples.strategy.minNumber
        minimum number of samples
    - samples.strategy.byClass
        path to a CSV file containing in first column the class's label and 
        in the second column the minimum number of samples required.
In the above example, classes of models '1' and '2' will be augmented to thethe most represented class in the corresponding model using the jitter method.

.. _desc_i2_classification.arg_train.sample_management:

:ref:`sample_management <i2_classification.arg_train.sample_management>`
------------------------------------------------------------------------
The CSV must contain a row per transfert
    .. code-block:: python

        >>> cat /absolute/path/myRules.csv
            1,2,4,2
Meaning :
        +--------+-------------+------------+----------+
        | source | destination | class name | quantity |
        +========+=============+============+==========+
        |   1    |      2      |      4     |     2    |
        +--------+-------------+------------+----------+


.. _desc_i2_classification.arg_train.sample_selection:

:ref:`sample_selection <i2_classification.arg_train.sample_selection>`
----------------------------------------------------------------------
This field parameters the strategy of polygon sampling. It directly refers to options of OTB’s SampleSelection application.

Example
^^^^^^^

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'}]
                           }

In the example above, all polygons will be sampled with the 20% ratio. But the polygons which belong to the model 4 will be periodically sampled, instead of the ransom sampling used for other polygons.
Notice than ``per_models`` key contains a list of strategies. Then we can imagine the following :

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'},
                                         {'target_model':'1',
                                          'sampler':'random',
                                          'strategy', 'byclass',
                                          'strategy.byclass.in', '/path/to/myCSV.csv'
                                         }]
                           }
 where the first column of /path/to/myCSV.csv is class label (integer), second one is the required samples number (integer).

.. _desc_i2_classification.arg_train.sample_validation:

:ref:`sample_validation <i2_classification.arg_train.sample_validation>`
------------------------------------------------------------------------
This field parameters the strategy of polygon sampling. It directly refers to options of OTB’s SampleSelection application.

Example
-------

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'}]
                           }

In the example above, all polygons will be sampled with the 20% ratio. But the polygons which belong to the model 4 will be periodically sampled, instead of the ransom sampling used for other polygons.
Notice than ``per_models`` key contains a list of strategies. Then we can imagine the following :

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'},
                                         {'target_model':'1',
                                          'sampler':'random',
                                          'strategy', 'byclass',
                                          'strategy.byclass.in', '/path/to/myCSV.csv'
                                         }]
                           }
 where the first column of /path/to/myCSV.csv is class label (integer), second one is the required samples number (integer).

.. _desc_i2_classification.arg_train.split_ground_truth:

:ref:`split_ground_truth <i2_classification.arg_train.split_ground_truth>`
--------------------------------------------------------------------------
 If set to False, the chain use all polygons for both training and validation

.. _i2_classification.builders:

builders
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.builders.builders_class_name:
      * - :ref:`builders_class_name <desc_i2_classification.builders.builders_class_name>`
        - ['i2_classification']
        - The name of the class defining the builder
        - list
        - False
        - :ref:`builders_class_name <desc_i2_classification.builders.builders_class_name>`


          .. _i2_classification.builders.builders_paths:
      * - :ref:`builders_paths <desc_i2_classification.builders.builders_paths>`
        - /path/to/iota2/sources
        - The path to user builders
        - list
        - False
        - :ref:`builders_paths <desc_i2_classification.builders.builders_paths>`





Notes
=====

.. _desc_i2_classification.builders.builders_class_name:

:ref:`builders_class_name <i2_classification.builders.builders_class_name>`
---------------------------------------------------------------------------
Available builders are : 'i2_classification', 'i2_features_map', 'i2_obia' and 'i2_vectorization'

.. _desc_i2_classification.builders.builders_paths:

:ref:`builders_paths <i2_classification.builders.builders_paths>`
-----------------------------------------------------------------
If not indicated, the iota2 source directory is used: */iota2/sequence_builders/

.. _i2_classification.chain:

chain
*****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.chain.check_inputs:
      * - :ref:`check_inputs <desc_i2_classification.chain.check_inputs>`
        - True
        - Enable the inputs verification.
        - bool
        - False
        - :ref:`check_inputs <desc_i2_classification.chain.check_inputs>`


          .. _i2_classification.chain.cloud_threshold:
      * - :ref:`cloud_threshold <desc_i2_classification.chain.cloud_threshold>`
        - 0
        - Threshold to consider that a pixel is valid
        - int
        - False
        - :ref:`cloud_threshold <desc_i2_classification.chain.cloud_threshold>`


          .. _i2_classification.chain.color_table:
      * - color_table
        - None
        - Absolute path to the file which link classes and their colors
        - str
        - True
        - color_table


          .. _i2_classification.chain.data_field:
      * - data_field
        - None
        - Field name indicating classes labels in `ground_thruth`
        - str
        - True
        - data_field


          .. _i2_classification.chain.first_step:
      * - first_step
        - None
        - The step group name indicating where the chain start
        - str
        - True
        - first_step


          .. _i2_classification.chain.ground_truth:
      * - ground_truth
        - None
        - Absolute path to reference data
        - str
        - True
        - ground_truth


          .. _i2_classification.chain.l5_path_old:
      * - l5_path_old
        - None
        - Absolute path to Landsat-5 images coming from old THEIA format (D*H*)
        - str
        - False
        - l5_path_old


          .. _i2_classification.chain.l8_path:
      * - l8_path
        - None
        - Absolute path to Landsat-8 images comingfrom new tiled THEIA data
        - str
        - False
        - l8_path


          .. _i2_classification.chain.l8_path_old:
      * - l8_path_old
        - None
        - Absolute path to Landsat-8 images coming from old THEIA format (D*H*)
        - str
        - False
        - l8_path_old


          .. _i2_classification.chain.last_step:
      * - last_step
        - None
        - The step group name indicating where the chain ends
        - str
        - True
        - last_step


          .. _i2_classification.chain.list_tile:
      * - list_tile
        - None
        - List of tile to process, separated by space
        - str
        - True
        - list_tile


          .. _i2_classification.chain.logger_level:
      * - logger_level
        - INFO
        - Set the logger level: NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL
        - str
        - False
        - logger_level


          .. _i2_classification.chain.minimum_required_dates:
      * - minimum_required_dates
        - 2
        - required minimum number of available dates for each sensor
        - int
        - False
        - minimum_required_dates


          .. _i2_classification.chain.nomenclature_path:
      * - nomenclature_path
        - None
        - Absolute path to the nomenclature description file
        - str
        - True
        - nomenclature_path


          .. _i2_classification.chain.output_path:
      * - :ref:`output_path <desc_i2_classification.chain.output_path>`
        - None
        - Absolute path to the output directory.
        - str
        - True
        - :ref:`output_path <desc_i2_classification.chain.output_path>`


          .. _i2_classification.chain.output_statistics:
      * - :ref:`output_statistics <desc_i2_classification.chain.output_statistics>`
        - False
        - Enable the writing of PNG files containing additional statistics
        - bool
        - False
        - :ref:`output_statistics <desc_i2_classification.chain.output_statistics>`


          .. _i2_classification.chain.proj:
      * - proj
        - EPSG:2154
        - The projection wanted. Format EPSG:XXXX is mandatory
        - str
        - False
        - proj


          .. _i2_classification.chain.region_field:
      * - :ref:`region_field <desc_i2_classification.chain.region_field>`
        - region
        - The column name for region indicator in`region_path` file
        - str
        - False
        - :ref:`region_field <desc_i2_classification.chain.region_field>`


          .. _i2_classification.chain.region_path:
      * - region_path
        - None
        - Absolute path to region vector file
        - str
        - False
        - region_path


          .. _i2_classification.chain.remove_output_path:
      * - :ref:`remove_output_path <desc_i2_classification.chain.remove_output_path>`
        - True
        - Enable the removing of complete `output_path` directory
        - bool
        - False
        - :ref:`remove_output_path <desc_i2_classification.chain.remove_output_path>`


          .. _i2_classification.chain.s1_path:
      * - s1_path
        - None
        - Absolute path to Sentinel-1 configuration file
        - str
        - False
        - s1_path


          .. _i2_classification.chain.s2_l3a_output_path:
      * - s2_l3a_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory.
        - str
        - False
        - s2_l3a_output_path


          .. _i2_classification.chain.s2_l3a_path:
      * - s2_l3a_path
        - None
        - Absolute path to Sentinel-2 L3A images (THEIA format)
        - str
        - False
        - s2_l3a_path


          .. _i2_classification.chain.s2_output_path:
      * - s2_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory.
        - str
        - False
        - s2_output_path


          .. _i2_classification.chain.s2_path:
      * - s2_path
        - None
        - Absolute path to Sentinel-2 images (THEIA format)
        - str
        - False
        - s2_path


          .. _i2_classification.chain.s2_s2c_output_path:
      * - s2_s2c_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory.
        - str
        - False
        - s2_s2c_output_path


          .. _i2_classification.chain.s2_s2c_path:
      * - s2_s2c_path
        - None
        - Absolute path to Sentinel-2 images (Sen2Cor format)
        - str
        - False
        - s2_s2c_path


          .. _i2_classification.chain.spatial_resolution:
      * - :ref:`spatial_resolution <desc_i2_classification.chain.spatial_resolution>`
        - 10
        - Output spatial resolution
        - list
        - False
        - :ref:`spatial_resolution <desc_i2_classification.chain.spatial_resolution>`


          .. _i2_classification.chain.user_feat_path:
      * - :ref:`user_feat_path <desc_i2_classification.chain.user_feat_path>`
        - None
        - Absolute path to the user's features path
        - str
        - False
        - :ref:`user_feat_path <desc_i2_classification.chain.user_feat_path>`





Notes
=====

.. _desc_i2_classification.chain.check_inputs:

:ref:`check_inputs <i2_classification.chain.check_inputs>`
----------------------------------------------------------
Enable the inputs verification. It can take a lot of time for large dataset. Check if region intersect reference data for instance

.. _desc_i2_classification.chain.cloud_threshold:

:ref:`cloud_threshold <i2_classification.chain.cloud_threshold>`
----------------------------------------------------------------
Indicates the threshold for a polygon to be used for learning. It use the validity count, which is incremented if a cloud, a cloud shadow or a saturated pixel is detected

.. _desc_i2_classification.chain.output_path:

:ref:`output_path <i2_classification.chain.output_path>`
--------------------------------------------------------
Absolute path to the output directory.It is recommended to have one directory per run of the chain

.. _desc_i2_classification.chain.output_statistics:

:ref:`output_statistics <i2_classification.chain.output_statistics>`
--------------------------------------------------------------------
Enable the writing of PNG files containing additional statisticscontaining the confidence by learning/validation pixels

.. _desc_i2_classification.chain.region_field:

:ref:`region_field <i2_classification.chain.region_field>`
----------------------------------------------------------
this column in the database must contains string which can be converted into integers. For instance '1_2' does not match this condition

.. _desc_i2_classification.chain.remove_output_path:

:ref:`remove_output_path <i2_classification.chain.remove_output_path>`
----------------------------------------------------------------------
Enable the removing of complete `output_path` directory
Only if the `first_step` is `init` and the folder name is valid

.. _desc_i2_classification.chain.spatial_resolution:

:ref:`spatial_resolution <i2_classification.chain.spatial_resolution>`
----------------------------------------------------------------------
The spatial resolution expected.It can be provided as integer or float,or as a list containing two values for non squared resolution

.. _desc_i2_classification.chain.user_feat_path:

:ref:`user_feat_path <i2_classification.chain.user_feat_path>`
--------------------------------------------------------------
Absolute path to the user's features path They must be stored by tiles

.. _i2_classification.coregistration:

coregistration
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.coregistration.band_ref:
      * - band_ref
        - 1
        - Number of the band of the VHR image to use for coregistration
        - int
        - False
        - band_ref


          .. _i2_classification.coregistration.band_src:
      * - band_src
        - 3
        - Number of the band of the src raster to use for coregistration
        - int
        - False
        - band_src


          .. _i2_classification.coregistration.date_src:
      * - :ref:`date_src <desc_i2_classification.coregistration.date_src>`
        - None
        - Date `YYYYMMDD` of the reference image
        - str
        - False
        - :ref:`date_src <desc_i2_classification.coregistration.date_src>`


          .. _i2_classification.coregistration.date_vhr:
      * - date_vhr
        - None
        - Date `YYYYMMDD` of the VHR image
        - str
        - False
        - date_vhr


          .. _i2_classification.coregistration.iterate:
      * - iterate
        - True
        - Proceed several iteration by reducing the step between geobin to find SIFT points
        - bool
        - False
        - iterate


          .. _i2_classification.coregistration.minsiftpoints:
      * - minsiftpoints
        - 40
        - Minimal number of SIFT points to find to create the new RPC model
        - int
        - False
        - minsiftpoints


          .. _i2_classification.coregistration.minstep:
      * - minstep
        - 16
        - Minimal size of steps between bins in pixels
        - int
        - False
        - minstep


          .. _i2_classification.coregistration.mode:
      * - :ref:`mode <desc_i2_classification.coregistration.mode>`
        - 2
        - Coregistration mode of the time series
        - int
        - False
        - :ref:`mode <desc_i2_classification.coregistration.mode>`


          .. _i2_classification.coregistration.pattern:
      * - :ref:`pattern <desc_i2_classification.coregistration.pattern>`
        - None
        - Pattern of the time series files to coregister
        - str
        - False
        - :ref:`pattern <desc_i2_classification.coregistration.pattern>`


          .. _i2_classification.coregistration.prec:
      * - prec
        - 3
        - Estimated shift between source and reference raster in pixel (source raster resolution)
        - int
        - False
        - prec


          .. _i2_classification.coregistration.resample:
      * - resample
        - True
        - Resample the reference and the source rasterto the same resolution to find SIFT points
        - bool
        - False
        - resample


          .. _i2_classification.coregistration.step:
      * - step
        - 256
        - Initial size of steps between bins in pixels
        - int
        - False
        - step


          .. _i2_classification.coregistration.vhr_path:
      * - vhr_path
        - None
        - Absolute path to VHR path
        - str
        - False
        - vhr_path





Notes
=====

.. _desc_i2_classification.coregistration.date_src:

:ref:`date_src <i2_classification.coregistration.date_src>`
-----------------------------------------------------------
If no `date_src` is mentionned, the best image will be automatically choose for coregistration

.. _desc_i2_classification.coregistration.mode:

:ref:`mode <i2_classification.coregistration.mode>`
---------------------------------------------------
+--------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Mode   | Method                                                                                                                                                                                                                                    |
+========+===========================================================================================================================================================================================================================================+
|  1     |  single coregistration between one source image (and its masks) and the VHR image                                                                                                                                                         |
+--------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|  2     | this mode operates a coregistration between a image of the timeseries and the VHR image, then the same RPC model is used to orthorectify every images of the timeseries                                                                   |
+--------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|  3     | cascade mode, this mode operates a first coregistration between a source image and the VHR image, then each image of the timeseries is coregistered step by step with the closest temporal images of the timeseries already coregistered  |
+--------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+


.. _desc_i2_classification.coregistration.pattern:

:ref:`pattern <i2_classification.coregistration.pattern>`
---------------------------------------------------------
By default the value is left to `None` and the pattern depends on the sensor used (*STACK.tif for Sentinel2, ORTHO_SURF_CORR_PENTE*.TIF)

Examples
^^^^^^^^
    .. code-block:: python

        pattern: '*STACK.tif'

.. _i2_classification.dim_red:

dim_red
*******

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.dim_red.dim_red:
      * - dim_red
        - False
        - Enable the dimensionality reduction mode
        - bool
        - False
        - dim_red


          .. _i2_classification.dim_red.reduction_mode:
      * - :ref:`reduction_mode <desc_i2_classification.dim_red.reduction_mode>`
        - global
        - The reduction mode
        - str
        - False
        - :ref:`reduction_mode <desc_i2_classification.dim_red.reduction_mode>`


          .. _i2_classification.dim_red.target_dimension:
      * - target_dimension
        - 4
        - The number of dimension required, according to `reduction_mode`
        - int
        - False
        - target_dimension





Notes
=====

.. _desc_i2_classification.dim_red.reduction_mode:

:ref:`reduction_mode <i2_classification.dim_red.reduction_mode>`
----------------------------------------------------------------
Values authorized are: 'global' or '?'

.. _i2_classification.external_features:

external_features
*****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.external_features.concat_mode:
      * - :ref:`concat_mode <desc_i2_classification.external_features.concat_mode>`
        - True
        - Enable the use of all features
        - bool
        - False
        - :ref:`concat_mode <desc_i2_classification.external_features.concat_mode>`


          .. _i2_classification.external_features.exogeneous_data:
      * - exogeneous_data
        - None
        - An additional to be used for external features
        - str
        - False
        - exogeneous_data


          .. _i2_classification.external_features.external_features_flag:
      * - external_features_flag
        - False
        - Enable the external features mode
        - bool
        - False
        - external_features_flag


          .. _i2_classification.external_features.functions:
      * - functions
        - None
        - The function list to be used to compute features, separated by space
        - str
        - False
        - functions


          .. _i2_classification.external_features.module:
      * - module
        - /path/to/iota2/sources
        - Absolute path for user source code
        - str
        - False
        - module


          .. _i2_classification.external_features.output_name:
      * - output_name
        - None
        - Temporary chunks are written using this name as prefix
        - str
        - False
        - output_name





Notes
=====

.. _desc_i2_classification.external_features.concat_mode:

:ref:`concat_mode <i2_classification.external_features.concat_mode>`
--------------------------------------------------------------------
If disabled, only external features are used in the whole processing

.. _i2_classification.iota2_feature_extraction:

iota2_feature_extraction
************************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.iota2_feature_extraction.acor_feat:
      * - :ref:`acor_feat <desc_i2_classification.iota2_feature_extraction.acor_feat>`
        - False
        - Apply atmospherically corrected features
        - bool
        - False
        - :ref:`acor_feat <desc_i2_classification.iota2_feature_extraction.acor_feat>`


          .. _i2_classification.iota2_feature_extraction.copy_input:
      * - copy_input
        - True
        - use spectral bands as features
        - bool
        - False
        - copy_input


          .. _i2_classification.iota2_feature_extraction.extract_bands:
      * - extract_bands
        - False
        - 
        - bool
        - False
        - extract_bands


          .. _i2_classification.iota2_feature_extraction.keep_duplicates:
      * - keep_duplicates
        - True
        - use 'rel_refl' can generate duplicated feature (ie: NDVI), set to False remove these duplicated features
        - bool
        - False
        - keep_duplicates


          .. _i2_classification.iota2_feature_extraction.rel_refl:
      * - rel_refl
        - False
        - Compute relative reflectances by the red band
        - bool
        - False
        - rel_refl





Notes
=====

.. _desc_i2_classification.iota2_feature_extraction.acor_feat:

:ref:`acor_feat <i2_classification.iota2_feature_extraction.acor_feat>`
-----------------------------------------------------------------------
Apply atmospherically corrected featuresas explained at : http://www.cesbio.ups-tlse.fr/multitemp/?p=12746

.. _i2_classification.python_data_managing:

python_data_managing
********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.python_data_managing.chunk_size_mode:
      * - chunk_size_mode
        - split_number
        - The chunk split mode
        - str
        - False
        - chunk_size_mode


          .. _i2_classification.python_data_managing.chunk_size_x:
      * - chunk_size_x
        - 50
        - The number if rows for chunk
        - int
        - False
        - chunk_size_x


          .. _i2_classification.python_data_managing.chunk_size_y:
      * - chunk_size_y
        - 50
        - The number if rows for chunk
        - int
        - False
        - chunk_size_y


          .. _i2_classification.python_data_managing.data_mode_access:
      * - :ref:`data_mode_access <desc_i2_classification.python_data_managing.data_mode_access>`
        - gapfilled
        - Choose which data can be accessed in custom features
        - str
        - False
        - :ref:`data_mode_access <desc_i2_classification.python_data_managing.data_mode_access>`


          .. _i2_classification.python_data_managing.fill_missing_dates:
      * - :ref:`fill_missing_dates <desc_i2_classification.python_data_managing.fill_missing_dates>`
        - False
        - Fill raw data with no data if dates are missing
        - bool
        - False
        - :ref:`fill_missing_dates <desc_i2_classification.python_data_managing.fill_missing_dates>`


          .. _i2_classification.python_data_managing.number_of_chunks:
      * - number_of_chunks
        - 50
        - The expected number of chunks
        - int
        - False
        - number_of_chunks





Notes
=====

.. _desc_i2_classification.python_data_managing.data_mode_access:

:ref:`data_mode_access <i2_classification.python_data_managing.data_mode_access>`
---------------------------------------------------------------------------------
Three values are allowed:
- gapfilled: give access only the gapfilled data
- raw: gives access only the original raw data
- both: provides access to both data
..Notes:: Data are spatialy resampled, these parameters concern only temporal interpolation

.. _desc_i2_classification.python_data_managing.fill_missing_dates:

:ref:`fill_missing_dates <i2_classification.python_data_managing.fill_missing_dates>`
-------------------------------------------------------------------------------------
If raw data access is enabled, this option considers all unique dates for all tiles and identify which dates are missing for each tile. A missing date is filled using a no data constant value.Cloud or saturation are not corrected, but masks are provided Masks contain three value: 0 for valid data, 1 for cloudy or saturated pixels, 2 for a missing date

.. _i2_classification.scikit_models_parameters:

scikit_models_parameters
************************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.scikit_models_parameters.cross_validation_folds:
      * - cross_validation_folds
        - 5
        - The number of k-folds
        - int
        - False
        - cross_validation_folds


          .. _i2_classification.scikit_models_parameters.cross_validation_grouped:
      * - cross_validation_grouped
        - False
        - 
        - bool
        - False
        - cross_validation_grouped


          .. _i2_classification.scikit_models_parameters.cross_validation_parameters:
      * - cross_validation_parameters
        - {}
        - 
        - dict
        - False
        - cross_validation_parameters


          .. _i2_classification.scikit_models_parameters.model_type:
      * - :ref:`model_type <desc_i2_classification.scikit_models_parameters.model_type>`
        - None
        - machine learning algorthm’s name
        - str
        - False
        - :ref:`model_type <desc_i2_classification.scikit_models_parameters.model_type>`


          .. _i2_classification.scikit_models_parameters.standardization:
      * - standardization
        - False
        - 
        - bool
        - False
        - standardization





Notes
=====

.. _desc_i2_classification.scikit_models_parameters.model_type:

:ref:`model_type <i2_classification.scikit_models_parameters.model_type>`
-------------------------------------------------------------------------
Models comming from scikit-learn are use if scikit_models_parameters.model_type is different from None. More informations about how to use scikit-learn is available at iota2 and scikit-learn machine learning algorithms.

.. _i2_classification.sensors_data_interpolation:

sensors_data_interpolation
**************************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.sensors_data_interpolation.auto_date:
      * - :ref:`auto_date <desc_i2_classification.sensors_data_interpolation.auto_date>`
        - True
        - Enable the use of `start_date` and `end_date`
        - bool
        - False
        - :ref:`auto_date <desc_i2_classification.sensors_data_interpolation.auto_date>`


          .. _i2_classification.sensors_data_interpolation.use_additional_features:
      * - use_additional_features
        - False
        - Enable the use of additional features
        - bool
        - False
        - use_additional_features


          .. _i2_classification.sensors_data_interpolation.use_gapfilling:
      * - use_gapfilling
        - True
        - Enable the use of gapfilling
        - bool
        - False
        - use_gapfilling


          .. _i2_classification.sensors_data_interpolation.write_outputs:
      * - :ref:`write_outputs <desc_i2_classification.sensors_data_interpolation.write_outputs>`
        - False
        - Write temporary files
        - bool
        - False
        - :ref:`write_outputs <desc_i2_classification.sensors_data_interpolation.write_outputs>`





Notes
=====

.. _desc_i2_classification.sensors_data_interpolation.auto_date:

:ref:`auto_date <i2_classification.sensors_data_interpolation.auto_date>`
-------------------------------------------------------------------------
If True, iota2 will automatically guess the first and the last interpolation date. Else, `start_date` and `end_date` of each sensors will be used

.. _desc_i2_classification.sensors_data_interpolation.write_outputs:

:ref:`write_outputs <i2_classification.sensors_data_interpolation.write_outputs>`
---------------------------------------------------------------------------------
Write the time series before and after gapfilling, the mask time series, and also the feature time series. This option required a large amount of free disk space.

.. _i2_classification.task_retry_limits:

task_retry_limits
*****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.task_retry_limits.allowed_retry:
      * - allowed_retry
        - 0
        - Allow dask to retry a failed job N times.
        - int
        - False
        - allowed_retry


          .. _i2_classification.task_retry_limits.maximum_cpu:
      * - :ref:`maximum_cpu <desc_i2_classification.task_retry_limits.maximum_cpu>`
        - 4
        - The maximum number of CPU available
        - int
        - False
        - :ref:`maximum_cpu <desc_i2_classification.task_retry_limits.maximum_cpu>`


          .. _i2_classification.task_retry_limits.maximum_ram:
      * - :ref:`maximum_ram <desc_i2_classification.task_retry_limits.maximum_ram>`
        - 16
        - The maximum amount of RAM available. (gB)
        - int
        - False
        - :ref:`maximum_ram <desc_i2_classification.task_retry_limits.maximum_ram>`





Notes
=====

.. _desc_i2_classification.task_retry_limits.maximum_cpu:

:ref:`maximum_cpu <i2_classification.task_retry_limits.maximum_cpu>`
--------------------------------------------------------------------
the amount of cpu will be doubled if the task is killed due to ram overconsumption until maximum_cpu or allowed_retry are reach

.. _desc_i2_classification.task_retry_limits.maximum_ram:

:ref:`maximum_ram <i2_classification.task_retry_limits.maximum_ram>`
--------------------------------------------------------------------
the amount of RAM will be doubled if the task is killed due to ram overconsumption until maximum_ram or allowed_retry are reach

.. _i2_classification.userFeat:

userFeat
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_classification.userFeat.arbo:
      * - arbo
        - /*
        - The input folder hierarchy
        - str
        - False
        - arbo


          .. _i2_classification.userFeat.patterns:
      * - patterns
        - ALT,ASP,SLP
        - key name for detect the input images
        - str
        - False
        - patterns





