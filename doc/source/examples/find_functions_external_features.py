def print_get_methods(config_file: str,
                      tile: str = "T31TCJ",
                      output_path: str = "/tmp/example"):
    """
    This function prints the list of methods available to access data
    """
    from iota2.Common import customNumpyFeatures as CNF
    from iota2.configuration_files import read_config_file as rcf

    # At this stage, the tile name does not matter as it only used to
    # create the dictionary. This dictionary is the same for all tiles.
    sensors_parameters = rcf.iota2_parameters(
        config_file).get_sensors_parameters(tile)
    enable_gap, enable_raw = rcf.read_config_file(config_file).getParam(
        "python_data_managing", "data_mode_access")
    exogeneous_data = rcf.read_config_file(config_file).getParam(
        "external_features", "exogeneous_data")
    data_cont = CNF.data_container(tile,
                                   output_path,
                                   sensors_parameters,
                                   enabled_raw=enable_raw,
                                   enabled_gap=enable_gap,
                                   exogeneous_data=exogeneous_data)

    # This line show you all get methods available

    functions = [fun for fun in dir(data_cont) if "get_" in fun]
    functions.sort()
    for fun in functions:
        print(fun)
