.. IOTA2 documentation master file, created by
   sphinx-quickstart on Wed Jun  6 12:37:42 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to iota2's documentation !
==================================

.. Note::
   This documentation is not complete and most parts still in development.

.. toctree::
   :maxdepth: 1
   :caption: Getting started:

   Get iota2 <HowToGetIOTA2>
   IOTA2 launching parameters <going_further_i2_launching_params>
   

.. toctree::
   :maxdepth: 1
   :caption: Classification

   Starting with classification <i2_classification_tutorial>
   Land cover mapping parameters <i2_classification_builder>

   
.. toctree::
   :maxdepth: 1
   :caption: Features Maps

   Starting with features maps <i2_features_map_tutorial>
   Features maps parameters <i2_features_map_builder>

.. toctree::
   :maxdepth: 1
   :caption: Vectorization

   Starting with vectorization <i2_vectorization_tutorial>
   Vectorization parameters <i2_vectorization_builder>

.. toctree::
   :maxdepth: 1
   :caption: OBIA

   Starting with OBIA <i2_obia_tutorial>
   OBIA parameters <i2_obia_builder>


.. toctree::
   :maxdepth: 1
   :caption: General Information

   Sentinel-2 Level 3A <sentinel_2_N3A>
   External features <external_features>
   FAQ <FAQ>

.. toctree::
   :maxdepth: 1
   :caption: Tips

   Disk space isssue <handle_disk_space>
   Access current development <access_current_dev>
   Produce the chain graph <produce_chain_graph>
   
.. toctree::
   :maxdepth: 2
   :caption: Developer documentation

   iota2 Workflow <iota2_workflow>
   iota2 scheduling <i2_scheduling>
   Development recommendations <develop_reco>
   
   Configuration parameters <all_parameters>
   API <api>

iota2 is still under development. To see the progress, discuss with the development team or view the code sources go to https://framagit.org/iota2-project/iota2/

