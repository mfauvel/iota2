### Procedure to generate IOTA²'s documentation

Note: the documentation is built everyday by [this cron job](https://framagit.org/iota2-project/iota2-devtools/-/blob/main/crontab/build_doc.sh).  
Note: all the build requirements are available in the iota2 conda package.

1 - Build the documentation to HTML pages
```bash
sphinx-build doc/source doc/build
```

2 - Serve the website to visit locally
```bash
python -m http.server -d doc/build/
```

3 - Update public version
```bash
rsync -cvrl --delete doc/build/ user@docs.iota2.net:/var/www/docs.iota2.net/
```