#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest running_Tests_vectorization
"""
Tests dedicated to launch iota2 runs
"""
import os
import sys
import shutil
import logging
import unittest
from config import Config

from iota2.Iota2 import run
from iota2.Common.FileUtils import FileSearch_AND
from iota2.VectorTools.vector_functions import getFields
import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
from iota2.Tests.UnitTests.tests_utils.tests_utils_launcher import run_cmd

IOTA2DIR = os.environ.get('IOTA2DIR')

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")

# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True

IOTA2_SCRIPTS = os.path.join(IOTA2DIR, "iota2")
sys.path.append(IOTA2_SCRIPTS)

LOGGER = logging.getLogger("distributed.worker")


class iota_tests_runs_case_vectorization(unittest.TestCase):
    """
    Tests dedicated to launch iota2 runs
    """
    # before launching tests
    @classmethod
    def setUpClass(cls):
        # definition of local variables
        cls.group_test_name = "iota_tests_runs_case_vectorization"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []

        # input data
        cls.config_ref = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "i2_config_vecto.cfg")
        cls.nomenclature_vecto = os.path.join(IOTA2DIR, "data", "references",
                                              "running_iota2",
                                              "nomenclature.cfg")
        cls.clip_file = os.path.join(IOTA2DIR, "data", "references",
                                     "running_iota2", "fake_region.shp")
        cls.nomenclature_path = os.path.join(IOTA2DIR, "data", "references",
                                             "running_iota2",
                                             "nomenclature.txt")
        cls.ground_truth_path = os.path.join(IOTA2DIR, "data", "references",
                                             "running_iota2",
                                             "ground_truth.shp")
        cls.color_path = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "color.txt")
        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory, ignore_errors=True)
        os.mkdir(cls.iota2_tests_directory)

    @classmethod
    def tearDownClass(cls):
        """after launching all tests"""
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory, ignore_errors=True)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory, ignore_errors=True)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        """list2reason"""
        out = None
        if exc_list and exc_list[-1][0] is self:
            out = exc_list[-1][1]
        return out

    def tearDown(self):
        """after launching a test, remove test's data if test succeed"""
        result = self.defaultTestResult()
        self._feedErrorsToResult(result, self._outcome.errors)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok_test = not error and not failure

        self.all_tests_ok.append(ok_test)
        if ok_test:
            shutil.rmtree(self.test_working_directory, ignore_errors=True)

    def test_classification_then_vectorization(self):
        """test : merge classification and vectorization workflow
        """

        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_run_results")
        fake_s2_theia_dir = os.path.join(self.test_working_directory,
                                         "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0)
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_l2a_classif_vecto.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_path = fake_s2_theia_dir
        cfg_test.simplification.gridsize = 2
        cfg_test.chain.data_field = "code"
        cfg_test.chain.spatial_resolution = 10
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        cfg_test.builders.builders_class_name = [
            "i2_classification", "i2_vectorization"
        ]
        cfg_test.simplification.nomenclature = self.nomenclature_vecto
        cfg_test.simplification.outprefix = "departement_"
        cfg_test.save(open(config_test, 'w'))

        # run iota2
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 33 -scheduler_type local"
        run_cmd(cmd)

        # asserts -> check if files exists

        # classification final products
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

        # vectorization final products
        expected_vecto_files = [
            "departement_1.prj", "departement_1.shx", "departement_1.dbf",
            "departement_1.shp", "departement_1.zip"
        ]
        for vecto_file_name in expected_vecto_files:
            expected_file = os.path.join(
                os.path.join(running_output_path, "vectors", vecto_file_name))
            self.assertTrue(
                os.path.exists(expected_file),
                msg=f"the expected file {expected_file} is missing")
