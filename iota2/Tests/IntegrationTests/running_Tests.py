#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest running_Tests
"""
Tests dedicated to launch iota2 runs
"""
import os
import sys
import shutil
import logging
import unittest
import numpy as np
from iota2.Common import i2_constants as i2_const
from iota2.Tests.UnitTests.tests_utils.tests_utils_launcher import run_cmd
import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR

I2_CONST = i2_const.iota2_constants()

IOTA2DIR = os.environ.get('IOTA2DIR')

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")

# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True

IOTA2_SCRIPTS = os.path.join(IOTA2DIR, "iota2")
sys.path.append(IOTA2_SCRIPTS)

LOGGER = logging.getLogger("distributed.worker")


class iota_tests_runs_case(unittest.TestCase):
    """
    Tests dedicated to launch iota2 runs
    """
    # before launching tests
    @classmethod
    def setUpClass(cls):
        # definition of local variables
        cls.group_test_name = "iota_tests_runs_case"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []

        # input data
        cls.config_ref = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "i2_config.cfg")
        cls.config_ref_keep = os.path.join(IOTA2DIR, "data", "references",
                                           "running_iota2", "i2_builder.cfg")

        cls.config_ref_scikit = os.path.join(IOTA2DIR, "data", "references",
                                             "running_iota2",
                                             "i2_config_scikit.cfg")
        cls.ground_truth_path = os.path.join(IOTA2DIR, "data", "references",
                                             "running_iota2",
                                             "ground_truth.shp")
        cls.region_path = os.path.join(IOTA2DIR, "data", "references",
                                       "running_iota2", "region.shp")
        cls.nomenclature_path = os.path.join(IOTA2DIR, "data", "references",
                                             "running_iota2",
                                             "nomenclature.txt")
        cls.color_path = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "color.txt")
        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory, ignore_errors=True)
        os.mkdir(cls.iota2_tests_directory)

    @classmethod
    def tearDownClass(cls):
        """after launching all tests"""
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory, ignore_errors=True)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory, ignore_errors=True)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        """list2reason"""
        out = None
        if exc_list and exc_list[-1][0] is self:
            out = exc_list[-1][1]
        return out

    def tearDown(self):
        """after launching a test, remove test's data if test succeed"""
        result = self.defaultTestResult()
        self._feedErrorsToResult(result, self._outcome.errors)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok_test = not error and not failure

        self.all_tests_ok.append(ok_test)
        if ok_test:
            shutil.rmtree(self.test_working_directory, ignore_errors=True)

    def test_multi_model_s2_theia_run(self):
        """
        Tests iota2's run using s2 (theia format)
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        from iota2.VectorTools.vector_functions import getFieldElement
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_s2_theia_dir = os.path.join(self.test_working_directory,
                                         "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0)
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_path = fake_s2_theia_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.spatial_resolution = 10
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.region_field = "region"
        cfg_test.chain.region_path = self.region_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        # asserts
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

        # check vector content
        formatting_vector = FileSearch_AND(
            os.path.join(running_output_path, "formattingVectors"), True,
            "T31TCJ.shp")
        self.assertTrue(formatting_vector)
        region_content = getFieldElement(formatting_vector[0],
                                         field="region",
                                         mode="all",
                                         elemType="str")
        code_content = getFieldElement(formatting_vector[0],
                                       field=I2_CONST.re_encoding_label_name,
                                       mode="all",
                                       elemType="str")
        tile_origin_content = getFieldElement(formatting_vector[0],
                                              field="tile_o",
                                              mode="all",
                                              elemType="str")
        # check if all regions are castable in integer
        for elem in region_content:
            try:
                cast_int = int(elem)
            except:
                self.assertTrue(
                    False,
                    msg=
                    f"missformatted content in {formatting_vector}, 'region' column contains {elem}, not castable in integer"
                )

        # check if all lables are castable in integer
        for elem in code_content:
            try:
                cast_int = int(elem)
            except:
                self.assertTrue(
                    False,
                    msg=
                    f"missformatted content in {formatting_vector}, 'code' column contains {elem}, not castable in integer"
                )

        # check if all lables are castable in integer
        for elem in tile_origin_content:
            self.assertTrue(
                elem == "T31TCJ",
                msg=
                f"missformatted content in {formatting_vector}, 'tile_o' column contains {elem} different from T31TCJ"
            )

    # Tests definitions
    def test_data_augmentation_s2_theia_run(self):
        """
        Tests iota2's run using s2 (theia format)
        and perform data augmentation algorithm
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_s2_theia_dir = os.path.join(self.test_working_directory,
                                         "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0)
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_path = fake_s2_theia_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.spatial_resolution = 10
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path

        sample_augmentation = {
            "target_models": ["1"],
            "strategy": "jitter",
            "strategy.jitter.stdfactor": 10,
            "samples.strategy": "balance",
            "activate": True
        }
        cfg_test.arg_train.sample_augmentation = sample_augmentation

        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_s2_theia_run(self):
        """
        Tests iota2's run using s2 (theia format)
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_s2_theia_dir = os.path.join(self.test_working_directory,
                                         "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0)
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_path = fake_s2_theia_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.spatial_resolution = 10
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_s2_s2c_run(self):
        """
        Tests iota2's run using s2 (peps format)
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        mtd_files = [
            os.path.join(IOTA2DIR, "data", "MTD_MSIL2A_20190501.xml"),
            os.path.join(IOTA2DIR, "data", "MTD_MSIL2A_20190506.xml")
        ]
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_s2_s2c_dir = os.path.join(self.test_working_directory,
                                       "s2_s2c_data")

        TUR.generate_fake_s2_s2c_data(
            fake_s2_s2c_dir,
            tile_name,
            mtd_files,
            res=10.0,
            fake_raster=TUR.fun_array("iota2_binary"),
            fake_scene_classification=1 + TUR.fun_array("iota2_binary") * 2,
            origin_x=566377.0,
            origin_y=6284029.0,
            epsg_code=2154)

        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_s2c.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_s2c_path = fake_s2_s2c_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.spatial_resolution = 10
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 19 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_l8_run(self):
        """
        Tests iota2's run using l8 (theia format)
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_l8_theia_dir = os.path.join(self.test_working_directory,
                                         "l8_data")
        TUR.generate_fake_l8_data(fake_l8_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=30.0)
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_l8.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.l8_path = fake_l8_theia_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        cfg_test.chain.spatial_resolution = [30, 30]
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_l8_old_run(self):
        """
        Tests iota2's run using l8 (peps format)
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "D0005H0002"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_l8_old_dir = os.path.join(self.test_working_directory,
                                       "l8_old_data")
        TUR.generate_fake_l8_old_data(fake_l8_old_dir,
                                      tile_name,
                                      ["20200101", "20200112", "20200127"],
                                      res=30.0)
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_l8_old.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.l8_path_old = fake_l8_old_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        cfg_test.chain.spatial_resolution = [30, 30]
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_l5_old_run(self):
        """
        Tests iota2's run using l5 (peps format)
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "D0005H0002"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_l5_old_dir = os.path.join(self.test_working_directory,
                                       "l5_old_data")
        TUR.generate_fake_l5_old_data(fake_l5_old_dir,
                                      tile_name,
                                      ["20200101", "20200112", "20200127"],
                                      res=30.0)
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_l5_old.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.l5_path_old = fake_l5_old_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        cfg_test.chain.spatial_resolution = [30, 30]
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_s2_l3a_run(self):
        """
        Tests iota2's run using s2_l3a (theia format)
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_s2_l3a_dir = os.path.join(self.test_working_directory, "s2_data")
        TUR.generate_fake_s2_l3a_data(fake_s2_l3a_dir,
                                      tile_name,
                                      ["20200101", "20200112", "20200127"],
                                      res=10.0)
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_l3a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_l3a_path = fake_s2_l3a_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.spatial_resolution = 10
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_s2_theia_custom_features_run(self):
        """
        Tests iota2's run using s2 (theia format) 
        and custom features
        """
        import pandas as pd
        import sqlite3
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR

        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_s2_theia_dir = os.path.join(self.test_working_directory,
                                         "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0,
                                  array_name="ones")
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_path = fake_s2_theia_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.spatial_resolution = 10
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        # cfg_test.external_features.module = os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.external_features.functions = "test_index test_index_sum"
        cfg_test.python_data_managing.number_of_chunks = 1
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        expected_colums = [
            'ogc_fid', 'geometry', 'i2label', 'region', 'code', 'originfid',
            'tile_o', 'sentinel2_b2_20200101', 'sentinel2_b3_20200101',
            'sentinel2_b4_20200101', 'sentinel2_b5_20200101',
            'sentinel2_b6_20200101', 'sentinel2_b7_20200101',
            'sentinel2_b8_20200101', 'sentinel2_b8a_20200101',
            'sentinel2_b11_20200101', 'sentinel2_b12_20200101',
            'sentinel2_b2_20200111', 'sentinel2_b3_20200111',
            'sentinel2_b4_20200111', 'sentinel2_b5_20200111',
            'sentinel2_b6_20200111', 'sentinel2_b7_20200111',
            'sentinel2_b8_20200111', 'sentinel2_b8a_20200111',
            'sentinel2_b11_20200111', 'sentinel2_b12_20200111',
            'sentinel2_b2_20200121', 'sentinel2_b3_20200121',
            'sentinel2_b4_20200121', 'sentinel2_b5_20200121',
            'sentinel2_b6_20200121', 'sentinel2_b7_20200121',
            'sentinel2_b8_20200121', 'sentinel2_b8a_20200121',
            'sentinel2_b11_20200121', 'sentinel2_b12_20200121',
            'sentinel2_ndvi_20200101', 'sentinel2_ndvi_20200111',
            'sentinel2_ndvi_20200121', 'sentinel2_ndwi_20200101',
            'sentinel2_ndwi_20200111', 'sentinel2_ndwi_20200121',
            'sentinel2_brightness_20200101', 'sentinel2_brightness_20200111',
            'sentinel2_brightness_20200121', 'custfeat_1_b1', 'custfeat_1_b2',
            'custfeat_1_b3', 'custfeat_2_b1'
        ]

        con = sqlite3.connect(
            os.path.join(running_output_path, "learningSamples",
                         "Samples_region_1_seed0_learn.sqlite"))
        df = pd.read_sql_query("SELECT * from output", con)
        columns = df.columns

        self.assertTrue(all(k == j for k, j in zip(columns, expected_colums)))

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_s2_scikit_learn(self):
        """
        Test iota2's run using s2 (theia format) and scikit-learn workflow
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_s2_theia_dir = os.path.join(self.test_working_directory,
                                         "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0,
                                  array_name="ones")
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_l2a_scikit_learn.cfg")
        shutil.copy(self.config_ref_scikit, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_path = fake_s2_theia_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        cfg_test.chain.spatial_resolution = 10
        cfg_test.scikit_models_parameters.standardization = False
        cfg_test.scikit_models_parameters.model_type = "RandomForestClassifier"
        cfg_test.scikit_models_parameters.min_samples_split = 25
        cfg_test.scikit_models_parameters.random_state = 0
        cfg_test.python_data_managing.number_of_chunks = 1
        cfg_test.scikit_models_parameters.cross_validation_parameters = {
            'n_estimators': [50, 100, 150]
        }
        cfg_test.scikit_models_parameters.cross_validation_grouped = True
        cfg_test.scikit_models_parameters.cross_validation_folds = 2
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(FileSearch_AND(
            os.path.join(running_output_path, "final"), True,
            "Classif_Seed_0_ColorIndexed.tif"),
                        msg="Classif_Seed_0_ColorIndexed.tif is missing")
        self.assertTrue(FileSearch_AND(
            os.path.join(running_output_path, "final"), True,
            "Classif_Seed_0.tif"),
                        msg="Classif_Seed_0.tif is missing")
        self.assertTrue(FileSearch_AND(
            os.path.join(running_output_path, "final"), True,
            "Confidence_Seed_0.tif"),
                        msg="Confidence_Seed_0.tif is missing")
        self.assertTrue(FileSearch_AND(
            os.path.join(running_output_path, "final"), True,
            "Confusion_Matrix_Classif_Seed_0.png"),
                        msg="Confusion_Matrix_Classif_Seed_0.png is missing")
        self.assertTrue(FileSearch_AND(
            os.path.join(running_output_path, "final"), True,
            "diff_seed_0.tif"),
                        msg="diff_seed_0.tif")
        self.assertTrue(FileSearch_AND(
            os.path.join(running_output_path, "final"), True,
            "PixelsValidity.tif"),
                        msg="PixelsValidity.tif is missing")
        self.assertTrue(FileSearch_AND(
            os.path.join(running_output_path, "final"), True, "RESULTS.txt"),
                        msg="RESULTS.txt")

    def test_s2_s2c_custom_features_run(self):
        """
        Tests iota2's run using s2 (S2C format)
        and custom features
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results_cust_s2s2c")
        mtd_files = [
            os.path.join(IOTA2DIR, "data", "MTD_MSIL2A_20190501.xml"),
            os.path.join(IOTA2DIR, "data", "MTD_MSIL2A_20190506.xml")
        ]
        fake_s2_s2c_dir = os.path.join(self.test_working_directory,
                                       "s2_s2c_data")

        TUR.generate_fake_s2_s2c_data(fake_s2_s2c_dir,
                                      tile_name,
                                      mtd_files,
                                      res=10.0,
                                      fake_raster=TUR.fun_array("ones"),
                                      fake_scene_classification=1 +
                                      TUR.fun_array("ones") * 2,
                                      origin_x=566377.0,
                                      origin_y=6284029.0,
                                      epsg_code=2154)

        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_s2c.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_s2c_path = fake_s2_s2c_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        cfg_test.chain.spatial_resolution = 10
        # cfg_test.external_features.module = os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.external_features.functions = ("test_index_s2_s2c "
                                                "test_index_sum_s2_s2c")
        cfg_test.python_data_managing.number_of_chunks = 1
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_l5_old_custom_features_run(self):
        """
        Tests iota2's run using Landsat5 (old format)
        and custom features
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "D0005H0002"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_l5_old_dir = os.path.join(self.test_working_directory,
                                       "l5_old_data")
        TUR.generate_fake_l5_old_data(fake_l5_old_dir,
                                      tile_name,
                                      ["20200101", "20200112", "20200127"],
                                      res=30.0,
                                      array_name="ones")
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_l5_old.cfg")

        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.l5_path_old = fake_l5_old_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        # cfg_test.external_features.module = os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.external_features.functions = ("test_index_l5_old "
                                                "test_index_sum_l5_old")
        cfg_test.chain.spatial_resolution = [30, 30]
        cfg_test.python_data_managing.number_of_chunks = 1
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_l8_old_custom_features_run(self):
        """
        Tests iota2's run using s2 (theia format) 
        and custom features
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "D0005H0002"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_l8_old_dir = os.path.join(self.test_working_directory,
                                       "l8_old_data")
        TUR.generate_fake_l8_old_data(fake_l8_old_dir,
                                      tile_name,
                                      ["20200101", "20200112", "20200127"],
                                      res=30.0,
                                      array_name="ones")
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_l8_old.cfg")

        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.l8_path_old = fake_l8_old_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        # cfg_test.external_features.module = os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.external_features.functions = ("test_index_l8_old"
                                                " test_index_sum_l8_old")
        cfg_test.chain.spatial_resolution = [30, 30]
        cfg_test.python_data_managing.number_of_chunks = 1
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_l8_custom_features_run(self):
        """
        Tests iota2's run using Landsat8 (theia format)
        and custom features
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_l8_theia_dir = os.path.join(self.test_working_directory,
                                         "l8_data")
        TUR.generate_fake_l8_data(fake_l8_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=30.0,
                                  array_name="ones")
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_l8.cfg")

        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.l8_path = fake_l8_theia_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        # cfg_test.external_features.module = os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.external_features.functions = (
            "test_index_l8 test_index_sum_l8")
        cfg_test.chain.spatial_resolution = [30, 30]
        cfg_test.python_data_managing.number_of_chunks = 1
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_s2_l3a_custom_features_run(self):
        """
        Tests iota2's run using s2 (L3A format) 
        and custom features
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_s2_l3a_dir = os.path.join(self.test_working_directory, "s2_data")
        TUR.generate_fake_s2_l3a_data(fake_s2_l3a_dir,
                                      tile_name,
                                      ["20200101", "20200112", "20200127"],
                                      res=10.0,
                                      array_name="ones")
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_l3a.cfg")

        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_l3a_path = fake_s2_l3a_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path

        cfg_test.chain.spatial_resolution = 10
        # cfg_test.external_features.module = os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.external_features.functions = ("test_index_s2_l3a "
                                                "test_index_sum_s2_l3a")
        cfg_test.python_data_managing.number_of_chunks = 1
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_s2_theia_custom_features_concat_test_run(self):
        """
        Tests iota2's run using s2 (theia format) 
        and custom features
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_s2_theia_dir = os.path.join(self.test_working_directory,
                                         "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0,
                                  array_name="ones")
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_path = fake_s2_theia_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.spatial_resolution = 10
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        # cfg_test.external_features.module = os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.external_features.functions = "test_index test_index_sum"
        cfg_test.python_data_managing.number_of_chunks = 1
        cfg_test.external_features.concat_mode = False
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_s2_theia_run_keep_bands(self):
        """
        Tests iota2's run using s2 (theia format)
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_s2_theia_dir = os.path.join(self.test_working_directory,
                                         "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0)
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref_keep, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_path = fake_s2_theia_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.spatial_resolution = [10, 10]
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        cfg_test.Sentinel_2.keep_bands = ["B2", "B3"]
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_s2_theia_custom_features_raw_and_exo_test_run(self):
        """
        Tests iota2's run using s2 (theia format) 
        and custom features
        """
        import sqlite3
        import pandas as pd
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_s2_theia_dir = os.path.join(self.test_working_directory,
                                         "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0,
                                  array_name="ones")
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_path = fake_s2_theia_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.spatial_resolution = 10
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        # cfg_test.external_features.module = os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test.python_data_managing.number_of_chunks = 1
        cfg_test.external_features.concat_mode = False
        cfg_test.external_features.functions = (
            "get_raw_data get_soi use_raw_data use_exogeneous_data")
        cfg_test.external_features.exogeneous_data = os.path.join(
            IOTA2DIR, "data", "numpy_features", "fake_exogeneous_data.tif")
        cfg_test.python_data_managing.data_mode_access = "both"
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)

        con = sqlite3.connect(
            os.path.join(running_output_path, "learningSamples",
                         "Samples_region_1_seed0_learn.sqlite"))
        df = pd.read_sql_query("SELECT * from output", con)
        columns = df.columns

        expected_colums = [
            'ogc_fid', 'geometry', 'i2label', 'region', 'code', 'originfid',
            'tile_o', 'sentinel2_b2_20200101', 'sentinel2_b3_20200101',
            'sentinel2_b4_20200101', 'sentinel2_b5_20200101',
            'sentinel2_b6_20200101', 'sentinel2_b7_20200101',
            'sentinel2_b8_20200101', 'sentinel2_b8a_20200101',
            'sentinel2_b11_20200101', 'sentinel2_b12_20200101',
            'sentinel2_b2_20200112', 'sentinel2_b3_20200112',
            'sentinel2_b4_20200112', 'sentinel2_b5_20200112',
            'sentinel2_b6_20200112', 'sentinel2_b7_20200112',
            'sentinel2_b8_20200112', 'sentinel2_b8a_20200112',
            'sentinel2_b11_20200112', 'sentinel2_b12_20200112',
            'sentinel2_b2_20200127', 'sentinel2_b3_20200127',
            'sentinel2_b4_20200127', 'sentinel2_b5_20200127',
            'sentinel2_b6_20200127', 'sentinel2_b7_20200127',
            'sentinel2_b8_20200127', 'sentinel2_b8a_20200127',
            'sentinel2_b11_20200127', 'sentinel2_b12_20200127',
            'sentinel2_mask_20200101', 'sentinel2_mask_20200112',
            'sentinel2_mask_20200127', 'soi_1', 'soi_2', 'soi_3',
            'custfeat_3_b1', 'custfeat_3_b2', 'custfeat_3_b3', 'exo_20200101',
            'exo_20200111', 'exo_20200121'
        ]

        self.assertTrue(all(k == j for k, j in zip(columns, expected_colums)))

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))

    def test_s2_theia_split_model_run(self):
        """
        Tests iota2's run using s2 (theia format) split models
        """
        from config import Config
        from iota2.Common.FileUtils import FileSearch_AND
        import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_s2_theia_dir = os.path.join(self.test_working_directory,
                                         "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0)
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_path = fake_s2_theia_dir
        cfg_test.chain.data_field = "code"
        cfg_test.chain.spatial_resolution = 10
        cfg_test.chain.list_tile = tile_name
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        cfg_test.chain.region_field = "region"
        cfg_test.chain.region_path = self.region_path
        cfg_test.arg_train.mode_outside_regionsplit = 0.01
        cfg_test.arg_train.random_seed = 1
        cfg_test.arg_classification.classif_mode = "fusion"

        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -scheduler_type local"
        run_cmd(cmd)

        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0_ColorIndexed.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Classif_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confidence_Seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "Confusion_Matrix_Classif_Seed_0.png"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "diff_seed_0.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "PixelsValidity.tif"))
        self.assertTrue(
            FileSearch_AND(os.path.join(running_output_path, "final"), True,
                           "RESULTS.txt"))
        classif = TUR.rasterToArray(
            os.path.join(running_output_path, "final", "Classif_Seed_0.tif"))
        print(np.sum(classif))
        self.assertTrue(np.sum(classif) > 0)
