#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest running_Tests_vectorization
"""
Tests dedicated to launch iota2 runs
"""
import os
import sys
import shutil
import logging
import unittest
from config import Config

from iota2.Iota2 import run
from iota2.Common.FileUtils import FileSearch_AND
from iota2.VectorTools.vector_functions import getFields
from iota2.Tests.UnitTests.tests_utils.tests_utils_launcher import run_cmd
from iota2.Tests.UnitTests.tests_utils.tests_utils_rasters import generate_fake_classif
from iota2.Tests.UnitTests.tests_utils.tests_utils_rasters import generate_range_raster

IOTA2DIR = os.environ.get('IOTA2DIR')

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")

# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True

IOTA2_SCRIPTS = os.path.join(IOTA2DIR, "iota2")
sys.path.append(IOTA2_SCRIPTS)

LOGGER = logging.getLogger("distributed.worker")


class iota_tests_runs_case_vectorization(unittest.TestCase):
    """
    Tests dedicated to launch iota2 runs
    """
    # before launching tests
    @classmethod
    def setUpClass(cls):
        # definition of local variables
        cls.group_test_name = "iota_tests_runs_case_vectorization"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []

        # input data
        cls.config_ref = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "i2_config_vecto.cfg")
        cls.nomenclature_vecto = os.path.join(IOTA2DIR, "data", "references",
                                              "running_iota2",
                                              "nomenclature.cfg")
        cls.clip_file = os.path.join(IOTA2DIR, "data", "references",
                                     "running_iota2", "fake_region.shp")

        cls.zonal_vector = os.path.join(IOTA2DIR, "data", "references",
                                     "running_iota2", "fake_zonal.shp")        
        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory, ignore_errors=True)
        os.mkdir(cls.iota2_tests_directory)

    @classmethod
    def tearDownClass(cls):
        """after launching all tests"""
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory, ignore_errors=True)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory, ignore_errors=True)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        """list2reason"""
        out = None
        if exc_list and exc_list[-1][0] is self:
            out = exc_list[-1][1]
        return out

    def tearDown(self):
        """after launching a test, remove test's data if test succeed"""
        result = self.defaultTestResult()
        self._feedErrorsToResult(result, self._outcome.errors)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok_test = not error and not failure

        self.all_tests_ok.append(ok_test)
        if ok_test:
            shutil.rmtree(self.test_working_directory, ignore_errors=True)

    def test_vectorization_with_clipfile(self):
        """test vectorization workflow with a clip file
        """

        # prepare test inputs
        fake_classif = os.path.join(self.test_working_directory,
                                    "fake_classif.tif")
        fake_confidence = os.path.join(self.test_working_directory,
                                       "fake_confidence.tif")
        fake_validity = os.path.join(self.test_working_directory,
                                     "fake_validity.tif")
        generate_fake_classif(fake_classif)
        generate_range_raster(fake_confidence)
        generate_range_raster(fake_validity)

        test_output_path = os.path.join(self.test_working_directory,
                                        "vecto_clip")
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_vecto_clipfile.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = test_output_path
        cfg_test.simplification.gridsize = 2
        cfg_test.simplification.classification = fake_classif
        cfg_test.simplification.confidence = fake_confidence
        cfg_test.simplification.validity = fake_validity
        cfg_test.simplification.clipfile = self.clip_file
        cfg_test.simplification.clipfield = "region"
        cfg_test.simplification.nomenclature = self.nomenclature_vecto
        cfg_test.simplification.prod = "oso"
        cfg_test.simplification.outprefix = "departement_"                
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        run(config_test, None, "local", 1, 15, False, None, [], False, 1, None)

        # asserts
        # check if the resulting file exists
        vecto_result_list = FileSearch_AND(
            os.path.join(test_output_path, "vectors"), True, "departement",
            ".shp")
        self.assertTrue(len(vecto_result_list) == 2)

        # check if file content is ok (list of fields)
        expected_fields_list = [
            'Classe', 'Validmean', 'Validstd', 'Confidence', 'UrbainDens',
            'UrbainDiff', 'ZoneIndCom', 'Routes', 'Colza', 'CerealPail',
            'Proteagine', 'Soja', 'Tournesol', 'Mais', 'Riz', 'TuberRacin',
            'Prairie', 'Vergers', 'Vignes', 'Feuillus', 'Coniferes', 'Pelouse',
            'Landes', 'SurfMin', 'PlageDune', 'GlaceNeige', 'Eau', 'Aire'
        ]
        for vecto_result in vecto_result_list:
            test_vecto_fields = getFields(vecto_result)
            self.assertTrue(test_vecto_fields == expected_fields_list)

    def test_vectorization_without_clipfile(self):
        """test vectorization workflow without a clip file
        """

        # prepare test inputs
        fake_classif = os.path.join(self.test_working_directory,
                                    "fake_classif.tif")
        fake_confidence = os.path.join(self.test_working_directory,
                                       "fake_confidence.tif")
        fake_validity = os.path.join(self.test_working_directory,
                                     "fake_validity.tif")
        generate_fake_classif(fake_classif)
        generate_range_raster(fake_confidence)
        generate_range_raster(fake_validity)

        test_output_path = os.path.join(self.test_working_directory,
                                        "vecto_no_clip")
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_vecto_no_clipfile.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = test_output_path
        cfg_test.simplification.gridsize = 2
        cfg_test.simplification.classification = fake_classif
        cfg_test.simplification.confidence = fake_confidence
        cfg_test.simplification.validity = fake_validity
        cfg_test.simplification.nomenclature = self.nomenclature_vecto
        cfg_test.simplification.prod = "oso"
        cfg_test.simplification.outprefix = "departement_"                        
        cfg_test.save(open(config_test, 'w'))
        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 25 -scheduler_type local"
        run_cmd(cmd)

        # asserts
        # check if the resulting file exists
        vecto_result_list = FileSearch_AND(
            os.path.join(test_output_path, "vectors"), True,
            "departement_1.shp")
        self.assertTrue(len(vecto_result_list) == 1)
        vecto_result = vecto_result_list[0]

        # check if file content is ok (list of fields)
        expected_fields_list = [
            'Classe', 'Validmean', 'Validstd', 'Confidence', 'UrbainDens',
            'UrbainDiff', 'ZoneIndCom', 'Routes', 'Colza', 'CerealPail',
            'Proteagine', 'Soja', 'Tournesol', 'Mais', 'Riz', 'TuberRacin',
            'Prairie', 'Vergers', 'Vignes', 'Feuillus', 'Coniferes', 'Pelouse',
            'Landes', 'SurfMin', 'PlageDune', 'GlaceNeige', 'Eau', 'Aire'
        ]
        test_vecto_fields = getFields(vecto_result)
        self.assertTrue(test_vecto_fields == expected_fields_list)

    def test_vectorization_without_grid(self):
        """test vectorization workflow without a clip file
        """

        # prepare test inputs
        fake_classif = os.path.join(self.test_working_directory,
                                    "fake_classif.tif")
        fake_confidence = os.path.join(self.test_working_directory,
                                       "fake_confidence.tif")
        fake_validity = os.path.join(self.test_working_directory,
                                     "fake_validity.tif")
        generate_fake_classif(fake_classif)
        generate_range_raster(fake_confidence)
        generate_range_raster(fake_validity)

        test_output_path = os.path.join(self.test_working_directory,
                                        "vecto_no_grid")
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_vecto_no_grid.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = test_output_path
        cfg_test.simplification.classification = fake_classif
        cfg_test.simplification.confidence = fake_confidence
        cfg_test.simplification.validity = fake_validity
        cfg_test.simplification.nomenclature = self.nomenclature_vecto
        cfg_test.simplification.prod = "oso"
        cfg_test.simplification.outprefix = "departement_"                        
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 11 -scheduler_type local"
        run_cmd(cmd)

        # asserts
        # check if the resulting file exists
        vecto_result_list = FileSearch_AND(
            os.path.join(test_output_path, "vectors"), True,
            "departement_1.shp")
        self.assertTrue(len(vecto_result_list) == 1)
        vecto_result = vecto_result_list[0]

        # check if file content is ok (list of fields)
        expected_fields_list = [
            'Classe', 'Validmean', 'Validstd', 'Confidence', 'UrbainDens',
            'UrbainDiff', 'ZoneIndCom', 'Routes', 'Colza', 'CerealPail',
            'Proteagine', 'Soja', 'Tournesol', 'Mais', 'Riz', 'TuberRacin',
            'Prairie', 'Vergers', 'Vignes', 'Feuillus', 'Coniferes', 'Pelouse',
            'Landes', 'SurfMin', 'PlageDune', 'GlaceNeige', 'Eau', 'Aire'
        ]
        test_vecto_fields = getFields(vecto_result)
        self.assertTrue(test_vecto_fields == expected_fields_list)

    def test_generalization(self):
        """test vectorization workflow when zonal vector is provided
        """

        # prepare test inputs
        fake_classif = os.path.join(self.test_working_directory,
                                    "fake_classif.tif")
        fake_confidence = os.path.join(self.test_working_directory,
                                       "fake_confidence.tif")
        fake_validity = os.path.join(self.test_working_directory,
                                     "fake_validity.tif")
        generate_fake_classif(fake_classif)
        generate_range_raster(fake_confidence)
        generate_range_raster(fake_validity)

        test_output_path = os.path.join(self.test_working_directory,
                                        "vecto_zonal")
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_vecto_zonal.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = test_output_path
        cfg_test.simplification.classification = fake_classif
        cfg_test.simplification.confidence = fake_confidence
        cfg_test.simplification.validity = fake_validity
        cfg_test.simplification.zonal_vector = self.zonal_vector        
        cfg_test.simplification.outprefix = "departement_"
        cfg_test.simplification.nomenclature = self.nomenclature_vecto
        cfg_test.simplification.prod = "carhab"        
        cfg_test.save(open(config_test, 'w'))

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 11 -scheduler_type local"
        run_cmd(cmd)

        # asserts
        # check if the resulting file exists
        vecto_result_list = FileSearch_AND(
            os.path.join(test_output_path, "vectors"), True,
            "departement_0.shp")
        self.assertTrue(len(vecto_result_list) == 1)
        vecto_result = vecto_result_list[0]

        # check if file content is ok (list of fields)
        expected_fields_list = [
            'UrbainDens', 'UrbainDiff', 'ZoneIndCom', 'Routes', 'Colza', 'CerealPail',
            'Proteagine', 'Soja', 'Tournesol', 'Mais', 'Riz', 'TuberRacin',
            'Prairie', 'Vergers', 'Vignes', 'Feuillus', 'Coniferes', 'Pelouse',
            'Landes', 'SurfMin', 'PlageDune', 'GlaceNeige', 'Eau', 'Major', 'Pourc_majo', 'Conf_maj_m', 'Conf_maj_e', 'Conf_moy'
        ]
        test_vecto_fields = getFields(vecto_result)
        self.assertTrue(test_vecto_fields == expected_fields_list)
