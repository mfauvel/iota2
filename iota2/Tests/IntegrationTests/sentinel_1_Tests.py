#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest sensors_Tests
"""unittest sensors instances
"""
import os
import sys
import shutil
import unittest

from iota2.Common.FileUtils import ensure_dir
import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
from iota2.Tests.UnitTests.tests_utils.tests_utils_launcher import get_large_i2_data_test

IOTA2DIR = os.environ.get('IOTA2DIR')
RM_IF_ALL_OK = True


class iota2_test_sentinel1_test(unittest.TestCase):
    """Test sentinel_1
    """
    # before launching tests
    @classmethod
    def setUpClass(cls):
        get_large_i2_data_test()
        # definition of local variables
        cls.group_test_name = "iota2_test_sensors_test"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

        cls.expected_labels = [
            'sentinel1_desvv_20151231', 'sentinel1_desvh_20151231',
            'sentinel1_ascvv_20170518', 'sentinel1_ascvh_20170518'
        ]
        cls.sar_config_test = os.path.join(IOTA2DIR, "data", "references",
                                           "running_iota2",
                                           "large_i2_data_test", "s1_data",
                                           "i2_config_sar.cfg")
        cls.srtm_db = os.path.join(IOTA2DIR, "data", "references",
                                   "running_iota2", "large_i2_data_test",
                                   "s1_data", "srtm.shp")
        cls.srtm_dir = os.path.join(IOTA2DIR, "data", "references",
                                    "running_iota2", "large_i2_data_test",
                                    "s1_data", "SRTM")
        cls.geoid_file = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "large_i2_data_test",
                                      "s1_data", "egm96.grd")
        cls.acquisitions = os.path.join(IOTA2DIR, "data", "references",
                                        "running_iota2", "large_i2_data_test",
                                        "s1_data", "acquisitions")
        cls.tiles_grid_db = os.path.join(IOTA2DIR, "data", "references",
                                         "running_iota2", "large_i2_data_test",
                                         "s1_data", "Features.shp")

    # after launching tests
    @classmethod
    def tearDownClass(cls):
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # create directories
        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        """ list2reason
        """
        out = None
        if exc_list and exc_list[-1][0] is self:
            out = exc_list[-1][1]
        return out

    def tearDown(self):
        """ after launching a test, remove test's data if test succeed
        """
        if sys.version_info > (3, 4, 0):
            result = self.defaultTestResult()
            self._feedErrorsToResult(result, self._outcome.errors)
        else:
            result = getattr(self, '_outcomeForDoCleanups',
                             self._resultForDoCleanups)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        test_ok = not error and not failure

        self.all_tests_ok.append(test_ok)
        if test_ok:
            shutil.rmtree(self.test_working_directory)

    def test_instance_s1(self):
        """Tests if the class sentinel_1 can be instanciate
        """
        import configparser
        from iota2.Sensors.Sentinel_1 import sentinel_1

        # prepare data
        config_path_test_s1 = os.path.join(self.test_working_directory,
                                           "s1_config.cfg")
        S2_data = self.test_working_directory
        TUR.generate_fake_s2_data(
            self.test_working_directory,
            "T31TCJ",
            ["20200101"],
        )

        s1_output_data = os.path.join(self.test_working_directory, "tilled_s1")
        ensure_dir(s1_output_data)
        config = configparser.ConfigParser()
        config.read(self.sar_config_test)
        paths = config["Paths"]
        paths["output"] = s1_output_data
        paths["srtm"] = self.srtm_dir
        paths["geoidfile"] = self.geoid_file
        paths["s1images"] = self.acquisitions
        processing = config["Processing"]

        processing["referencesfolder"] = S2_data
        processing["srtmshapefile"] = self.srtm_db
        processing["tilesshapefile"] = self.tiles_grid_db
        processing["TemporalResolution"] = "10"

        with open(config_path_test_s1, 'w') as configfile:
            config.write(configfile)

        args = {
            "tile_name": "T31TCJ",
            "target_proj": 2154,
            "all_tiles": "T31TCJ",
            "image_directory": config_path_test_s1,
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": self.test_working_directory,
            "temporal_res": 10,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False
        }

        ensure_dir(
            os.path.join(self.test_working_directory, "features", "T31TCJ",
                         "tmp"))
        s1_sensor = sentinel_1(**args)
        (sar_features, _), features_labels = s1_sensor.get_features()
        sar_features.ExecuteAndWriteOutput()
        expected_output = sar_features.GetParameterString("out")
        self.assertTrue(os.path.exists(expected_output))
        self.assertTrue(features_labels == self.expected_labels)
