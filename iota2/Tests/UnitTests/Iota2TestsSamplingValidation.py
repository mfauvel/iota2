#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest running_Tests
"""
Tests dedicated to launch iota2 runs
"""
import os
import sys
import shutil
import logging
import unittest

from iota2.Common.Utils import run
from iota2.Tests.UnitTests.tests_utils import tests_utils_vectors as TUV
IOTA2DIR = os.environ.get('IOTA2DIR')

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")

# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True

IOTA2_SCRIPTS = os.path.join(IOTA2DIR, "iota2")
sys.path.append(IOTA2_SCRIPTS)

LOGGER = logging.getLogger("distributed.worker")


class iota_full_s2_run(unittest.TestCase):
    """
    """
    @classmethod
    def setUpClass(cls):
        # definition of local variables
        cls.group_test_name = "sampling_validation_tests"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []

        # input data
        cls.ref_data_folder = os.path.join(IOTA2DIR, "data", "references",
                                           "Runs_standards",
                                           "Usual_s2_theia_run",
                                           "test_results")
        cls.s2ref_data_folder = os.path.join(IOTA2DIR, "data", "references",
                                             "Runs_standards",
                                             "Usual_s2_theia_run", "s2_data")
        cls.config_ref = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "i2_config.cfg")
        cls.ground_truth_path = os.path.join(IOTA2DIR, "data", "references",
                                             "running_iota2",
                                             "ground_truth.shp")
        cls.nomenclature_path = os.path.join(IOTA2DIR, "data", "references",
                                             "running_iota2",
                                             "nomenclature.txt")
        cls.color_path = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "color.txt")

        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory, ignore_errors=True)
        os.mkdir(cls.iota2_tests_directory)

    @classmethod
    def tearDownClass(cls):
        """after launching all tests"""
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory, ignore_errors=True)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory, ignore_errors=True)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        """list2reason"""
        out = None
        if exc_list and exc_list[-1][0] is self:
            out = exc_list[-1][1]
        return out

    def tearDown(self):
        """after launching a test, remove test's data if test succeed"""
        result = self.defaultTestResult()
        self._feedErrorsToResult(result, self._outcome.errors)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok_test = not error and not failure

        self.all_tests_ok.append(ok_test)
        if ok_test:
            shutil.rmtree(self.test_working_directory, ignore_errors=True)

    def test_compute_stats(self):
        """
    
        """
        import xmltodict
        from iota2.Sampling import SamplesStat
        cmd = f"cp -r {self.ref_data_folder}/* {self.test_working_directory}"
        run(cmd)

        os.remove(
            os.path.join(self.test_working_directory, "samplesSelection",
                         "T31TCJ_region_1_seed_0_stats_val.xml"))
        os.path.join(self.test_working_directory, "samplesSelection",
                     "T31TCJ_region_1_seed_0_stats.xml")
        SamplesStat.samples_stats(region_seed_tile=["1", "0", "T31TCJ"],
                                  iota2_directory=self.test_working_directory,
                                  data_field="i2label",
                                  sampling_validation=True,
                                  working_directory=None)

        self.assertTrue(
            os.path.exists(
                os.path.join(self.test_working_directory, "samplesSelection",
                             "T31TCJ_region_1_seed_0_stats_val.xml")))
        with open(
                os.path.join(
                    self.test_working_directory, "samplesSelection",
                    "T31TCJ_region_1_seed_0_stats_val.xml")) as xml_run:
            dict_run = xmltodict.parse(xml_run.read())
        with open(
                os.path.join(
                    self.ref_data_folder, "samplesSelection",
                    "T31TCJ_region_1_seed_0_stats_val.xml")) as xml_ref:
            dict_ref = xmltodict.parse(xml_ref.read())
        self.assertTrue(dict_ref == dict_run)

    def test_selection(self):
        """

        """
        import sqlite3
        import geopandas as gpd
        from iota2.Sampling import SamplesSelection
        cmd = f"cp -r {self.ref_data_folder}/* {self.test_working_directory}"
        run(cmd)

        os.remove(
            os.path.join(
                self.test_working_directory, "samplesSelection",
                "T31TCJ_samples_region_1_val_seed_0_selection.sqlite"))
        SamplesSelection.samples_selection(
            model=os.path.join(self.test_working_directory, 'samplesSelection',
                               "samples_region_1_seed_0.shp"),
            working_directory=self.test_working_directory,
            output_path=self.test_working_directory,
            runs=1,
            epsg="EPSG:2154",
            masks_name="MaskCommunSL.tif",
            parameters={
                "sampler": "random",
                "strategy": "all"
            },
            data_field="i2label",
            sampling_validation=True,
            parameters_validation={
                "sampler": "random",
                "strategy": "all"
            },
            random_seed=1)
        con = sqlite3.connect(
            os.path.join(
                self.test_working_directory, "samplesSelection",
                "T31TCJ_samples_region_1_val_seed_0_selection.sqlite"))
        query = "SELECT * FROM T31TCJ_samples_region_1_val_seed_0_selection"
        df_run = gpd.read_postgis(sql=query,
                                  con=con,
                                  geom_col="geometry",
                                  crs={"init": "EPSG:2154"})
        con2 = sqlite3.connect(
            os.path.join(
                self.ref_data_folder, "samplesSelection",
                "T31TCJ_samples_region_1_val_seed_0_selection.sqlite"))

        df_ref = gpd.read_postgis(sql=query,
                                  con=con2,
                                  geom_col="geometry",
                                  crs={"init": "EPSG:2154"})

        self.assertTrue(len(df_ref.index) == len(df_run.index))

    def test_validation_extraction(self):
        """
        
        """
        from config import Config
        from iota2.Sampling import VectorSampler
        from iota2.configuration_files import read_config_file as rcf
        cmd = f"cp -r {self.ref_data_folder}/* {self.test_working_directory}"
        run(cmd)

        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = self.test_working_directory
        cfg_test.chain.s2_path = self.s2ref_data_folder
        cfg_test.chain.data_field = "code"
        cfg_test.chain.spatial_resolution = 10
        cfg_test.chain.list_tile = "T31TCJ"
        cfg_test.chain.ground_truth = self.ground_truth_path
        cfg_test.chain.nomenclature_path = self.nomenclature_path
        cfg_test.chain.color_table = self.color_path
        cfg_test.save(open(config_test, 'w'))
        folder_sample = os.path.join(self.test_working_directory,
                                     "learningSamples")
        os.remove(
            os.path.join(folder_sample,
                         "T31TCJ_region_1_seed0_Samples_learn.sqlite"))
        os.remove(
            os.path.join(folder_sample,
                         "T31TCJ_region_1_seed0_Samples_val.sqlite"))
        VectorSampler.generate_samples_simple(
            folder_sample=folder_sample,
            working_directory=folder_sample,
            train_shape=os.path.join(self.test_working_directory,
                                     "samplesSelection",
                                     "T31TCJ_selection_merge.sqlite"),
            path_wd=None,
            data_field="i2label",
            region_field="region",
            output_path=self.test_working_directory,
            runs=1,
            proj="EPSG:2154",
            sensors_parameters=rcf.iota2_parameters(
                config_test).get_sensors_parameters("T31TCJ"),
            sar_optical_post_fusion=False,
            sampling_validation=True,
            ram=128,
            w_mode=False,
            folder_features=None,
            sample_sel=None,
            mode="usually",
            custom_features=False,
            module_path=None,
            list_functions=None,
            force_standard_labels=False,
            number_of_chunks=50,
            targeted_chunk=None,
            chunk_size_x=None,
            chunk_size_y=None,
            chunk_size_mode="split_number",
            concat_mode=True,
            features_from_raw_dates=False,
            multi_param_cust={
                "enable_raw": False,
                "enable_gap": True,
                "fill_missing_dates": False,
                "all_dates_dict": None,
                "mask_valid_data": None,
                "mask_value": 0,
                "exogeneous_data": None
            })
        self.assertTrue(
            os.path.exists(
                os.path.join(folder_sample,
                             "T31TCJ_region_1_seed0_Samples_val.sqlite")))
        self.assertTrue(
            os.path.exists(
                os.path.join(folder_sample,
                             "T31TCJ_region_1_seed0_Samples_learn.sqlite")))
        self.assertTrue(
            TUV.compare_sqlite(
                os.path.join(folder_sample,
                             "T31TCJ_region_1_seed0_Samples_val.sqlite"),
                os.path.join(self.ref_data_folder, "learningSamples",
                             "T31TCJ_region_1_seed0_Samples_val.sqlite"),
                cmp_mode='table'))
