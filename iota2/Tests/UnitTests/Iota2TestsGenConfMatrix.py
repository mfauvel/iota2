#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsGenConfMatrix

import os
import sys
import shutil
import unittest
from iota2.configuration_files import read_config_file as rcf

IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True


class iota_test_gen_conf_matrix(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """ init class from unitest"""
        # definition of local variables
        cls.group_test_name = "Iota2TestsGenConfMatrix"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []

        # definition of local variables
        cls.fichier_config = os.path.join(
            IOTA2DIR, "config", "Config_4Tuiles_Multi_FUS_Confidence.cfg")
        cls.ref_data = os.path.join(IOTA2DIR, "data", "references",
                                    "GenConfMatrix")
        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

    # after launching all tests
    @classmethod
    def tearDownClass(cls):
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        if exc_list and exc_list[-1][0] is self:
            return exc_list[-1][1]

    # after launching a test, remove test's data if test succeed
    def tearDown(self):
        if sys.version_info > (3, 4, 0):
            result = self.defaultTestResult()
            self._feedErrorsToResult(result, self._outcome.errors)
        else:
            result = getattr(self, "_outcomeForDoCleanups",
                             self._resultForDoCleanups)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok = not error and not failure

        self.all_tests_ok.append(ok)
        if ok:
            shutil.rmtree(self.test_working_directory)

    def test_gen_conf_matrix(self):
        """test generate confusion matrix"""
        from iota2.Validation import GenConfusionMatrix as GCM

        # Prepare data

        pathappval = os.path.join(self.test_working_directory, "dataAppVal")
        pathclassif = os.path.join(self.test_working_directory, "classif")
        final = os.path.join(self.test_working_directory, "final")
        cmdpath = os.path.join(self.test_working_directory, "cmd")

        # test and creation of pathClassif
        if not os.path.exists(pathclassif):
            os.mkdir(pathclassif)
        if not os.path.exists(pathclassif + "/MASK"):
            os.mkdir(pathclassif + "/MASK")
        if not os.path.exists(pathclassif + "/tmpClassif"):
            os.mkdir(pathclassif + "/tmpClassif")
        # test and creation of Final
        if not os.path.exists(final):
            os.mkdir(final)
        if not os.path.exists(final + "/TMP"):
            os.mkdir(final + "/TMP")

        # test and creation of cmdPath
        if not os.path.exists(cmdpath):
            os.mkdir(cmdpath)
        if not os.path.exists(cmdpath + "/confusion"):
            os.mkdir(cmdpath + "/confusion")

        # test and creation of pathAppVal
        if not os.path.exists(pathappval):
            os.mkdir(pathappval)

        # copy input data
        src_files = os.listdir(self.ref_data + "/Input/dataAppVal")
        for file_name in src_files:
            full_file_name = os.path.join(self.ref_data + "/Input/dataAppVal",
                                          file_name)
            shutil.copy(full_file_name, pathappval)
        src_files = os.listdir(self.ref_data + "/Input/Classif/MASK")
        for file_name in src_files:
            full_file_name = os.path.join(
                self.ref_data + "/Input/Classif/MASK", file_name)
            shutil.copy(full_file_name, pathclassif + "/MASK")
        src_files = os.listdir(self.ref_data + "/Input/Classif/classif")
        for file_name in src_files:
            full_file_name = os.path.join(
                self.ref_data + "/Input/Classif/classif/", file_name)
            shutil.copy(full_file_name, pathclassif)
        src_files = os.listdir(self.ref_data + "/Input/final/TMP")
        for file_name in src_files:
            full_file_name = os.path.join(self.ref_data + "/Input/final/TMP/",
                                          file_name)
            shutil.copy(full_file_name, final + "/TMP")

        # Run test

        rcf.clearConfig()
        cfg = rcf.read_config_file(self.fichier_config)
        cfg.setParam('chain', 'output_path', self.test_working_directory)
        cfg.setParam('chain', 'list_tile', 'D0005H0002')

        data_field = 'CODE'
        in_classif = os.path.join(self.test_working_directory, "classif",
                                  "D0005H0003_model_1_confidence_seed_0.tif")

        ref_vec = os.path.join(pathappval, "D0005H0003_region_1_seed0_val.shp")
        out_csv = os.path.join(self.test_working_directory, "csv_test.csv")
        GCM.gen_conf_matrix(in_classif,
                            out_csv,
                            data_field,
                            ref_vec,
                            128,
                            labels_table={"class": "class"})
        csv_content = ("#Reference labels (rows):12\n"
                       "#Produced labels (columns):1\n"
                       "4\n")
        with open(out_csv, "r") as file_csv:
            file_csv_content = file_csv.read()

        self.assertTrue(file_csv_content == csv_content,
                        msg="csv file generation failed")
