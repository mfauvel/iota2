#!/usr/bin/env bash

if [ $1 ];then
	# uncomment the following line to get conda on HPC
	# module load conda
	env_location=$(conda info | grep 'base environment' | awk -F'[:]' '{print $2}' | awk -F'[" "]' '{print $2}')
	source $env_location/bin/activate
	conda activate $1
	python $PWD/iota2/Tests/launch_tests.py
	if [ $? -eq 0 ]; then
        exit 0
    else
        exit 1
	fi
else
	echo "launch_test.sh usage : "
	echo "launch_test.sh I2_CONDA_ENVIRONMENT_NAME"
fi
