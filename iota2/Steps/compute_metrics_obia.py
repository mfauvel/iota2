#!/usr/bin/python
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging
from iota2.Steps import IOTA2Step
from iota2.configuration_files import read_config_file as rcf
from iota2.Segmentation import prepare_segmentation_obia as pso
from iota2.VectorTools.vector_functions import get_re_encoding_labels_dic
LOGGER = logging.getLogger("distributed.worker")


class compute_metrics_obia(IOTA2Step.Step):
    resources_block_name = "compute_metrics_obia"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.working_directory = workingDirectory
        self.cfg = cfg
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.data_field = rcf.read_config_file(self.cfg).getParam(
            'chain', 'data_field')
        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        self.enable_stats = False
        self.field_region = rcf.read_config_file(self.cfg).getParam(
            'chain', 'region_field')
        self.res = rcf.read_config_file(self.cfg).getParam(
            'chain', "spatial_resolution")[0]
        reference_data = rcf.read_config_file(self.cfg).getParam(
            "chain", "ground_truth")
        reference_data_field = rcf.read_config_file(self.cfg).getParam(
            "chain", "data_field")
        old_label_to_new = get_re_encoding_labels_dic(reference_data,
                                                      reference_data_field)
        for seed in range(self.runs):
            for tile in self.tiles:
                task = self.i2_task(
                    task_name=f"compute_metrics_{tile}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": pso.compute_metrics_obia,
                        "iota2_directory": self.output_path,
                        "tile": tile,
                        "seed": seed,
                        "labels_vector_table": old_label_to_new,
                        "data_field": self.data_field,
                        "ref_label_name": self.i2_const.re_encoding_label_name,
                        "seg_field": self.i2_const.i2_segmentation_field_name
                    },
                    task_resources=self.get_resources())
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tile_tasks",
                    task_sub_group=f"{tile}_seed_{seed}",
                    task_dep_dico={"tile_tasks": [f"{tile}_seed_{seed}"]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Reassemble tiles and compute confusion matrix")
        return description

    # def step_inputs(self):
    #     """
    #     Return
    #     ------
    #         the return could be and iterable or a callable
    #     """
    #     # runs = [run for run in range(0, self.runs)]
    #     # return runs

    #     from iota2.Sampling import SamplesMerge as samples_merge
    #     region_tiles_seed = samples_merge.get_models(
    #         os.path.join(self.output_path, "formattingVectors"),
    #         self.field_region, self.nb_runs)
    #     # input(region_tiles_seed)
    #     parameters = []
    #     for region, tiles, seed in region_tiles_seed:
    #         # input(tiles)
    #         for tile in tiles:
    #             parameters.append({
    #                 "region": region,
    #                 "tile": tile,
    #                 "seed": seed,
    #                 "data_field": self.data_field,
    #                 "iota2_directory": self.output_path,
    #                 "enable_stats": self.enable_stats,
    #                 "working_directory": self.workingDirectory
    #             })
    #     return parameters

    # def step_execute(self):
    #     """
    #     Return
    #     ------
    #     lambda
    #         the function to execute as a lambda function. The returned object
    #         must be a lambda function.
    #     """
    #     from iota2.Classification import ObiaClassification as OCC
    #     # step_function = lambda x: OCC.launchObiaClassification(
    #     #     seed=x,
    #     #     nb_cpu=self.nb_cpu,
    #     #     data_field=self.data_field,
    #     #     output_path=self.output_path,
    #     #     path_wd=self.workingDirectory)
    #     # return step_function
    #     return lambda x: OCC.process_vector_classification(**x)

    # def step_outputs(self):
    #     """
    #     """
    #     pass
