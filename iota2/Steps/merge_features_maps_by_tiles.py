#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import logging

from iota2.Steps import IOTA2Step
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_RAM
from iota2.Common import write_features_map as wfm
LOGGER = logging.getLogger("distributed.worker")


class merge_features_maps_by_tiles(IOTA2Step.Step):

    resources_block_name = "mergeMapsByTiles"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.working_directory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.ram_extraction = 1024.0 * get_RAM(self.get_resources()["ram"])
        # read config file to init custom features and check validity
        self.external_features_flag = rcf.read_config_file(self.cfg).getParam(
            "external_features", "external_features_flag")
        # to allow writing maps using bandmath
        self.number_of_chunks = 0
        if self.external_features_flag:
            self.number_of_chunks = rcf.read_config_file(self.cfg).getParam(
                'python_data_managing', "number_of_chunks")
            self.chunk_size_mode = rcf.read_config_file(self.cfg).getParam(
                'python_data_managing', "chunk_size_mode")
        for tile in self.tiles:
            task = self.i2_task(task_name="merge_feat_maps_by_tiles",
                                log_dir=self.log_dir,
                                execution_mode=self.execution_mode,
                                task_parameters={
                                    "f":
                                    wfm.merge_features_by_tiles,
                                    "iota2_directory":
                                    self.output_path,
                                    "tile":
                                    tile,
                                    "res":
                                    rcf.read_config_file(self.cfg).getParam(
                                        'chain', 'spatial_resolution')
                                },
                                task_resources=self.get_resources())
            dep = []
            for i in range(self.number_of_chunks):
                dep.append(f"writing maps {tile} {i}")
            self.add_task_to_i2_processing_graph(
                task,
                task_group="merge_maps_by_tiles",
                task_sub_group=f"merge_maps_by_tiles {tile}",
                task_dep_dico={"writing maps": dep})
        # implement tests for check if custom features are well provided
        # so the chain failed during step init

    @classmethod
    def step_description(self):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Merge features chunk")
        return description
