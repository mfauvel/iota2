#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Iota2Cluster import get_RAM
from iota2.configuration_files import read_config_file as rcf

LOGGER = logging.getLogger("distributed.worker")


class simplification(IOTA2Step.Step):
    resources_block_name = "vectorization"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(simplification, self).__init__(cfg, cfg_resources_file,
                                             resources_block_name)

        # step variables
        self.RAM = 1024.0 * get_RAM(self.get_resources()["ram"])
        self.workingDirectory = workingDirectory
        self.outputPath = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.mmu = rcf.read_config_file(self.cfg).getParam(
            'simplification', 'mmu')
        self.douglas = rcf.read_config_file(self.cfg).getParam(
            'simplification', 'douglas')
        self.hermite = rcf.read_config_file(self.cfg).getParam(
            'simplification', 'hermite')
        self.angle = rcf.read_config_file(self.cfg).getParam(
            'simplification', 'angle')
        self.grasslib = rcf.read_config_file(self.cfg).getParam(
            'simplification', 'grasslib')

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Vectorisation and simplification of classification")
        return description

    def step_inputs(self):
        """
        Return
        ------
            the return could be and iterable or a callable
        """
        outfilereg = os.path.join(self.outputPath, 'final', 'simplification',
                                  'classif_regul.tif')
        return [outfilereg]

    def step_execute(self):
        """
        Return
        ------
        lambda
            the function to execute as a lambda function. The returned object
            must be a lambda function.
        """
        from iota2.simplification import vector_generalize as vas

        tmpdir = os.path.join(self.outputPath, 'final', 'simplification',
                              'tmp')
        if self.workingDirectory:
            tmpdir = self.workingDirectory
        outfilevectsimp = os.path.join(self.outputPath, 'final',
                                       'simplification', 'classif.shp')
        step_function = lambda x: vas.simplification(
            tmpdir, x, self.grasslib, outfilevectsimp, self.douglas, self.
            hermite, self.mmu, self.angle)
        return step_function

    def step_outputs(self):
        """
        """
        pass
