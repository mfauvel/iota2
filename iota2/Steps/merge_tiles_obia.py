#!/usr/bin/python
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging
from iota2.Steps import IOTA2Step
from iota2.configuration_files import read_config_file as rcf
from iota2.Segmentation import prepare_segmentation_obia as pso
LOGGER = logging.getLogger("distributed.worker")


class merge_tiles_obia(IOTA2Step.Step):
    resources_block_name = "merge_tiles_obia"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.working_directory = workingDirectory
        self.cfg = cfg
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        # self.data_field = SCF.serviceConfigFile(self.cfg).getParam(
        #     'chain', 'dataField')
        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        self.enable_stats = False
        self.field_region = rcf.read_config_file(self.cfg).getParam(
            'chain', 'region_field')
        self.res = rcf.read_config_file(self.cfg).getParam(
            'chain', "spatial_resolution")[0]

        for seed in range(self.runs):

            task = self.i2_task(task_name=f"merge_tile_{seed}",
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters={
                                    "f": pso.merge_clipped_tiles,
                                    "iota2_directory": self.output_path,
                                    "seed": seed
                                },
                                task_resources=self.get_resources())
            dep = [f"{tile}_seed_{seed}" for tile in self.tiles]
            self.add_task_to_i2_processing_graph(
                task,
                task_group="seed_tasks",
                task_sub_group=f"seed_{seed}",
                task_dep_dico={"tile_tasks": dep})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Merge all vector tiles")
        return description
