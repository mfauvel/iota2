#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.simplification import zonal_stats as zs
from iota2.Common.FileUtils import sortByFirstElem
from iota2.configuration_files import read_config_file as rcf
LOGGER = logging.getLogger("distributed.worker")

LOGGER = logging.getLogger("distributed.worker")


class ProdVectors(IOTA2Step.Step):
    resources_block_name = "prodVectors"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(ProdVectors, self).__init__(cfg, cfg_resources_file,
                                          self.resources_block_name)

        # step variables
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path")
        self.nomenclature = rcf.read_config_file(self.cfg).getParam(
            "simplification", "nomenclature")
        self.out_prefix = rcf.read_config_file(self.cfg).getParam(
            "simplification", "outprefix")
        self.prod = rcf.read_config_file(self.cfg).getParam(
            "simplification", "prod")
        tasks = self.get_tasks()
        for directory, vector_chunk_list in tasks:
            _, task_name = os.path.split(directory)
            task = self.i2_task(task_name=task_name,
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters={
                                    "f":
                                    zs.merge_sub_vector,
                                    "listofchkofzones":
                                    (directory, vector_chunk_list),
                                    "outpath":
                                    os.path.join(self.output_path, 'vectors'),
                                    "classes":
                                    self.nomenclature,
                                    "outbase":
                                    self.out_prefix,
                                    "prod":
                                    self.prod,
                                },
                                task_resources=self.get_resources())
            dep_list = []
            for vector_chunk in vector_chunk_list:
                dep = os.path.splitext(os.path.basename(vector_chunk))[0]
                dep_list.append(f"zonal_stat_{dep}")
            self.add_task_to_i2_processing_graph(
                task,
                task_group="merge_stats",
                task_sub_group=f"merge_stats_{task_name}",
                task_dep_dico={"zonal_stat": dep_list})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Merge statistics")
        return description

    def get_tasks(self, ):
        """
        """
        buff = []
        for in_vector, _, chunk_vect in self.regions_stats_partition:
            vec_name = os.path.splitext(os.path.basename(in_vector))[0]
            buff.append((os.path.join(self.output_path, "simplification",
                                      "tmp", vec_name), chunk_vect))
        return sortByFirstElem(buff)
