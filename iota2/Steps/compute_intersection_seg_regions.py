#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" The prepare segmentation for obia step"""
import os
import logging

from iota2.Steps import IOTA2Step

from iota2.Iota2Cluster import get_RAM
from iota2.configuration_files import read_config_file as rcf
from iota2.Segmentation import prepare_segmentation_obia as pso

LOGGER = logging.getLogger("distributed.worker")


class compute_intersection_seg_regions(IOTA2Step.Step):
    resources_block_name = "compute_inter_seg_reg"

    def __init__(self, cfg, cfg_resources_file, working_directory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.working_directory = working_directory
        self.RAM = 1024.0 * get_RAM(self.get_resources()["ram"])
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.spatial_res = rcf.read_config_file(self.cfg).getParam(
            "chain", "spatial_resolution")[0]
        self.region_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "region_path")
        self.region_field = rcf.read_config_file(self.cfg).getParam(
            "chain", 'region_field')
        self.region_priority = rcf.read_config_file(self.cfg).getParam(
            "obia", "region_priority")
        for tile in self.tiles:

            parameters = {
                "f": pso.compute_intersection_segmentation_regions,
                "tile": tile,
                "iota2_directory": self.output_path,
                "seg_field": self.i2_const.i2_segmentation_field_name,
                "region_priority": self.region_priority
            }
            if self.region_field is not None:
                parameters["region_field"] = self.region_field

            task = self.i2_task(task_name=f"compute_inter_sef_regs_{tile}",
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters=parameters,
                                task_resources=self.get_resources())
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tile_tasks",
                task_sub_group=tile,
                task_dep_dico={"tile_tasks": [tile]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Compute intersection between segment and region")
        return description


#prepare_initial_segmentation(in_seg, tile, iota2_directory, is_slic=False)
