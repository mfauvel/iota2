#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Iota2Cluster import get_RAM
from iota2.Validation import GenConfusionMatrix as GCM
from iota2.configuration_files import read_config_file as rcf

LOGGER = logging.getLogger("distributed.worker")


class confusionSAROpt(IOTA2Step.Step):
    resources_block_name = "SAROptConfusionMatrix"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(confusionSAROpt, self).__init__(cfg, cfg_resources_file,
                                              self.resources_block_name)

        # step variables
        self.workingDirectory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.data_field = self.i2_const.re_encoding_label_name
        self.sar_opt_conf_ram = 1024.0 * get_RAM(self.get_resources()["ram"])

        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')

        self.suffix_list = ["usually"]
        if rcf.read_config_file(self.cfg).getParam(
                'arg_train', 'dempster_shafer_sar_opt_fusion') is True:
            self.suffix_list.append("SAR")

        for suffix in self.suffix_list:
            for model_name, model_meta in self.spatial_models_distribution.items(
            ):
                for seed in range(self.runs):
                    for tile in model_meta["tiles"]:
                        task_name = f"confusion_{tile}_model_{model_name}_seed_{seed}"
                        ref_vector = os.path.join(
                            self.output_path, "dataAppVal", "bymodels",
                            f"{tile}_region_{model_name}_seed_{seed}_samples_val.shp"
                        )
                        classification = os.path.join(
                            self.output_path, "classif",
                            f"Classif_{tile}_model_{model_name}_seed_{seed}.tif"
                        )
                        if suffix == "SAR":
                            task_name = f"{task_name}_SAR"
                            classification = classification.replace(
                                ".tif", "_SAR.tif")
                        task = self.i2_task(
                            task_name=task_name,
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters={
                                "f": GCM.confusion_sar_optical,
                                "ref_vector": (ref_vector, classification),
                                "data_field": self.data_field,
                                "ram": self.sar_opt_conf_ram
                            },
                            task_resources=self.get_resources())
                        self.add_task_to_i2_processing_graph(
                            task,
                            task_group="tile_tasks_model_mode",
                            task_sub_group=
                            f"{tile}_{model_name}_{seed}_{suffix}",
                            task_dep_dico={
                                "tile_tasks_model_mode":
                                [f"{tile}_{model_name}_{seed}_{suffix}"]
                            })

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = (
            "Evaluate SAR vs optical classification's performance by tiles and models"
        )
        return description
