#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Iota2Cluster import get_RAM
from iota2.Sensors import ProcessLauncher
from iota2.configuration_files import read_config_file as rcf
LOGGER = logging.getLogger("distributed.worker")


class sensorsPreprocess(IOTA2Step.Step):
    resources_block_name = "preprocess_data"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(sensorsPreprocess, self).__init__(cfg, cfg_resources_file,
                                                self.resources_block_name)

        # step variables
        self.workingDirectory = workingDirectory
        self.RAM = 1024.0 * get_RAM(self.get_resources()["ram"])
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.step_tasks = []
        for tile in self.tiles:
            task = self.i2_task(task_name=f"preprocessing_{tile}",
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters={
                                    "f": ProcessLauncher.preprocess,
                                    "tile_name": tile,
                                    "config_path": self.cfg,
                                    "output_path": self.output_path,
                                    "working_directory": os.getenv('TMPDIR'),
                                    "RAM": self.RAM
                                },
                                task_resources=self.get_resources())
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tile_tasks",
                task_sub_group=tile,
                task_dep_dico={"first_task": []})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Sensors pre-processing")
        return description
