#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Common import IOTA2Directory as IOTA2_dir
from iota2.configuration_files import read_config_file as rcf
from iota2.Sensors.Sensors_container import sensors_container
from iota2.configuration_files.read_config_file import iota2_parameters

LOGGER = logging.getLogger("distributed.worker")


class IOTA2DirTree(IOTA2Step.Step):

    resources_block_name = "iota2_dir"

    def __init__(self, cfg, cfg_resources_file):
        # heritage init
        super(IOTA2DirTree, self).__init__(cfg, cfg_resources_file,
                                           self.resources_block_name)
        check_inputs = rcf.read_config_file(self.cfg).getParam(
            'chain', 'check_inputs')
        tiles = rcf.read_config_file(self.cfg).getParam('chain',
                                                        'list_tile').split(" ")
        output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        running_parameters = iota2_parameters(self.cfg)
        sensors_parameters = running_parameters.get_sensors_parameters(
            tiles[0])
        sensor_tile_container = sensors_container(tiles[0], None, output_path,
                                                  **sensors_parameters)

        sensor_path = sensor_tile_container.get_enabled_sensors_path()[0]
        dir_task = self.i2_task(
            task_name="generate_directories",
            log_dir=self.log_step_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f":
                IOTA2_dir.generate_directories,
                "root":
                output_path,
                "check_inputs":
                check_inputs,
                "merge_final_classifications":
                rcf.read_config_file(self.cfg).getParam(
                    'arg_classification', 'merge_final_classifications'),
                "ground_truth":
                os.path.join(output_path,
                             self.i2_const.re_encoding_label_file),
                "region_shape":
                rcf.read_config_file(self.cfg).getParam(
                    'chain', 'region_path'),
                "data_field":
                self.i2_const.re_encoding_label_name,
                "region_field":
                rcf.read_config_file(self.cfg).getParam(
                    'chain', 'region_field'),
                "epsg":
                int(
                    rcf.read_config_file(self.cfg).getParam(
                        'chain', 'proj').split(":")[-1]),
                "sensor_path":
                sensor_path,
                "tiles":
                tiles,
                "remove_output_path":
                rcf.read_config_file(self.cfg).getParam(
                    "chain", "remove_output_path"),
                "start_step":
                rcf.read_config_file(self.cfg).getParam("chain", "first_step")
            },
            task_resources=self.get_resources())
        self.add_task_to_i2_processing_graph(dir_task, "first_task")

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Construct IOTA² output directories")
        return description
