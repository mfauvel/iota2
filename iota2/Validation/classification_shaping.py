#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""module dedicated to mosaic classifications in order to produce
final maps"""
import os
import re
import shutil
import argparse
import logging
from typing import List
from typing import Dict
from typing import Union
from typing import Tuple
from typing import Optional
import numpy as np
from osgeo import gdal
# from osgeo.gdalconst import *

from iota2.Common import FileUtils as fu
from iota2.Common.rasterUtils import compress_raster
from iota2.Common.rasterUtils import re_encode_raster
from iota2.Common.FileUtils import getRasterResolution
from iota2.Common import CreateIndexedColorImage as color
from iota2.Common import OtbAppBank as otb
from iota2.VectorTools.vector_functions import getLayerName

from iota2.Common.Utils import run

LOGGER = logging.getLogger("distributed.worker")


def generate_diff_map(runs: int,
                      all_tiles: List[str],
                      data_field: str,
                      spatial_resolution: Tuple[Union[float, int], Union[float,
                                                                         int]],
                      output_path: str,
                      iota2_directory,
                      working_directory: Optional[str] = None,
                      logger=LOGGER) -> None:
    """
    produce a map of well/bad classified learning/validation pixels

    Parameters
    ----------
    runs:
        the number of seeds
    all_tiles:
        the list of all tiles processed
    path_wd:
        the working directory
    data_field:
        the name of label field
    spatial_resolution:
        the spatial resolution (res X, res Y)
    path_test:
        the output path

    """
    wdir = working_directory if working_directory else output_path
    # final_path = os.path.join(output_path, "final", "TMP")
    for seed in range(runs):
        for tile in all_tiles:
            val_tiles = fu.FileSearch_AND(
                os.path.join(iota2_directory, "dataAppVal"), True, tile,
                "_seed_" + str(seed) + "_val.sqlite")
            if not val_tiles:
                continue
            val_tile = val_tiles[0]
            learn_tile = fu.FileSearch_AND(
                os.path.join(iota2_directory, "dataAppVal"), True, tile,
                "_seed_" + str(seed) + "_learn.sqlite")[0]
            classif = os.path.join(iota2_directory, "final", "TMP",
                                   f"{tile}_seed_{seed}.tif")
            diff = os.path.join(iota2_directory, "final", "TMP",
                                f"{tile}_seed_{seed}_CompRef.tif")
            # working_directory = os.path.join(path_test, "final", "TMP")
            # if path_wd:
            #     working_directory = path_wd
            compare_ref(shape_ref=val_tile,
                        shape_learn=learn_tile,
                        classif=classif,
                        diff=diff,
                        working_directory=working_directory,
                        output_path=output_path,
                        data_field=data_field,
                        spatial_res=spatial_resolution,
                        logger=logger)

        all_diff = fu.FileSearch_AND(
            os.path.join(iota2_directory, "final", "TMP"), True,
            f"_seed_{seed}_CompRef.tif")
        diff_seed = os.path.join(iota2_directory, "final",
                                 f"diff_seed_{seed}.tif")
        if working_directory:
            diff_seed = os.path.join(wdir, f"diff_seed_{seed}.tif")
        if all_diff:
            if not spatial_resolution:
                res_x, res_y = fu.getRasterResolution(all_diff[0])
                spatial_resolution = (res_x, res_y)
            fu.assembleTile_Merge(all_diff,
                                  spatial_resolution,
                                  diff_seed,
                                  ot="Byte")
            if working_directory:
                shutil.copy(
                    wdir + f"/diff_seed_{seed}.tif",
                    os.path.join(iota2_directory, "final",
                                 f"diff_seed_{seed}.tif"))


def compare_ref(shape_ref: str,
                shape_learn: str,
                classif: str,
                diff: str,
                output_path: str,
                data_field: str,
                spatial_res: Tuple[Union[float, int], Union[float, int]],
                working_directory: Optional[str] = None,
                logger=LOGGER) -> str:
    """
    Compare the reference data with the predicted classification

    Parameters
    ----------
    shape_ref:
        validation shapefile
    shape_learn:
        learning shapefile
    classif:
        produced classification
    diff:
        output raster file
    working_directory:
        path to working directory
    output_path:
        the final output path
    data_field:
        the label column
    spatial_res:
        tuple of spatial resolution

    """
    wdir = working_directory if working_directory else output_path

    min_x, max_x, min_y, max_y = fu.getRasterExtent(classif)

    shape_raster_val = os.path.join(
        wdir,
        os.path.basename(shape_ref).replace(".sqlite", ".tif"))

    shape_raster_learn = os.path.join(
        wdir,
        os.path.basename(shape_learn).replace(".sqlite", ".tif"))
    if not spatial_res:
        res_x, res_y = fu.getRasterResolution(classif)
        spatial_res = (res_x, res_y)

    # Rasterise val
    shape_ref_table_name = getLayerName(shape_ref, "SQLite")
    cmd = (f"gdal_rasterize -l {shape_ref_table_name} -a {data_field} -init 0 "
           f"-tr {spatial_res[0]} {spatial_res[-1]} {shape_ref}"
           f" {shape_raster_val} "
           f"-te {min_x} {min_y} {max_x} {max_y}")
    run(cmd, logger=logger)
    # Rasterise learn
    shape_learn_table_name = getLayerName(shape_learn, "SQLite")
    cmd = (f"gdal_rasterize -l {shape_learn_table_name} -a {data_field} -init "
           f"0 -tr {spatial_res[0]} {spatial_res[-1]} {shape_learn}"
           f" {shape_raster_learn}"
           f" -te {min_x} {min_y} {max_x} {max_y}")
    run(cmd, logger=logger)

    # diff val

    diff_val = os.path.join(wdir,
                            os.path.basename(diff).replace(".tif", "_val.tif"))
    # reference identique -> 2  | reference != -> 1 | pas de reference -> 0
    bandmath_val = otb.CreateBandMathApplication({
        "il": [shape_raster_val, classif],
        "out":
        diff_val,
        "pixType":
        "uint8",
        "exp":
        "im1b1==0?0:im1b1==im2b1?2:1"
    })
    bandmath_val.ExecuteAndWriteOutput()
    os.remove(shape_raster_val)

    # diff learn
    diff_learn = os.path.join(
        wdir,
        os.path.basename(diff).replace(".tif", "_learn.tif"))
    # reference identique -> 4  | reference != -> 3 | pas de reference -> 0
    bandmath_learn = otb.CreateBandMathApplication({
        "il": [shape_raster_learn, classif],
        "out":
        diff_learn,
        "pixType":
        "uint8",
        "exp":
        "im1b1==0?0:im1b1==im2b1?4:3"
    })
    bandmath_learn.ExecuteAndWriteOutput()
    os.remove(shape_raster_learn)

    # sum diff val + learn
    diff_tmp = os.path.join(wdir, os.path.basename(diff))
    bandmath_sum = otb.CreateBandMathApplication({
        "il": [diff_val, diff_learn],
        "out": diff_tmp,
        "pixType": "uint8",
        "exp": "im1b1+im2b1"
    })
    bandmath_sum.ExecuteAndWriteOutput()
    os.remove(diff_val)
    os.remove(diff_learn)

    if working_directory:  # and not os.path.exists(diff):
        shutil.copy(diff_tmp, diff)
        os.remove(diff_tmp)

    return diff


def create_dummy_rasters(missing_tiles: List[str], runs: int,
                         output_path: str) -> None:
    """
    Create empty rasters 
    
    Parameters
    ----------
    missing_tiles:
        list of tiles
    runs:
        the number of random seeds
    output_path:
        the output path

    Notes
    -----
    use when mode is 'one_region' but there is no validations / learning
    samples into a specific tile
    """

    classifications_dir = os.path.join(output_path, "classif")
    final_dir = os.path.join(output_path, "final", "TMP")

    for tile in missing_tiles:
        classif_tile = fu.FileSearch_AND(classifications_dir, True,
                                         "Classif_" + str(tile))[0]
        for seed in range(runs):
            dummy_raster_name = tile + "_seed_" + str(seed) + "_CompRef.tif"
            dummy_raster = final_dir + "/" + dummy_raster_name
            dummy_raster_cmd = (f"gdal_merge.py -ot Byte -n 0 -createonly -o "
                                f"{ dummy_raster} {classif_tile}")
            run(dummy_raster_cmd)


def remove_in_list_by_regex(input_list: List[str], regex: str) -> List[str]:
    """
    Remove all element in the list that match regex

    Parameters
    ----------
    input_list:
        the list to filter
    regex:
        the regular expression
    """

    out_list = []
    for elem in input_list:
        match = re.match(regex, elem)
        if not match:
            out_list.append(elem)

    return out_list


def proba_map_fusion(proba_map_list: List[str],
                     ram: int = 128,
                     working_directory: Optional[str] = None,
                     logger=LOGGER) -> None:
    """
    Fusion of probabilities map

    Parameters
    ----------
    proba_map_list :
        list of probabilities map to merge
    ram :
        available ram in mb
    working_directory :
        working directory absolute path
    """
    from iota2.Common.OtbAppBank import CreateBandMathXApplication
    model_pos = 3

    proba_map_fus_dir, proba_map_fus_name = os.path.split(proba_map_list[0])
    proba_map_fus_name = proba_map_fus_name.split("_")
    proba_map_fus_name[model_pos] = proba_map_fus_name[model_pos].split("f")[0]
    proba_map_fus_name = "_".join(proba_map_fus_name)

    exp = "({}) dv {}".format(
        "+".join(["im{}".format(i + 1) for i in range(len(proba_map_list))]),
        len(proba_map_list))
    proba_map_fus_path = os.path.join(proba_map_fus_dir, proba_map_fus_name)
    if working_directory:
        proba_map_fus_path = os.path.join(working_directory,
                                          proba_map_fus_name)
    logger.info("Fusion of probality maps : {} at {}".format(
        proba_map_list, proba_map_fus_path))
    proba_merge = CreateBandMathXApplication({
        "il": proba_map_list,
        "ram": str(ram),
        "out": proba_map_fus_path,
        "exp": exp
    })
    proba_merge.ExecuteAndWriteOutput()
    if working_directory:
        copy_target = os.path.join(proba_map_fus_dir, proba_map_fus_name)
        logger.debug("copy {} to {}".format(proba_map_fus_path, copy_target))
        shutil.copy(proba_map_fus_path, copy_target)
        os.remove(proba_map_fus_path)


def build_confidence_cmd(final_tile: str,
                         classif_tile: List[str],
                         confidence: List[str],
                         output_confidence: str,
                         fact: int = 100,
                         pix_type: str = "uint8") -> str:
    """
    Prepare the otb command to fuse confidence

    Parameters
    ----------

    final_tile:
        merged tile classification
    classif_tile:
        the list of classification
    confidence:
        the list of confidence
    output_confidence:
        the output name for the confidence map
    fact:
        the factor to apply to confidence for storage
    pix_type:
        the pixel type used to store confidence

    Notes
    -----

    This function is used in 'fusion' classification mode.
    If the final decision is the same than the voter,
     the confidence value is added.
    If the final decision is not the same than the voter,
     1 - confidence is added
    The final confidence is divided by the number of voters

    """
    if len(classif_tile) != len(confidence):
        raise Exception(
            "number of confidence map and classifcation map must be the same")

    N = len(classif_tile)
    exp = []
    for i in range(len(classif_tile)):
        # si final ok avec votant ajout de la confiance
        # si final pas ok votant ajout de 1-confiance
        # le tout divisé par le nombre de votant
        exp.append("(im" + str(i + 2) + "b1==0?0:im1b1!=im" + str(i + 2) +
                   "b1?1-im" + str(i + 2 + N) + "b1:im" + str(i + 2 + N) +
                   "b1)")
    # im1b1==0 means image border here
    exp_confidence = "im1b1==0?0:(" + "+".join(exp) + ")/" + str(
        len(classif_tile))

    all_images = classif_tile + confidence
    all_images = " ".join(all_images)

    cmd = (f'otbcli_BandMath -ram 5120 -il {final_tile} {all_images} '
           f'-out {output_confidence} {pix_type} '
           f'-exp " {fact}*( {exp_confidence} )"')

    return cmd


def merge_confidence_in_fusion_mode(iota2_directory: str,
                                    path_to_classif: str,
                                    tmp_classif: str,
                                    tile: str,
                                    seed: int,
                                    suffix: str,
                                    suffix_sar: str,
                                    proba_map_flag: bool,
                                    probamap_pattern: str,
                                    working_directory: Optional[str] = None,
                                    logger=LOGGER) -> List[str]:
    """
    Handle the case when models are splitted

    Parameters:
    -----------

    iota2_directory:
        path to iota2 output directory
    path_to_classif:
        path where are stored classifications
    tmp_classif:
        path where are stored intermediate classifications
    tile:
        the tile name
    seed:
        the considered seed
    suffix:
        the classification suffix
    suffix_sar:
        the classification suffix if sar is enabled
    proba_map_flag:
        enable the generation of probability maps
    probamap_pattern:
        the prefix of probability maps
    working_directory:
        path to store intermediate results
    """
    wdir = working_directory if working_directory else path_to_classif
    # if model have been splitted, it exists classifications
    # with a f in the region name
    classif_tile = fu.fileSearchRegEx(
        os.path.join(path_to_classif,
                     f"Classif_{tile}*model_*f*_seed_{seed}{suffix}"))
    split_model = []
    confidence_out = []
    for classif in classif_tile:
        model = os.path.basename(classif).split("_")[3].split("f")[0]
        if model not in split_model:
            split_model.append(model)
            split_confidence = []
            confidence_all = fu.fileSearchRegEx(
                os.path.join(
                    path_to_classif, f"{tile}*model_*_confidence"
                    f"_seed_{seed}{suffix}"))
            confidence_without_split = remove_in_list_by_regex(
                confidence_all, f".*model_.*f.*_confidence.{suffix}")
            for model in split_model:
                # get all sub regions for the model selected
                # 'model'f* and not *f* in regex
                classif_tile = fu.fileSearchRegEx(
                    os.path.join(
                        path_to_classif, f"Classif_{tile}*model_{model}"
                        f"f*_seed_{seed}{suffix}"))
                # final_tile is the already existing result
                # of the fusion of classification
                final_tile = os.path.join(
                    path_to_classif, f"Classif_{tile}_model_{model}"
                    f"_seed_{seed}{suffix_sar}.tif")
                confidence = fu.fileSearchRegEx(
                    os.path.join(
                        path_to_classif, f"{tile}*model_{model}f*_confidence"
                        f"_seed_{seed}{suffix}"))
                if proba_map_flag:
                    proba_map_fusion(proba_map_list=fu.fileSearchRegEx(
                        os.path.join(
                            path_to_classif,
                            f"{probamap_pattern}_{tile}_model_"
                            f"{model}f*_seed_{seed}{suffix_sar}")),
                                     working_directory=wdir,
                                     ram=2000,
                                     logger=logger)
                classif_tile = sorted(classif_tile)
                confidence = sorted(confidence)
                output_confidence = os.path.join(
                    tmp_classif, f"{tile}_model_{model}_"
                    f"confidence_seed_{seed}{suffix_sar}.tif")
                cmd = build_confidence_cmd(final_tile,
                                           classif_tile,
                                           confidence,
                                           output_confidence,
                                           fact=100,
                                           pix_type="uint8")
                run(cmd, logger=logger)
                split_confidence.append(output_confidence)
                # -> confidence from splited models are from 0 to 100
                exp1 = "+".join([
                    "im" + str(i + 1) + "b1"
                    for i in range(len(split_confidence))
                ])
                # -> confidence from NO-splited models are from 0 to 1
                exp2 = "+".join([
                    "(100*im" + str(j + 1) + "b1)" for j in np.arange(
                        len(split_confidence),
                        len(split_confidence) + len(confidence_without_split))
                ])
                if not split_confidence:
                    exp2 = "+".join([
                        "100*im" + str(j + 1) + "b1"
                        for j in range(len(confidence_without_split))
                    ])
                if exp1 and exp2:
                    exp = exp1 + "+" + exp2
                if exp1 and not exp2:
                    exp = exp1
                if not exp1 and exp2:
                    exp = exp2

                confidence_list = (split_confidence + confidence_without_split)
                # all_confidence = " ".join(confidence_list)

                output_confidence = os.path.join(
                    tmp_classif, f"{tile}_GlobalConfidence_seed_{seed}.tif")
                # cmd = 'otbcli_BandMath -il ' + AllConfidence + ' -out
                # ' + OutPutConfidence + ' uint8 -exp "' + exp + '"'
                # run(cmd, logger=logger)
                bandmath = otb.CreateBandMathApplication({
                    "il": confidence_list,
                    "out": output_confidence,
                    "pixType": "uint8",
                    "exp": exp
                })
                bandmath.ExecuteAndWriteOutput()
                shutil.copyfile(
                    output_confidence,
                    os.path.join(iota2_directory, "final", "TMP",
                                 f"{tile}_GlobalConfidence_seed_{seed}.tif"))
                os.remove(output_confidence)
                confidence_out.append(
                    os.path.join(iota2_directory, "final", "TMP",
                                 f"{tile}_GlobalConfidence_seed_{seed}.tif"))

    return confidence_out


def gen_global_confidence(runs: int,
                          iota2_directory: str,
                          classif_mode: str,
                          all_tiles: List[str],
                          shape_region: str,
                          ds_sar_opt: bool,
                          proba_map_flag: bool,
                          working_directory: Optional[str] = None,
                          logger=LOGGER):
    """
    Get all confidence maps and generate the global product for each tile

    Parameters
    ----------

    runs:
        the number of random seeds
    iota2_directory:
        the path where iota2 output directory was created
    classif_mode:
        the classification mode 'separate' or 'fusion'
    all_tiles:
        the list of all tiles processed
    shape_region:
        the region shape file
    ds_sar_opt:
        enable or disable the use of SAR
    proba_map_flag:
        enable the probability final product creation
    working_directory:
        the working directory where tmp files can be stored
    """
    # Proposal: refact again ?
    # identify mode first then iterate on seed and tile
    probamap_pattern = "PROBAMAP"
    # tmp_classif = os.path.join(iota2_directory, "classif", "tmpClassif")
    path_to_classif = os.path.join(iota2_directory, "classif")

    wdir = working_directory if working_directory else path_to_classif
    tmp_classif = os.path.join(wdir, "tmpClassif")

    if not os.path.exists(tmp_classif):
        run(f"mkdir {tmp_classif}")

    suffix_sar = "_DS" if ds_sar_opt else ""
    confidences_out = []
    for seed in range(runs):
        for tile in all_tiles:
            if shape_region is None:
                if classif_mode == "separate":
                    confidence_pattern = os.path.join(
                        path_to_classif,
                        f"{tile}*model*confidence_seed_{seed}*{suffix_sar}.tif"
                    )
                    confidence = fu.fileSearchRegEx(confidence_pattern)
                    global_confidence = (f"{tile}_GlobalConfidence_"
                                         f"seed_{seed}.tif")
                    bandmath_conf = otb.CreateBandMathApplication({
                        "il": [confidence[0]],
                        "out":
                        os.path.join(wdir, global_confidence),
                        "pixType":
                        "uint8",
                        "exp":
                        "100*im1b1"
                    })
                    bandmath_conf.ExecuteAndWriteOutput()
                    shutil.copyfile(
                        os.path.join(wdir, global_confidence),
                        os.path.join(iota2_directory, "final", "TMP",
                                     global_confidence))
                    os.remove(os.path.join(wdir, global_confidence))
                else:
                    raise Exception(
                        ("if there is no region shape specify in the "
                         "configuration file, arg_classification.classif_mode"
                         " must be set to 'separate'"))
            else:  # output mode
                suffix = f"*{suffix_sar}"
                if classif_mode != "separate":
                    confidences_list = merge_confidence_in_fusion_mode(
                        iota2_directory=iota2_directory,
                        path_to_classif=path_to_classif,
                        tmp_classif=tmp_classif,
                        tile=tile,
                        seed=seed,
                        suffix=suffix,
                        suffix_sar=suffix_sar,
                        proba_map_flag=proba_map_flag,
                        probamap_pattern=probamap_pattern,
                        working_directory=working_directory,
                        logger=logger)
                    confidences_out += confidences_list
                else:
                    # mode separate
                    # 1 confidence masqued by region
                    confidence = fu.fileSearchRegEx(
                        os.path.join(
                            path_to_classif, f"{tile}*model*confidence_"
                            f"seed_{seed}{suffix}"))
                    exp = "+".join(
                        [f"im{i + 1}b1" for i in range(len(confidence))])
                    # all_confidence = " ".join(confidence)
                    # for currentConf in confidence:
                    global_conf = os.path.join(
                        tmp_classif,
                        f"{tile}_GlobalConfidence_seed_{seed}.tif")
                    global_conf_f = os.path.join(
                        iota2_directory, "final", "TMP",
                        f"{tile}_GlobalConfidence_seed_{seed}.tif")
                    bandmath = otb.CreateBandMathApplication({
                        "il":
                        confidence,
                        "out":
                        global_conf,
                        "pixType":
                        "uint8",
                        "exp":
                        f"100*{exp}"
                    })
                    bandmath.ExecuteAndWriteOutput()
                    shutil.copyfile(global_conf, global_conf_f)
                    os.remove(global_conf)
                    confidences_out.append(global_conf_f)
    return confidences_out


def classification_shaping(path_classif: str,
                           ds_sar_opt: bool,
                           output_path: str,
                           iota2_directory: str,
                           runs: int,
                           classif_mode: str,
                           region_shape: str,
                           proba_map_flag: bool,
                           output_statistics: bool,
                           data_field: str,
                           spatial_resolution: Tuple[Union[float, int],
                                                     Union[float, int]],
                           labels_conversion: Dict[int, Union[str, int]],
                           color_path: str,
                           tiles_from_cfg: List[str],
                           working_directory: Optional[str] = None,
                           logger=LOGGER) -> None:
    """
    Create the mosaic of all iota2 products

    Parameters:
    -----------
    path_classif:
        path to classifications
    ds_sar_opt:
        enable the use of SAR and Optical products together
    output_path:
        path to store final products
    iota2_directory:
        the iota2 output directories
    runs:
        the number of random seeds
    classif_mode:
        the classification mode 'separate' or 'fusion'
    region_shape:
        the region shapefile
    proba_map_flag:
        enable the generation of probabilities maps
    output_statistics:
        compute additional statistics on cloud coverage
    data_field:
        the class label column name
    spatial_resolution:
        the spatial resolution
    labels_conversion:
        dictionnary to decode internal labels to original labels
    color_path:
        the text file containing colors
    tiles_from_cfg:
        the full list of tiles provided by users
    working_directory:
        path to store intermediate products
    """
    wdir = working_directory if working_directory else output_path
    assemble_folder = os.path.join(iota2_directory, "final")
    tmp_dir = os.path.join(wdir, "TMP")
    if not os.path.exists(tmp_dir):
        os.mkdir(tmp_dir)
    pix_type = "uint8"
    suffix = "*DS*" if ds_sar_opt else "*"

    all_tiles = list(
        set([
            classif.split("_")[1] for classif in fu.FileSearch_AND(
                os.path.join(iota2_directory, "classif"), False, "Classif",
                ".tif")
        ]))

    # Prepare confidence and probabilities to be merged
    all_seed_confidence = gen_global_confidence(
        runs=runs,
        iota2_directory=iota2_directory,
        classif_mode=classif_mode,
        all_tiles=all_tiles,
        shape_region=region_shape,
        ds_sar_opt=ds_sar_opt,
        proba_map_flag=proba_map_flag,
        working_directory=working_directory,
        logger=logger)
    # ce move empeche de generer a nouveau les cartes de confiance si besoin
    # si pas de confflit il ne faut pas le faire
    # move_tmp_classif(region_shape, classif_mode, suffix, iota2_directory,
    #                  logger)
    if spatial_resolution:
        res_x = spatial_resolution[0]
        res_y = spatial_resolution[1]
    else:
        # use a classification as ref image
        reference = all_seed_confidence[0]
        res_x, res_y = getRasterResolution(reference)
        res_y = -res_y
    target_spatial_resolution = (res_x, res_y)
    # classification
    prepare_classification(assemble_folder=assemble_folder,
                           output_path=os.path.join(iota2_directory, "final",
                                                    "TMP"),
                           path_classif=path_classif,
                           classif_mode=classif_mode,
                           region_shape=region_shape,
                           ds_sar_opt=ds_sar_opt,
                           runs=runs,
                           pix_type=pix_type,
                           target_spatial_resolution=target_spatial_resolution)
    # confidence
    get_global_confidence(assemble_folder=assemble_folder,
                          list_tile=all_tiles,
                          confidence_path=os.path.join(iota2_directory,
                                                       "final", "TMP"),
                          runs=runs,
                          target_spatial_resolution=target_spatial_resolution)
    # proba_map
    if proba_map_flag:
        get_tiled_proba_map(
            assemble_folder=assemble_folder,
            proba_path=path_classif,
            runs=runs,
            suffix=suffix,
            target_spatial_resolution=target_spatial_resolution)
    # cloud
    get_final_cloud(assemble_folder=assemble_folder,
                    cloud_path=os.path.join(iota2_directory, "features"),
                    output_path=output_path,
                    list_tile=all_tiles,
                    output_statistics=output_statistics,
                    target_spatial_resolution=target_spatial_resolution,
                    working_directory=working_directory)

    for seed in range(runs):
        re_encoded_raster_path = os.path.join(
            iota2_directory, "final", f"Classif_Seed_{seed}_reencoded.tif")
        raster_classif_seed_path = os.path.join(iota2_directory, "final",
                                                f"Classif_Seed_{seed}.tif")
        decode_and_color_index_map(
            raster_classif_seed_path=raster_classif_seed_path,
            re_encoded_raster_path=re_encoded_raster_path,
            labels_conversion=labels_conversion,
            color_path=color_path,
            logger=logger)

    generate_diff_map(runs=runs,
                      all_tiles=all_tiles,
                      data_field=data_field,
                      spatial_resolution=spatial_resolution,
                      output_path=os.path.join(iota2_directory, "final",
                                               "TMP"),
                      iota2_directory=iota2_directory,
                      working_directory=working_directory,
                      logger=logger)
    missing_tiles = [elem for elem in all_tiles if elem not in tiles_from_cfg]
    create_dummy_rasters(missing_tiles, runs, iota2_directory)


def prepare_classification(assemble_folder: str, output_path: str,
                           path_classif: str, classif_mode: str,
                           region_shape: str, ds_sar_opt: bool, runs: int,
                           pix_type: str, target_spatial_resolution) -> None:
    """
    Get all classifications and reconstruct them by tiles then merge all

    Parameters
    ----------
    assemble_folder:
        the folder to store the mosaic
    output_path:
        the output path
    path_classif:
        path to classifications
    classif_mode:
        classification mode 'fusion' or 'separate'
    region_shape:
        the region shapefile
    ds_sar_opt:
        enable the use of SAR and optical together
    runs:
        the number of random seeds
    pix_type:
        the pixel type of the final product
    target_spatial_resolution:
        the spatial resolution
    """
    for seed in range(runs):
        sort = []
        classification = []
        # classification.append([])
        if classif_mode == "separate" or region_shape:
            all_classif_seed = fu.FileSearch_AND(path_classif, True, ".tif",
                                                 "Classif",
                                                 "seed_" + str(seed))
            if ds_sar_opt:
                all_classif_seed = fu.FileSearch_AND(path_classif, True,
                                                     ".tif", "Classif",
                                                     "seed_" + str(seed),
                                                     "DS.tif")
            ind = 1
        elif classif_mode == "fusion":
            all_classif_seed = fu.FileSearch_AND(
                path_classif, True, "_FUSION_NODATA_seed" + str(seed) + ".tif")
            if ds_sar_opt:
                all_classif_seed = fu.FileSearch_AND(
                    path_classif, True,
                    "_FUSION_NODATA_seed" + str(seed) + "_DS.tif")
            ind = 0
        all_classif_seed = remove_in_list_by_regex(all_classif_seed,
                                                   ".*model_.f._seed")
        for tile in all_classif_seed:
            sort.append((tile.split("/")[-1].split("_")[ind], tile))

        sort = fu.sortByFirstElem(sort)

        for tile, paths in sort:

            exp = "+".join([f"im{i+1}b1" for i in range(len(paths))])
            path_cl_final = os.path.join(output_path,
                                         f"{tile}_seed_{seed}.tif")
            classification.append(path_cl_final)
            bandmath = otb.CreateBandMathApplication({
                "il": paths,
                "out": path_cl_final,
                "exp": exp,
                "pixType": pix_type
            })
            bandmath.ExecuteAndWriteOutput()
        classif_mosaic_tmp = os.path.join(assemble_folder,
                                          f"Classif_Seed_{seed}_tmp.tif")
        classif_mosaic_compress = os.path.join(assemble_folder,
                                               f"Classif_Seed_{seed}.tif")
        fu.assembleTile_Merge(classification,
                              target_spatial_resolution,
                              classif_mosaic_tmp,
                              "Byte",
                              co={
                                  "COMPRESS": "LZW",
                                  "BIGTIFF": "YES"
                              })
        compress_raster(classif_mosaic_tmp, classif_mosaic_compress)
        os.remove(classif_mosaic_tmp)


def get_global_confidence(
    list_tile: List[str], confidence_path: str, runs: int,
    assemble_folder: str, target_spatial_resolution: Tuple[Union[float, int],
                                                           Union[float, int]]
) -> None:
    """
    Get all confidence maps and merge them

    Parameters
    ----------
    
    list_tile:
        the list of tiles produced
    confidence_path:
        path where confidence maps are stored
    runs:
        the number of random seeds
    assemble_folder:
        path to store the mosaic
    target_spatial_resolution:
        the expected spatial resolution
    """

    list_tile = sorted(list_tile)
    for seed in range(runs):
        confidence = []
        # confidence.append([])
        for tile in list_tile:
            tile_confidence = os.path.join(
                confidence_path, f"{tile}_GlobalConfidence_seed_{seed}.tif")
            if os.path.exists(tile_confidence):
                confidence.append(tile_confidence)
        confidence_mosaic_tmp = os.path.join(
            assemble_folder, f"Confidence_Seed_{seed}_tmp.tif")
        confidence_mosaic_compress = os.path.join(
            assemble_folder, f"Confidence_Seed_{seed}.tif")
        fu.assembleTile_Merge(confidence,
                              target_spatial_resolution,
                              confidence_mosaic_tmp,
                              "Byte",
                              co={
                                  "COMPRESS": "LZW",
                                  "BIGTIFF": "YES"
                              })
        compress_raster(confidence_mosaic_tmp, confidence_mosaic_compress)
        os.remove(confidence_mosaic_tmp)


def get_tiled_proba_map(
    proba_path: str, runs: int, suffix: str, assemble_folder: str,
    target_spatial_resolution: Tuple[Union[float, int], Union[float, int]]
) -> None:
    """
    Get all probabilities map and merge them
    Parameters
    ----------

    proba_path:
        path where probabilities maps are stored
    runs:
        the number of random seeds
    suffix:
        classificatin suffix
    assemble_folder:
        path to store the mosaic
    target_spatial_resolution:
        the expected spatial resolution
    """
    # proba_map = []
    for seed in range(runs):
        proba_map_list = fu.fileSearchRegEx(
            os.path.join(proba_path,
                         f"PROBAMAP_*_model_*_seed_{seed}{suffix}.tif"))
        proba_map_list = remove_in_list_by_regex(
            proba_map_list, ".*model_.*f.*_seed." + suffix)
        # proba_map.append(proba_map_list)
        proba_map_mosaic_tmp = os.path.join(
            assemble_folder, f"ProbabilityMap_seed_{seed}_tmp.tif")
        proba_map_mosaic_compress = os.path.join(
            assemble_folder, f"ProbabilityMap_seed_{seed}.tif")
        fu.assembleTile_Merge(proba_map_list,
                              target_spatial_resolution,
                              proba_map_mosaic_tmp,
                              "Int16",
                              co={
                                  "COMPRESS": "LZW",
                                  "BIGTIFF": "YES"
                              })
        compress_raster(proba_map_mosaic_tmp, proba_map_mosaic_compress)
        os.remove(proba_map_mosaic_tmp)


def get_final_cloud(cloud_path: str,
                    output_path: str,
                    list_tile: List[str],
                    output_statistics: bool,
                    assemble_folder: str,
                    target_spatial_resolution: Tuple[Union[int, float],
                                                     Union[int, float]],
                    working_directory: Optional[str] = None) -> None:
    """
    Prepare validity masks to be merged
    
    Parameters
    ----------
    cloud_path:
        path to cloud maps
    output_path:
        output path of by tiles products
    list_tile:
        the list of tiles
    assemble_folder:
        path to store the final mosaic
    output_statistics:
        compute additional statistics about cloud covering
    target_spatial_resolution:
        the expected spatial resolution
    working_directory:
        folder to store intermediate results
    """
    working_dir = working_directory if working_directory else os.path.join(
        output_path, "TMP")
    list_tile = sorted(list_tile)
    # for seed in range(runs):
    cloud = []
    # cloud.append([])
    for tile in list_tile:
        cloud_tile = os.path.join(cloud_path, f"{tile}", "nbView.tif")
        if os.path.exists(cloud_tile):
            classif_tile = os.path.join(output_path, "TMP",
                                        f"{tile}_seed_0.tif")
            cloud_tile_priority = f"{tile}_Cloud.tif"
            cloud_tile_priority_stats_ok = f"{tile}_Cloud_StatsOK.tif"
            cloud.append(os.path.join(output_path, "TMP", cloud_tile_priority))
            # if not os.path.exists(
            #         os.path.join(working_dir, cloud_tile_priority)):
            bandmath = otb.CreateBandMathApplication({
                "il": [cloud_tile, classif_tile],
                "out":
                os.path.join(working_dir, cloud_tile_priority),
                "pixType":
                "int16",
                "exp":
                "im2b1>0?im1b1:0"
            })
            bandmath.ExecuteAndWriteOutput()
            if output_statistics:
                bandmath = otb.CreateBandMathApplication({
                    "il": [cloud_tile, classif_tile],
                    "out":
                    os.path.join(working_dir, cloud_tile_priority_stats_ok),
                    "pixType":
                    "int16",
                    "exp":
                    "im2b1>0?im1b1:-1"
                })
                bandmath.ExecuteAndWriteOutput()
                if working_directory:
                    shutil.copy(
                        os.path.join(working_dir,
                                     cloud_tile_priority_stats_ok),
                        os.path.join(output_path,
                                     cloud_tile_priority_stats_ok))
                    os.remove(
                        os.path.join(working_dir,
                                     cloud_tile_priority_stats_ok))
            if working_directory:
                shutil.copy(
                    os.path.join(working_dir, cloud_tile_priority),
                    os.path.join(output_path, "TMP", cloud_tile_priority))
                os.remove(os.path.join(working_dir, cloud_tile_priority))
    cloud_mosaic_tmp = os.path.join(assemble_folder, "PixelsValidity_tmp.tif")
    cloud_mosaic_compress = os.path.join(assemble_folder, "PixelsValidity.tif")
    fu.assembleTile_Merge(cloud,
                          target_spatial_resolution,
                          cloud_mosaic_tmp,
                          "Byte",
                          co={
                              "COMPRESS": "LZW",
                              "BIGTIFF": "YES"
                          })
    compress_raster(cloud_mosaic_tmp, cloud_mosaic_compress)
    os.remove(cloud_mosaic_tmp)


def decode_and_color_index_map(raster_classif_seed_path: str,
                               re_encoded_raster_path: str,
                               labels_conversion: Dict[Union[str, int],
                                                       Union[str, int]],
                               color_path: str,
                               logger=LOGGER) -> None:
    """
    For an input map decode the labels and produce the colored map

    Parameters
    ----------

    raster_classif_seed_path:
        path to input classification
    re_encoded_raster_path:
        path to re encoded classification maps
    labels_conversion:
        dictionary to map label conversion
    color_path:
        path to color file
    
    """
    encoded_raster_bool, pix_type = re_encode_raster(raster_classif_seed_path,
                                                     re_encoded_raster_path,
                                                     labels_conversion,
                                                     logger=logger)
    raster_to_color_path = raster_classif_seed_path
    if encoded_raster_bool:
        shutil.move(re_encoded_raster_path, raster_classif_seed_path)
        labels_conversion_seed = None
    else:
        labels_conversion_seed = labels_conversion.copy()

    color_indexed_raster = color.CreateIndexedColorImage(
        raster_to_color_path,
        color_path,
        output_pix_type=gdal.GDT_Byte
        if pix_type == "uint8" else gdal.GDT_UInt16,
        labels_conversion=labels_conversion_seed)

    color_indexed_raster_compress = color_indexed_raster.replace(
        ".tif", "_compress.tif")
    compress_raster(color_indexed_raster, color_indexed_raster_compress)
    shutil.copy(color_indexed_raster_compress, color_indexed_raster)
    os.remove(color_indexed_raster_compress)
