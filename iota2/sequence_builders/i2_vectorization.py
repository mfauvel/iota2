#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import shutil
import logging
import osgeo.ogr as ogr
import osgeo.osr as osr
from typing import Optional
from functools import partial
from collections import OrderedDict

from iota2.Steps.IOTA2Step import Step
import iota2.Common.i2_constants as i2_const
from iota2.Steps.IOTA2Step import StepContainer
from iota2.configuration_files import read_config_file as rcf
from iota2.sequence_builders.i2_sequence_builder import i2_builder
from iota2.Common.IOTA2Directory import generate_vectorization_directories

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.iota2_constants()


class i2_vectorization(i2_builder):
    """
    class use to describe steps sequence and variable to use at
    each step (config)
    """
    def __init__(self,
                 cfg: str,
                 config_resources: str,
                 schduler_type: str,
                 restart: Optional[bool] = False,
                 tasks_states_file: Optional[str] = None,
                 hpc_working_directory: Optional[str] = "TMPDIR"):
        super().__init__(cfg, config_resources, schduler_type, restart,
                         tasks_states_file, hpc_working_directory)

        # steps definitions
        self.config_resources = config_resources
        # self.steps_group = OrderedDict()
        self.steps_group["init"] = OrderedDict()
        self.steps_group["regularisation"] = OrderedDict()
        self.steps_group["crown"] = OrderedDict()
        self.steps_group["mosaictiles"] = OrderedDict()
        self.steps_group["vectorisation"] = OrderedDict()
        self.steps_group["simplification"] = OrderedDict()
        self.steps_group["smoothing"] = OrderedDict()
        self.steps_group["clipvectors"] = OrderedDict()
        self.steps_group["lcstatistics"] = OrderedDict()

        # create dict for control variables
        self.control_var = {}

        # build steps
        self.sort_step()

    def init_dict_control_variables(self):
        """
        """
        config_content = rcf.read_config_file(self.cfg)
        self.control_var["output_path"] = config_content.getParam(
            "chain", "output_path")
        self.control_var["zonal_vector"] = config_content.getParam(
            "simplification", "zonal_vector")
        self.control_var["umc1"] = config_content.getParam(
            "simplification", "umc1")
        self.control_var["umc2"] = config_content.getParam(
            "simplification", "umc2")
        self.control_var["outregul"] = config_content.getParam(
            "simplification", "umc2")
        self.control_var["vecto_outputs_dir"] = config_content.getParam(
            "chain", "output_path")
        self.control_var["nomenclature"] = config_content.getParam(
            "simplification", "nomenclature")
        self.control_var["rssize"] = config_content.getParam(
            "simplification", "rssize")

        self.control_var["raster_to_regul"] = config_content.getParam(
            "simplification", "classification")
        self.control_var["confidence"] = config_content.getParam(
            "simplification", "confidence")
        self.control_var["validity"] = config_content.getParam(
            "simplification", "validity")
        seed_to_vectorize = config_content.getParam("simplification",
                                                    "seed") - 1
        if "i2_classification" in self.builders_instances:
            classification_data = self.builders_instances[
                "i2_classification"].get_outputs_data()
            self.control_var["validity"] = classification_data[
                "mosaic_validity"]
            classifications_mosaic = classification_data[
                "mosaic_classifications"]
            confidences_mosaic = classification_data["mosaic_classifications"]
            if seed_to_vectorize in classifications_mosaic:
                self.control_var["raster_to_regul"] = classifications_mosaic[
                    seed_to_vectorize]
                self.control_var["confidence"] = confidences_mosaic[
                    seed_to_vectorize]
            else:
                raise ValueError(
                    "iota2 configuration file parameter simplification.seed "
                    " not consistent with arg_train.runs")
            if config_content.getParam("simplification",
                                       "vectorize_fusion_of_classifications"):
                if not "mosaic_fusion_of_classifications" in classification_data:
                    raise ValueError(
                        "iota2 configuration file parameter "
                        "simplification.vectorize_fusion_of_classifications "
                        "not consistent with arg_classification.merge_final_classifications"
                    )
                self.control_var["raster_to_regul"] = classification_data[
                    "mosaic_fusion_of_classifications"]

        self.control_var["gridsize"] = config_content.getParam(
            "simplification", "gridsize")
        self.control_var["epsg"] = int(
            config_content.getParam("chain", "proj").split(":")[-1])
        self.control_var["chunk"] = config_content.getParam(
            "simplification", "chunk")
        self.control_var["clip_file"] = config_content.getParam(
            "simplification", "clipfile")
        self.control_var["clip_field"] = config_content.getParam(
            "simplification", "clipfield")
        self.control_var["clip_value"] = config_content.getParam(
            "simplification", "clipvalue")
        self.control_var["prefix"] = config_content.getParam(
            "simplification", "outprefix")
        self.control_var["remove_output_path"] = config_content.getParam(
            "chain", "remove_output_path")

    def vecto_pre_launch(self):
        """precompute data in order to be able to generate a dask graph
        """
        from iota2.Common.FileUtils import ensure_dir
        from iota2.Common.FileUtils import getRasterExtent
        from iota2.simplification import GridGenerator as gridg
        if self.control_var["gridsize"] is not None:
            outname = os.path.join(self.control_var["output_path"],
                                   "simplification", "grid.shp")
            xysize = self.control_var["gridsize"]
            epsg = self.control_var["epsg"]
            raster = self.control_var["raster_to_regul"]

            coordinates = None
            ensure_dir(
                os.path.join(self.control_var["output_path"],
                             "simplification"))
            gridg.grid_generate(outname, xysize, epsg, raster, coordinates)

        if not self.control_var["clip_file"]:
            min_x, max_x, min_y, max_y = getRasterExtent(
                self.control_var["raster_to_regul"])

            clip_file = os.path.join(self.control_var["output_path"],
                                     "clip.shp")

            clip_field = self.control_var["clip_field"]
            if not clip_field:
                clip_field = I2_CONST.i2_vecto_clip_field
            self.generate_clip_file(clip_file, clip_field,
                                    self.control_var["epsg"], min_x, max_x,
                                    min_y, max_y)

    def generate_clip_file(self, clip_file: str, clip_field: str, epsg: int,
                           min_x: float, max_x: float, min_y: float,
                           max_y: float):
        """create a shapeFile with a unique square polygon

        Parameters
        ----------
        clip_file:
            output shapeFile
        clip_field:
            output field
        epsg:
            output projection
        min_x:
           min x
        max_x:
           max x
        min_y:
           min y
        max_y:
           max y
        """
        driver = ogr.GetDriverByName("ESRI Shapefile")
        data_source = driver.CreateDataSource(clip_file)
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(epsg)
        layer_name = os.path.splitext(os.path.basename(clip_file))[0]
        layer = data_source.CreateLayer(layer_name, srs, ogr.wkbPolygon)
        field = ogr.FieldDefn(clip_field, ogr.OFTString)
        field.SetWidth(24)
        layer.CreateField(field)

        ring = ogr.Geometry(ogr.wkbLinearRing)
        ring.AddPoint(min_x, min_y)
        ring.AddPoint(max_x, min_y)
        ring.AddPoint(max_x, max_y)
        ring.AddPoint(min_x, max_y)
        ring.AddPoint(min_x, min_y)
        poly = ogr.Geometry(ogr.wkbPolygon)
        poly.AddGeometry(ring)

        feature = ogr.Feature(layer.GetLayerDefn())
        feature.SetField(clip_field, "1")
        feature.SetGeometry(poly)
        layer.CreateFeature(feature)
        feature = None
        data_source = None

    def build_steps(self, cfg, config_ressources=None):
        """
        build steps
        """
        from iota2.Steps import Clump
        from iota2.Steps import crown_build
        from iota2.Steps import crown_search
        from iota2.Steps import clip_vectors
        from iota2.Steps import prod_vectors
        from iota2.Steps import large_smoothing
        from iota2.Steps import Regularization
        from iota2.Steps import zonal_statistics
        from iota2.Steps import large_vectorization
        from iota2.Steps import merge_regularization
        from iota2.Steps import large_simplification
        from iota2.Steps import vectorization_dir_tree
        from iota2.Steps import mosaic_tiles_vectorization

        # control variable
        self.init_dict_control_variables()

        zonal_vector = self.control_var["zonal_vector"]

        s_container = StepContainer("vectorization")
        s_container.prelaunch_function = partial(self.vecto_pre_launch)
        do_regul = False
        # TODO : in fact 'umc' is use to trigger regularisation steps ?
        umc1 = self.control_var["umc1"]
        outregul = os.path.join(self.control_var["vecto_outputs_dir"],
                                "simplification", "classif_regul.tif")

        if not zonal_vector:
            if umc1:
                do_regul = True
                regulruns = 2 if self.control_var["umc2"] is not None else 1
                raster_to_regul = self.control_var["raster_to_regul"]
                if regulruns == 2:
                    outregul = os.path.join(
                        self.control_var["vecto_outputs_dir"],
                        "simplification", "tmp", "regul1.tif")
                    s_container.append(
                        partial(Regularization.Regularization,
                                self.cfg,
                                self.config_resources,
                                raster_to_regul=self.
                                control_var["raster_to_regul"],
                                umc=umc1,
                                nomenclature=self.control_var["nomenclature"],
                                step_name="regul_1",
                                workingDirectory=self.workingDirectory),
                        "regularisation")
                    s_container.append(
                        partial(merge_regularization.MergeRegularization,
                                self.cfg,
                                self.config_resources,
                                workingDirectory=self.workingDirectory,
                                resample=self.control_var["rssize"],
                                umc=umc1,
                                stepname="merge_regul1",
                                output=outregul), "regularisation")
                    umc1 = self.control_var["umc2"]
                    rssize = None
                    outregul = os.path.join(
                        self.control_var["vecto_outputs_dir"],
                        "simplification", "classif_regul.tif")
                    raster_to_regul = os.path.join(
                        self.control_var["vecto_outputs_dir"],
                        "simplification", "tmp", "regul1.tif")
                s_container.append(
                    partial(Regularization.Regularization,
                            self.cfg,
                            self.config_resources,
                            umc=umc1,
                            nomenclature=self.control_var["nomenclature"],
                            step_name="regul_2",
                            workingDirectory=self.workingDirectory,
                            raster_to_regul=raster_to_regul), "regularisation")
                s_container.append(
                    partial(merge_regularization.MergeRegularization,
                            self.cfg,
                            self.config_resources,
                            workingDirectory=self.workingDirectory,
                            resample=self.control_var["rssize"],
                            umc=umc1,
                            stepname="merge_regul1",
                            output=outregul), "regularisation")

            if self.control_var["gridsize"]:
                s_container.append(
                    partial(Clump.Clump,
                            self.cfg,
                            self.config_resources,
                            self.workingDirectory,
                            from_regul=do_regul), "regularisation")
                s_container.append(
                    partial(crown_search.CrownSearch, self.cfg,
                            self.config_resources, self.workingDirectory),
                    "crown")
                s_container.append(
                    partial(crown_build.CrownBuild, self.cfg,
                            self.config_resources, self.workingDirectory),
                    "crown")
                s_container.append(
                    partial(
                        mosaic_tiles_vectorization.MosaicTilesForVectorization,
                        self.cfg, self.config_resources,
                        self.workingDirectory), "mosaictiles")

            s_container.append(
                partial(large_vectorization.LargeVectorization, self.cfg,
                self.config_resources, self.control_var["raster_to_regul"], self.workingDirectory),
                "vectorisation")
            s_container.append(
                partial(large_simplification.LargeSimplification, self.cfg,
                        self.config_resources, self.workingDirectory),
                "simplification")
            s_container.append(
                partial(large_smoothing.LargeSmoothing, self.cfg,
                        self.config_resources, self.workingDirectory),
                "smoothing")
            s_container.append(
                partial(clip_vectors.ClipVectors, self.cfg,
                        self.config_resources, self.workingDirectory),
                "clipvectors")

        else:
            s_container.prelaunch_function = partial(self.guess_features)
            s_container.append(
            partial(zonal_statistics.ZonalStatistics, self.cfg,
                    self.config_resources, self.control_var["raster_to_regul"],
                    self.control_var["confidence"],
                    self.control_var["validity"], self.workingDirectory),
                "lcstatistics")
            s_container.append(
                partial(prod_vectors.ProdVectors, self.cfg,
                        self.config_resources, self.workingDirectory),
                "lcstatistics")

            return [s_container]

        s_container_stats = StepContainer("statistics")
        s_container_stats.prelaunch_function = partial(self.guess_features)
        s_container_stats.append(
            partial(zonal_statistics.ZonalStatistics, self.cfg,
                    self.config_resources, self.control_var["raster_to_regul"],
                    self.control_var["confidence"],
                    self.control_var["validity"], self.workingDirectory),
            "lcstatistics")

        s_container_stats.append(
            partial(prod_vectors.ProdVectors, self.cfg, self.config_resources,
                    self.workingDirectory), "lcstatistics")

        return [s_container, s_container_stats]

    def guess_features(self):
        """
        """
        from iota2.VectorTools.split_vector import split_vector_in_features

        if self.control_var["zonal_vector"]:
            if self.control_var["clip_value"]:
                clipval = self.control_var["clip_value"]
            else:
                clipval = I2_CONST.i2_vecto_clip_value

            vector4zs = os.path.join(
                self.control_var["output_path"], "simplification", "vectors",
                self.control_var["prefix"] + '_' + str(clipval))

            for ext in ['.shp', '.dbf', '.prj', '.shx']:
                print(
                    os.path.splitext(self.control_var["zonal_vector"])[0] +
                    ext, vector4zs + ext)

                shutil.copy(
                    os.path.splitext(self.control_var["zonal_vector"])[0] +
                    ext, vector4zs + ext)

        regions_stats_partition = split_vector_in_features(
            os.path.join(self.control_var["output_path"], "simplification",
                         "vectors"),
            os.path.join(self.control_var["output_path"], "simplification",
                         "tmp"), self.control_var["chunk"])
        Step.regions_stats_partition = regions_stats_partition

    def check_config_parameters(self):
        config_content = rcf.read_config_file(self.cfg)

    def generate_output_directories(self,
                                    first_step_index: int,
                                    restart: bool,
                                    builder_index: Optional[int] = 0):
        i2_output_dir = self.control_var["output_path"]
        rm_if_exists = self.control_var["remove_output_path"]

        task_status_file = os.path.join(i2_output_dir,
                                        I2_CONST.i2_tasks_status_filename)
        restart = restart and os.path.exists(task_status_file)

        rm_if_exists = rm_if_exists and os.path.exists(i2_output_dir)

        if rm_if_exists and first_step_index != 0 or restart:
            pass
        elif builder_index == 0 and rm_if_exists:
            shutil.rmtree(i2_output_dir)

        if restart:
            pass
        elif (first_step_index == 0
              and not restart) or not os.path.exists(i2_output_dir):
            generate_vectorization_directories(i2_output_dir)
        else:
            pass
