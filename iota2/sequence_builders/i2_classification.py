#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import ogr
import shutil
import logging
from typing import List
from typing import Dict
from typing import Union
from typing import Optional
from functools import partial
from collections import Counter
from collections import OrderedDict
from iota2.Steps.IOTA2Step import Step
import iota2.Common.i2_constants as i2_const
from iota2.Common import ServiceError as sErr
from iota2.Steps.IOTA2Step import StepContainer
from iota2.Common.FileUtils import FileSearch_AND
from iota2.Common.FileUtils import sortByFirstElem
from iota2.VectorTools.vector_functions import getFields
from iota2.Common.FileUtils import get_iota2_project_dir
from iota2.Common.IOTA2Directory import generate_directories
from iota2.configuration_files import read_config_file as rcf
from iota2.VectorTools.vector_functions import getFieldElement
from iota2.sequence_builders.i2_sequence_builder import i2_builder
from iota2.configuration_files import check_config_parameters as ccp
from iota2.VectorTools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.iota2_constants()


class i2_classification(i2_builder):
    """
    class use to describe steps sequence and variable to use at
    each step (config)
    """
    def __init__(self,
                 cfg: str,
                 config_resources: str,
                 schduler_type: str,
                 restart: Optional[bool] = False,
                 tasks_states_file: Optional[str] = None,
                 hpc_working_directory: Optional[str] = "TMPDIR"):
        super().__init__(cfg, config_resources, schduler_type, restart,
                         tasks_states_file, hpc_working_directory)

        self.config_resources = config_resources
        # create dict for control variables
        self.control_var = {}
        # steps definitions
        # self.steps_group = OrderedDict()

        self.steps_group["init"] = OrderedDict()
        self.steps_group["sampling"] = OrderedDict()
        self.steps_group["dimred"] = OrderedDict()
        self.steps_group["learning"] = OrderedDict()
        self.steps_group["classification"] = OrderedDict()
        self.steps_group["mosaic"] = OrderedDict()
        self.steps_group["validation"] = OrderedDict()

        #build steps
        self.sort_step()

    def get_dir(self):
        """
        usage : return iota2_directories
        """
        directories = [
            'classif', 'config_model', 'dataRegion', 'envelope',
            'formattingVectors', 'metaData', 'samplesSelection', 'stats',
            'cmd', 'dataAppVal', 'dimRed', 'final', 'learningSamples', 'model',
            'shapeRegion', "features"
        ]

        iota2_outputs_dir = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')

        return [os.path.join(iota2_outputs_dir, d) for d in directories]

    def models_distribution(self):
        """function to determine which models intersect which tiles
           by using intermediate iota2 results
        """
        formatting_dir = os.path.join(self.output_i2_directory,
                                      "formattingVectors")
        formatting_files = FileSearch_AND(formatting_dir, True, ".shp")
        tiles = []
        regions_tiles = []
        for formatting_file in formatting_files:
            tile_name = os.path.splitext(os.path.basename(formatting_file))[0]
            regions_tile = getFieldElement(formatting_file,
                                           field=self.region_field,
                                           mode="unique",
                                           elemType="str")
            if not tile_name in tiles:
                tiles.append(tile_name)
            for region in regions_tile:
                regions_tiles.append((region, tile_name))
        regions = sortByFirstElem(regions_tiles)
        regions_distrib = {}
        for region_name, region_tiles in dict(regions).items():
            regions_distrib[region_name] = {"tiles": region_tiles}
        if not tiles:
            raise ValueError(
                "Can't find data in 'formattingVectors' directory")
        Step.set_models_spatial_information(tiles, regions_distrib)

        # manage tiles to classify
        shape_region_dir = os.path.join(self.output_i2_directory,
                                        "shapeRegion")
        region_tiles_files = FileSearch_AND(shape_region_dir, False, ".shp")
        regions_to_classify = []
        for region_tile in region_tiles_files:
            region = region_tile.split("_")[-2]
            tile = region_tile.split("_")[-1]
            regions_to_classify.append((region, tile))
        regions_to_classify = sortByFirstElem(regions_to_classify)
        dico_regions_to_classify = {}
        for region, tiles in regions_to_classify:
            dico_regions_to_classify[region] = {"tiles": tiles}

        regions_classif_distrib = {}

        for region_name, dico_tiles in regions_distrib.items():
            regions_classif_distrib[region_name] = {
                "tiles":
                dico_regions_to_classify[region_name.split("f")[0]]["tiles"]
            }
            if "nb_sub_model" in dico_tiles:
                regions_classif_distrib[region_name][
                    "nb_sub_model"] = dico_tiles["nb_sub_model"]

        regions_classif_distrib_no_split = {}
        for region, region_meta in Step.spatial_models_distribution_no_sub_splits.copy(
        ).items():
            regions_classif_distrib_no_split[region] = {
                "nb_sub_model": region_meta["nb_sub_model"],
                "tiles": dico_regions_to_classify[region]["tiles"].copy()
            }
        Step.spatial_models_distribution_classify = regions_classif_distrib
        Step.spatial_models_distribution_no_sub_splits_classify = regions_classif_distrib_no_split

    def formatting_reference_data(self):
        """
        """
        ref_data = self.control_var["ground_truth"]
        ref_data_field = self.control_var["data_field"]
        ref_data_i2_formatted = os.path.join(
            self.control_var["iota2_outputs_dir"], "reference_data.shp")
        self.ref_data_formatting(ref_data, ref_data_field,
                                 ref_data_i2_formatted)

    def ref_data_formatting(self, ref_data: str, ref_data_field: str,
                            ref_data_i2_formatted: str) -> None:
        """re-encode reference data

        Parameters
        ----------
        ref_data
            input reference database
        ref_data_field
            column containing labels database
        ref_data_i2_formatted
            output formatted database
        """
        # from sklearn import preprocessing
        # preprocessing.LabelEncoder() cant exclude re-encoding values
        # label_encoder.transform([1, 2, 3, 4]) is always 0 which is the
        # iota2 'no labels' value

        i2_label_column = I2_CONST.re_encoding_label_name
        ref_data_fields = getFields(ref_data)
        if i2_label_column in ref_data_fields:
            raise ValueError(
                f"the reference data : {ref_data} contains the field"
                f" {i2_label_column}. Please rename the column name")
        lut_labels = get_re_encoding_labels_dic(ref_data, ref_data_field)

        ds = ogr.Open(ref_data)
        drv = ds.GetDriver()
        drv.CopyDataSource(ds, ref_data_i2_formatted)
        ds = ogr.Open(ref_data_i2_formatted)
        driver = ds.GetDriver()
        data_src = driver.Open(ref_data_i2_formatted, 1)
        layer = data_src.GetLayer()
        layer.CreateField(ogr.FieldDefn(i2_label_column, ogr.OFTInteger))

        for feat in layer:
            feat.SetField(i2_label_column,
                          lut_labels[feat.GetField(ref_data_field)])
            layer.SetFeature(feat)

    def build_steps(
            self,
            cfg: str,
            config_ressources: Optional[str] = None) -> List[StepContainer]:
        """
        build steps
        """
        Step.set_models_spatial_information(self.tiles, {})

        # control variable
        self.init_dict_control_variables()

        # will contains all IOTA² steps
        s_container_init = StepContainer(name="classifications_init")
        s_container_init.prelaunch_function = partial(
            self.formatting_reference_data)

        s_container_spatial_distrib = StepContainer(
            name="classifications_spatial_distrib")
        s_container_spatial_distrib.prelaunch_function = partial(
            self.models_distribution)

        # build chain
        # init steps
        self.init_step_group(s_container_init)
        # sampling steps
        self.sampling_step_group(s_container_init, s_container_spatial_distrib)
        # learning steps
        self.learning_step_group(s_container_spatial_distrib)

        # classifications
        self.classification_step_group(s_container_spatial_distrib)

        # mosaic step
        self.mosaic_step_group(s_container_spatial_distrib)

        # validation steps
        self.validation_steps_group(s_container_spatial_distrib)
        return [s_container_init, s_container_spatial_distrib]

    def pre_check(self) -> None:
        """
        do some quick checks
        check configuration file mandatory section
        check configuration parameters consitency
        check region vector
        """
        # Check if paramters are coherent
        self.check_mandatory_section()
        self.check_config_parameters()
        self.check_input_text_files()
        self.check_compat_param()
        self.check_region_vector()
        self.check_sensors_data()

    def check_input_text_files(self):
        """check nomenclature and color files consistency
        """
        from iota2.Validation.ResultsUtils import get_nomenclature
        from iota2.Validation.ResultsUtils import get_color_table
        config_content = rcf.read_config_file(self.cfg)
        nomenclature_file = config_content.getParam("chain",
                                                    "nomenclature_path")
        color_file = config_content.getParam("chain", "color_table")

        nomenclature = get_nomenclature(nomenclature_file)
        colors = get_color_table(color_file)

        if len(nomenclature.keys()) != len(colors.keys()):
            raise ValueError(
                "Iota2 detected an inconsistency : "
                f"color file {color_file} as {len(colors.keys())} entries "
                f" while the nomenclature file {nomenclature_file} as {len(nomenclature.keys())} entries"
            )

        missing_labels = []
        for class_label in colors.keys():
            if class_label not in nomenclature.keys():
                missing_labels.append(class_label)
        if missing_labels:
            raise ValueError("Iota2 detected an inconsistency : "
                             f" these class '{', '.join(missing_labels)}'"
                             " are in the color table file, but does not"
                             "exists in the nomenclature file")

        input_vector_db_file = config_content.getParam("chain", "ground_truth")
        label_field = config_content.getParam("chain", "data_field")
        labels = getFieldElement(input_vector_db_file,
                                 driverName="ESRI Shapefile",
                                 field=label_field,
                                 mode="unique",
                                 elemType="str")
        missing_labels = []
        for label in labels:
            if label not in nomenclature.keys():
                missing_labels.append(label)
        if missing_labels:
            raise ValueError(
                "Iota2 detected an inconsistency : "
                f" these class '{', '.join(missing_labels)}'"
                " are in the input database, but does not"
                "exists in the nomenclature file and / or in the color table file"
            )

    def check_sensors_data(self):
        from iota2.Sensors.Sensors_container import sensors_container
        config_content = rcf.read_config_file(self.cfg)
        i2_params = rcf.iota2_parameters(self.cfg)

        minimum_required_dates = config_content.getParam(
            "chain", "minimum_required_dates")

        sensors_dates = i2_params.get_available_sensors_dates()
        for sensor_name, sensor_dates in sensors_dates.items():

            dates_counter = Counter(sensor_dates)
            if sensor_name.lower() != "userfeatures" and sensor_name.lower(
            ) != "sentinel1":
                duplicated_dates = []
                for date, count in dates_counter.items():
                    if count > 1:
                        duplicated_dates.append(date)
                if duplicated_dates:
                    raise ValueError(
                        f"Iota2 detected duplicated dates '{', '.join(map(str, duplicated_dates))}' for the sensor '{sensor_name}' please merge or remove them"
                    )
            if sensor_name.lower() != "sentinel1" and sensor_name.lower(
            ) != "userfeatures" and len(sensor_dates) < minimum_required_dates:
                raise ValueError(
                    f"Iota2 detected insufficient number of available dates for the sensor '{sensor_name}', {len(sensor_dates)} dates detected. Minimum is {minimum_required_dates}"
                )

    def check_region_vector(self):
        """check region vector consitency
        """
        config_content = rcf.read_config_file(self.cfg)
        region_path = config_content.getParam("chain", "region_path")
        if config_content.getParam("chain", "region_path"):

            self.test_database_name(region_path)
            region_field = config_content.getParam("chain", "region_field")
            try:
                config_content.backlog_params["cfg_raw"].chain.region_field
            except AttributeError:
                raise sErr.configError("chain.region_field must be set")

            driver = ogr.GetDriverByName("ESRI Shapefile")
            dataSource = driver.Open(region_path, 0)
            if dataSource is None:
                raise Exception("Could not open " + region_path)
            layer = dataSource.GetLayer()
            field_index = layer.FindFieldIndex(region_field, False)
            layerDefinition = layer.GetLayerDefn()
            fieldTypeCode = layerDefinition.GetFieldDefn(field_index).GetType()
            fieldType = layerDefinition.GetFieldDefn(
                field_index).GetFieldTypeName(fieldTypeCode)
            if fieldType != "String":
                raise sErr.configError("the region field must be a string")
            for feat in layer:
                reg_value = feat.GetField(region_field)
                if not reg_value.isdecimal():
                    raise ValueError(
                        f"region '{reg_value}' cannot be translated as an integer, please rename it"
                    )

    def test_database_name(self, input_vector):
        """
        """
        import string

        avail_characters = string.ascii_letters
        first_character = os.path.basename(input_vector)[0]
        if first_character not in avail_characters:
            raise sErr.configError(
                "the file '{}' is containing a non-ascii letter at first position in it's name : {}"
                .format(input_vector, first_character))

    def check_config_parameters(self):
        config_content = rcf.read_config_file(self.cfg)

        try:
            # directory tests
            ccp.test_ifexists(os.path.join(get_iota2_project_dir(), "iota2"))
            ccp.test_ifexists(config_content.cfg.chain.nomenclature_path)
            ccp.test_ifexists(config_content.cfg.chain.ground_truth)
            ccp.test_ifexists(config_content.cfg.chain.color_table)
            if config_content.cfg.chain.s2_output_path:
                ccp.test_ifexists(config_content.cfg.chain.s2_output_path)
            if config_content.cfg.chain.s2_s2c_output_path:
                ccp.test_ifexists(config_content.cfg.chain.s2_s2c_output_path)
            # TODO et les autres capteurs ?
            ccp.test_var_config_file(config_content.cfg, "chain", "first_step",
                                     str, [
                                         "init",
                                         "sampling",
                                         "dimred",
                                         "learning",
                                         "classification",
                                         "mosaic",
                                         "validation",
                                         "regularisation",
                                         "crown",
                                         "mosaictiles",
                                         "vectorisation",
                                         "simplification",
                                         "smoothing",
                                         "clipvectors",
                                         "lcstatistics",
                                     ])
            ccp.test_var_config_file(config_content.cfg, "chain", "last_step",
                                     str, [
                                         "init",
                                         "sampling",
                                         "dimred",
                                         "learning",
                                         "classification",
                                         "mosaic",
                                         "validation",
                                         "regularisation",
                                         "crown",
                                         "mosaictiles",
                                         "vectorisation",
                                         "simplification",
                                         "smoothing",
                                         "clipvectors",
                                         "lcstatistics",
                                     ])
        except sErr.configFileError as err:
            raise err

        ccp.is_field_in_vector_data(config_content.cfg.chain.ground_truth,
                                    config_content.cfg.chain.data_field,
                                    "Integer")
        if config_content.cfg.chain.region_path:
            ccp.is_field_in_vector_data(config_content.cfg.chain.region_path,
                                        config_content.cfg.chain.region_field,
                                        "String")
            ccp.region_vector_field_as_string(config_content.cfg)

    def check_mandatory_section(self):

        config_cont = rcf.read_config_file(self.cfg)
        # if arg_train is not readed in config file and scikit model not enabled
        # return a configError
        list_readed_section = config_cont.backlog_params["readed"]
        if config_cont.cfg.scikit_models_parameters.model_type is None:
            if "arg_train" not in list_readed_section:
                raise sErr.configError(
                    f"'arg_train' is empty in file {self.cfg}"
                    " Choosing a classifier is mandatory")

    def check_compat_param(self):
        config_content = rcf.read_config_file(self.cfg)
        # parameters compatibilities check
        classier_probamap_avail = ["sharkrf"]
        if (config_content.getParam("arg_classification",
                                    "enable_probability_map") is True
                and config_content.getParam("arg_train", "classifier").lower()
                not in classier_probamap_avail):
            raise sErr.configError(
                "'enable_probability_map:True' only available with "
                "the 'sharkrf' classifier")
        if (config_content.getParam("chain", "region_path") is None
                and config_content.cfg.arg_classification.classif_mode
                == "fusion"):
            raise sErr.configError(
                "you can't chose 'one_region' mode and ask a fusion of "
                "classifications\n")
        if (config_content.cfg.arg_classification.merge_final_classifications
                and config_content.cfg.arg_train.runs == 1):
            raise sErr.configError(
                "these parameters are incompatible runs:1 and"
                " merge_final_classifications:True")
        if (config_content.cfg.arg_train.split_ground_truth is False
                and config_content.cfg.arg_train.runs != 1):
            raise sErr.configError(
                "these parameters are incompatible split_ground_truth : False and "
                "runs different from 1")
        if (config_content.cfg.arg_classification.merge_final_classifications
                and config_content.cfg.arg_train.split_ground_truth is False):
            raise sErr.configError(
                "these parameters are incompatible merge_final_classifications"
                ":True and split_ground_truth : False")
        if (config_content.cfg.arg_train.dempster_shafer_sar_opt_fusion
                and 'None' in config_content.cfg.chain.s1_path):
            raise sErr.configError(
                "these parameters are incompatible dempster_shafer_SAR"
                "_Opt_fusion : True and s1_path : 'None'")
        if (config_content.cfg.arg_train.dempster_shafer_sar_opt_fusion
                and 'None' in config_content.cfg.chain.user_feat_path
                and 'None' in config_content.cfg.chain.l5_path_old
                and 'None' in config_content.cfg.chain.l8_path
                and 'None' in config_content.cfg.chain.l8_path_old
                and 'None' in config_content.cfg.chain.s2_path
                and 'None' in config_content.cfg.chain.s2_s2c_path):
            raise sErr.configError(
                "to perform post-classification fusion, optical data must be "
                "used")
        if (config_content.cfg.scikit_models_parameters.model_type is not None
                and config_content.cfg.arg_train.enable_autocontext is True):
            raise sErr.configError(
                "these parameters are incompatible enable_autocontext : True"
                " and model_type")
        if (config_content.cfg.scikit_models_parameters.model_type is not None
                and
                config_content.cfg.external_features.external_features_flag):
            raise sErr.configError(
                "these parameters are incompatible external_features "
                "and scikit_models_parameters")
        if (config_content.cfg.external_features.external_features_flag
                and config_content.cfg.arg_train.enable_autocontext):
            raise sErr.configError(
                "these parameters are incompatible external_features "
                "and enable_autocontext")

        return True

    def init_dict_control_variables(self):
        # control variable
        config_content = rcf.read_config_file(self.cfg)
        self.control_var["tile_list"] = config_content.getParam(
            "chain", "list_tile").split(" ")
        self.control_var["check_inputs"] = config_content.getParam(
            "chain", "check_inputs")
        self.control_var["Sentinel1"] = config_content.getParam(
            'chain', 's1_path')
        self.control_var["shapeRegion"] = config_content.getParam(
            'chain', 'region_path')
        self.control_var["classif_mode"] = config_content.getParam(
            'arg_classification', 'classif_mode')
        self.control_var["sample_management"] = config_content.getParam(
            'arg_train', 'sample_management')
        self.control_var["sample_augmentation"] = dict(
            config_content.getParam('arg_train', 'sample_augmentation'))
        self.control_var["sample_augmentation_flag"] = self.control_var[
            "sample_augmentation"]["activate"]
        self.control_var["dimred"] = config_content.getParam(
            'dim_red', 'dim_red')
        self.control_var["classifier"] = config_content.getParam(
            'arg_train', 'classifier')
        self.control_var["ds_sar_opt"] = config_content.getParam(
            'arg_train', 'dempster_shafer_sar_opt_fusion')
        self.control_var["keep_runs_results"] = config_content.getParam(
            'arg_classification', 'keep_runs_results')
        self.control_var[
            "merge_final_classifications"] = config_content.getParam(
                'arg_classification', 'merge_final_classifications')
        self.control_var["ground_truth"] = config_content.getParam(
            'chain', 'ground_truth')
        self.control_var["runs"] = config_content.getParam('arg_train', 'runs')
        self.control_var["data_field"] = config_content.getParam(
            'chain', 'data_field')
        self.control_var["outStat"] = config_content.getParam(
            'chain', 'output_statistics')
        self.control_var["VHR"] = config_content.getParam(
            'coregistration', 'vhr_path')
        self.control_var["gridsize"] = config_content.getParam(
            'simplification', 'gridsize')
        self.control_var["umc1"] = config_content.getParam(
            'simplification', 'umc1')
        self.control_var["umc2"] = config_content.getParam(
            'simplification', 'umc2')
        self.control_var["rssize"] = config_content.getParam(
            'simplification', 'rssize')
        self.control_var["inland"] = config_content.getParam(
            'simplification', 'inland')
        self.control_var["iota2_outputs_dir"] = config_content.getParam(
            'chain', 'output_path')
        self.control_var["use_scikitlearn"] = config_content.getParam(
            'scikit_models_parameters', 'model_type') is not None
        self.control_var["nomenclature"] = config_content.getParam(
            'simplification', 'nomenclature')
        self.control_var["enable_autocontext"] = config_content.getParam(
            'arg_train', 'enable_autocontext')
        self.control_var["enable_external_features"] = config_content.getParam(
            "external_features", "external_features_flag")
        self.control_var["remove_output_path"] = config_content.getParam(
            "chain", "remove_output_path")
        self.control_var["sampling_validation"] = config_content.getParam(
            "arg_train", "sampling_validation")

    def init_step_group(self, s_container):
        """
        Initialize initialisation steps
        """
        from iota2.Steps import check_inputs_step
        from iota2.Steps import sensorsPreprocess
        from iota2.Steps import Coregistration
        from iota2.Steps import CommonMasks
        from iota2.Steps import PixelValidity
        from iota2.Steps import slicSegmentation

        if self.control_var["check_inputs"]:
            s_container.append(
                partial(check_inputs_step.check_inputs_classif_workflow,
                        self.cfg, self.config_resources), "init")
        s_container.append(
            partial(sensorsPreprocess.sensorsPreprocess, self.cfg,
                    self.config_resources, self.workingDirectory), "init")
        if self.control_var["VHR"]:
            s_container.append(
                partial(Coregistration.Coregistration, self.cfg,
                        self.config_resources, self.workingDirectory), "init")
        s_container.append(
            partial(CommonMasks.CommonMasks, self.cfg, self.config_resources,
                    self.workingDirectory), "init")
        s_container.append(
            partial(PixelValidity.PixelValidity, self.cfg,
                    self.config_resources, self.workingDirectory), "init")
        if self.control_var["enable_autocontext"]:
            s_container.append(
                partial(slicSegmentation.slicSegmentation, self.cfg,
                        self.config_resources, self.workingDirectory), "init")

    def sampling_step_group(self, first_container: StepContainer,
                            second_container: StepContainer):
        """
        Declare each step for samples management
        """
        from iota2.Steps import Envelope
        from iota2.Steps import genRegionVector
        from iota2.Steps import VectorFormatting
        from iota2.Steps import splitSamples
        from iota2.Steps import samplesMerge
        from iota2.Steps import statsSamplesModel
        from iota2.Steps import samplingLearningPolygons
        from iota2.Steps import superPixPos
        from iota2.Steps import samplesByTiles
        from iota2.Steps import samplesExtraction
        from iota2.Steps import samplesByModels
        from iota2.Steps import copySamples
        from iota2.Steps import genSyntheticSamples
        from iota2.Steps import samplesDimReduction
        from iota2.Steps import superPixSplit

        first_container.append(
            partial(Envelope.Envelope, self.cfg, self.config_resources,
                    self.workingDirectory), "sampling")
        if not self.control_var["shapeRegion"]:
            first_container.append(
                partial(genRegionVector.genRegionVector, self.cfg,
                        self.config_resources, self.workingDirectory),
                "sampling")
        first_container.append(
            partial(VectorFormatting.VectorFormatting, self.cfg,
                    self.config_resources, self.workingDirectory), "sampling")
        huge_models = False
        if self.control_var["shapeRegion"] and self.control_var[
                "classif_mode"] == "fusion":
            huge_models = True
            first_container.append(
                partial(splitSamples.splitSamples, self.cfg,
                        self.config_resources, self.workingDirectory),
                "sampling")

        second_container.append(
            partial(samplesMerge.samplesMerge, self.cfg, self.config_resources,
                    self.workingDirectory, huge_models), "sampling")
        second_container.append(
            partial(statsSamplesModel.statsSamplesModel, self.cfg,
                    self.config_resources, self.workingDirectory), "sampling")
        second_container.append(
            partial(samplingLearningPolygons.samplingLearningPolygons,
                    self.cfg, self.config_resources, self.workingDirectory),
            "sampling")
        if self.control_var["enable_autocontext"]:
            second_container.append(
                partial(superPixPos.superPixPos, self.cfg,
                        self.config_resources, self.workingDirectory),
                "sampling")
        second_container.append(
            partial(samplesByTiles.samplesByTiles, self.cfg,
                    self.config_resources,
                    self.control_var["enable_autocontext"],
                    self.workingDirectory), "sampling")
        second_container.append(
            partial(samplesExtraction.samplesExtraction, self.cfg,
                    self.config_resources, self.workingDirectory), "sampling")
        if not self.control_var["enable_autocontext"]:
            second_container.append(
                partial(samplesByModels.samplesByModels, self.cfg,
                        self.config_resources), "sampling")
            transfert_samples = False
            if self.control_var["sample_management"] and self.control_var[
                    "sample_management"].lower() != 'none':
                transfert_samples = True
                second_container.append(
                    partial(copySamples.copySamples, self.cfg,
                            self.config_resources, self.workingDirectory),
                    "sampling")
            if self.control_var["sample_augmentation_flag"]:
                second_container.append(
                    partial(genSyntheticSamples.genSyntheticSamples, self.cfg,
                            self.config_resources, transfert_samples,
                            self.workingDirectory), "sampling")
            if self.control_var["dimred"]:
                second_container.append(
                    partial(
                        samplesDimReduction.samplesDimReduction, self.cfg,
                        self.config_resources, transfert_samples
                        and not self.control_var["sample_augmentation_flag"],
                        self.workingDirectory), "sampling")
        else:
            second_container.append(
                partial(superPixSplit.superPixSplit, self.cfg,
                        self.config_resources, self.workingDirectory),
                "sampling")

    def learning_step_group(self, s_container: StepContainer):
        """
        Initialize learning step
        """
        from iota2.Steps import learnModel
        s_container.append(
            partial(learnModel.learnModel, self.cfg, self.config_resources,
                    self.workingDirectory), "learning")

    def classification_step_group(self, s_container: StepContainer):
        """
        Initialize classification steps
        """

        from iota2.Steps import classification
        from iota2.Steps import skClassificationsMerge
        from iota2.Steps import confusionSAROpt
        from iota2.Steps import confusionSAROptMerge
        from iota2.Steps import SAROptFusion
        from iota2.Steps import classificationsFusion
        from iota2.Steps import fusionsIndecisions

        s_container.append(
            partial(classification.classification, self.cfg,
                    self.config_resources, self.workingDirectory),
            "classification")
        if self.control_var["use_scikitlearn"] or self.control_var[
                "enable_external_features"]:
            s_container.append(
                partial(skClassificationsMerge.ScikitClassificationsMerge,
                        self.cfg, self.config_resources,
                        self.workingDirectory), "classification")
        if self.control_var["ds_sar_opt"]:
            s_container.append(
                partial(confusionSAROpt.confusionSAROpt, self.cfg,
                        self.config_resources, self.workingDirectory),
                "classification")
            s_container.append(
                partial(confusionSAROptMerge.confusionSAROptMerge, self.cfg,
                        self.config_resources, self.workingDirectory),
                "classification")
            s_container.append(
                partial(SAROptFusion.SAROptFusion, self.cfg,
                        self.config_resources, self.workingDirectory),
                "classification")
        if self.control_var["classif_mode"] == "fusion" and self.control_var[
                "shapeRegion"]:
            s_container.append(
                partial(classificationsFusion.classificationsFusion, self.cfg,
                        self.config_resources, self.workingDirectory),
                "classification")
            s_container.append(
                partial(fusionsIndecisions.fusionsIndecisions, self.cfg,
                        self.config_resources, self.workingDirectory),
                "classification")

    def mosaic_step_group(self, s_container: StepContainer):
        """
        Initialize mosaic step
        """
        from iota2.Steps import mosaic
        s_container.append(
            partial(mosaic.mosaic, self.cfg, self.config_resources,
                    self.workingDirectory), "mosaic")

    def validation_steps_group(self, s_container: StepContainer):
        """
        """
        from iota2.Steps import confusionCmd
        from iota2.Steps import confusionsMerge
        from iota2.Steps import reportGeneration
        from iota2.Steps import mergeSeedClassifications

        s_container.append(
            partial(confusionCmd.confusionCmd, self.cfg, self.config_resources,
                    self.workingDirectory), "validation")
        if self.control_var["keep_runs_results"]:
            s_container.append(
                partial(confusionsMerge.confusionsMerge, self.cfg,
                        self.config_resources, self.workingDirectory),
                "validation")
            s_container.append(
                partial(reportGeneration.reportGeneration, self.cfg,
                        self.config_resources, self.workingDirectory),
                "validation")
        if (self.control_var["merge_final_classifications"]
                and self.control_var["runs"] > 1):
            s_container.append(
                partial(mergeSeedClassifications.mergeSeedClassifications,
                        self.cfg, self.config_resources,
                        self.workingDirectory), "validation")

    def get_outputs_data(self) -> Dict[str, str]:
        data = {}
        data["mosaic_classifications"] = {}
        data["mosaic_confidence"] = {}
        for seed in range(self.control_var["runs"]):
            data["mosaic_classifications"][seed] = os.path.join(
                self.control_var["iota2_outputs_dir"], "final",
                f"Classif_Seed_{seed}.tif")
            data["mosaic_confidence"][seed] = os.path.join(
                self.control_var["iota2_outputs_dir"], "final",
                f"Confidence_Seed_{seed}.tif")
        data["mosaic_validity"] = os.path.join(
            self.control_var["iota2_outputs_dir"], "final",
            "PixelsValidity.tif")
        if self.control_var["merge_final_classifications"]:
            data["mosaic_fusion_of_classifications"] = os.path.join(
                self.control_var["iota2_outputs_dir"], "final",
                "Classifications_fusion.tif")
        return data

    def generate_output_directories(self,
                                    first_step_index: int,
                                    restart: bool,
                                    builder_index: Optional[int] = 0):
        """
        """
        i2_output_dir = self.control_var["iota2_outputs_dir"]
        rm_if_exists = self.control_var["remove_output_path"]

        task_status_file = os.path.join(i2_output_dir,
                                        I2_CONST.i2_tasks_status_filename)

        restart = restart and os.path.exists(task_status_file)
        rm_if_exists = rm_if_exists and os.path.exists(i2_output_dir)
        if rm_if_exists and first_step_index != 0 or restart:
            pass
        elif builder_index == 0 and rm_if_exists:
            shutil.rmtree(i2_output_dir)

        if restart:
            pass
        elif (first_step_index == 0
              and not restart) or not os.path.exists(i2_output_dir):
            generate_directories(
                i2_output_dir, self.control_var["merge_final_classifications"],
                self.control_var["tile_list"])
        else:
            pass
