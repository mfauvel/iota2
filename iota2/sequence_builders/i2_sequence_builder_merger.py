#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging
from typing import List
from typing import Union
from typing import Optional
from collections import OrderedDict

from iota2.Steps.IOTA2Step import StepContainer
from iota2.sequence_builders.i2_sequence_builder import i2_builder
from iota2.sequence_builders.i2_sequence_builder import waiting_i2_graph

LOGGER = logging.getLogger("distributed.worker")


class workflow_merger(i2_builder):
    """class dedicated to merge i2_sequence_builder objects
    """
    def __init__(self,
                 builders_list: List,
                 cfg: str,
                 config_resources: str,
                 schduler_type: str,
                 restart: Optional[bool] = False,
                 tasks_states_file: Optional[str] = None,
                 hpc_working_directory: Optional[str] = "TMPDIR"):
        """
        """
        super().__init__(cfg, config_resources, schduler_type, restart,
                         tasks_states_file, hpc_working_directory)
        self.builders_instance = []
        self.builders_len = []
        for builder in builders_list:
            dummy_builder = builder(cfg, config_resources, schduler_type,
                                    restart, tasks_states_file,
                                    hpc_working_directory)
            self.builders_instance.append(dummy_builder)
            self.builders_len.append(dummy_builder.get_steps_sequence_size())
            for group_name, _ in dummy_builder.steps_group.items():
                self.steps_group[group_name] = OrderedDict()
        #build steps
        self.sort_step()

    def get_builders_steps(self, first_step_index: int, last_step_index: int,
                           *builders_length):
        dico = {}
        len_container_buff = 0
        max_step_index = 0
        for builder_num, length in enumerate(builders_length):
            dico[builder_num] = OrderedDict()
            for cpt in range(length):
                indice = cpt + len_container_buff
                dico[builder_num][indice] = cpt
                max_step_index += 1
            len_container_buff = length

        if last_step_index > max_step_index - 1:
            last_step_index = max_step_index - 1
        list_ind = []
        for builder_num, length in enumerate(builders_length):
            if first_step_index in dico[
                    builder_num] and not last_step_index in dico[builder_num]:
                list_ind.append(
                    (dico[builder_num][first_step_index],
                     dico[builder_num][next(reversed(dico[builder_num]))]))
            elif last_step_index in dico[
                    builder_num] and not first_step_index in dico[builder_num]:
                list_ind.append((0, dico[builder_num][last_step_index]))
            elif last_step_index in dico[
                    builder_num] and first_step_index in dico[builder_num]:
                list_ind.append((dico[builder_num][first_step_index],
                                 dico[builder_num][last_step_index]))
            else:
                list_ind.append([])
        return list_ind

    def build_steps(
            self,
            cfg: str,
            config_ressources: Optional[str] = None) -> List[StepContainer]:
        steps_containers = []
        for builder in self.builders_instance:
            steps_containers += builder.build_steps(cfg, config_ressources)
        return steps_containers

    def generate_output_directories(self, first_step_index: int,
                                    restart: bool):

        for builder_index, builder in enumerate(self.builders_instance):
            builder.generate_output_directories(first_step_index, restart,
                                                builder_index)

    def pre_check(self):
        """
        """
        for builder in self.builders_instance:
            builder.pre_check()

    def get_final_i2_exec_graph(self, first_step_index, last_step_index,
                                graph_figures):
        """
        """
        final_graph = []
        graphs_per_builders = OrderedDict()
        step_to_run = self.get_builders_steps(first_step_index,
                                              last_step_index,
                                              *self.builders_len)
        for tuple_start_end, builder in zip(step_to_run,
                                            self.builders_instance):
            if tuple_start_end:
                new_graphs = builder.get_final_i2_exec_graph(
                    tuple_start_end[0], tuple_start_end[1], "")
                # final_graph += new_graphs
                for wating_new_graph in new_graphs:
                    final_graph.append(
                        waiting_i2_graph(
                            self, wating_new_graph.container,
                            wating_new_graph.starting_step,
                            wating_new_graph.ending_step,
                            wating_new_graph.output_graph,
                            wating_new_graph.container.prelaunch_function,
                            wating_new_graph.figure_suffix))
                builder_name = type(builder).__name__
                graphs_per_builders[builder_name] = len(new_graphs)

        if not graph_figures:
            return final_graph

        # the purpose of the following is the feed the right output figures filenames
        default_graph_fig_names = []
        for builder_name, builder_size in graphs_per_builders.items():
            builder_graphs = []
            for builder_graph_ind in range(builder_size):
                builder_graphs.append(
                    f"{builder_name}_graph_{builder_graph_ind + 1}.png")
            default_graph_fig_names.append(builder_graphs)

        graph_figures_list = []
        default_fig_directory, _ = os.path.split(graph_figures[0])
        ind = 0
        for builder_fig_list in default_graph_fig_names:
            buff = []
            for file_name in builder_fig_list:
                try:
                    buff.append(graph_figures[ind])
                except IndexError:
                    buff.append(os.path.join(default_fig_directory, file_name))

                ind += 1
            graph_figures_list.append(buff)
        final_graph = []
        graph_figures_ind = 0
        for tuple_start_end, builder in zip(step_to_run,
                                            self.builders_instance):
            if tuple_start_end:
                new_graphs = builder.get_final_i2_exec_graph(
                    tuple_start_end[0], tuple_start_end[1],
                    graph_figures_list[graph_figures_ind])
                for wating_new_graph in new_graphs:
                    final_graph.append(
                        waiting_i2_graph(
                            self, wating_new_graph.container,
                            wating_new_graph.starting_step,
                            wating_new_graph.ending_step,
                            wating_new_graph.output_graph,
                            wating_new_graph.container.prelaunch_function,
                            wating_new_graph.figure_suffix))
                # final_graph += new_graphs
                graph_figures_ind += 1
        return final_graph
