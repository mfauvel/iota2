import numpy as np
from typing import Tuple, List


# ############################################################################
# S2 tests utilities functions
# ############################################################################
def get_identity(self):
    # print("get identity")
    # print(self.get_B2())

    return self.get_interpolated_Sentinel2_B2(), []


def test_index_sum(self):
    coef = self.get_interpolated_Sentinel2_B2(
    ) + self.get_interpolated_Sentinel2_B4()
    labels = []
    return np.sum(coef, axis=2), labels


def test_index(self):
    coef = self.get_interpolated_Sentinel2_B2(
    ) + self.get_interpolated_Sentinel2_B4()
    labels = []
    return coef, labels


def duplicate_ndvi(self):
    ndvi = self.get_interpolated_Sentinel2_NDVI()
    labels = [f"dupndvi_{i+1}" for i in range(ndvi.shape[2])]
    return ndvi, labels


def get_ndvi(self):
    coef = (self.get_interpolated_Sentinel2_B8() -
            self.get_interpolated_Sentinel2_B4()) / (
                self.get_interpolated_Sentinel2_B4() +
                self.get_interpolated_Sentinel2_B8() + 1E-9)
    labels = [f"ndvi_{i+1}" for i in range(coef.shape[2])]
    return coef, labels


def custom_function(self):
    print(dir(self))
    print(self.get_interpolated_Sentinel2_b1(),
          self.get_interpolated_Sentinel2_b2())
    return self.get_interpolated_Sentinel2_b1(
    ) + self.get_interpolated_Sentinel2_b2()


def custom_function_inv(self):
    print(dir(self))
    print(self.get_interpolated_Sentinel2_b1(),
          self.get_interpolated_Sentinel2_b2())
    return self.get_interpolated_Sentinel2_b1(
    ) - self.get_interpolated_Sentinel2_b2()


def get_ndsi(self):
    coef = (self.get_interpolated_Sentinel2_B3() -
            self.get_interpolated_Sentinel2_B11()) / (
                self.get_interpolated_Sentinel2_B3() +
                self.get_interpolated_Sentinel2_B11())
    labels = [f"ndsi_{i+1}" for i in range(coef.shape[2])]
    print("out custom features")
    return coef, labels


def get_cari(self):
    a = ((self.get_interpolated_Sentinel2_B5() -
          self.get_interpolated_Sentinel2_B3()) /
         150) * 670 + self.get_interpolated_Sentinel2_B4() + (
             self.get_interpolated_Sentinel2_B3() -
             ((self.get_interpolated_Sentinel2_B5() -
               self.get_interpolated_Sentinel2_B3()) / 150) * 550)
    b = ((self.get_interpolated_Sentinel2_B5() -
          self.get_interpolated_Sentinel2_B3()) / (150 * 150)) + 1
    coef = (self.get_interpolated_Sentinel2_B5() /
            self.get_interpolated_Sentinel2_B4()) * ((np.sqrt(a * a)) /
                                                     (np.sqrt(b)))
    labels = [f"cari_{i+1}" for i in range(coef.shape[2])]

    return coef, labels


def get_red_edge2(self):
    coef = (self.get_interpolated_Sentinel2_B5() -
            self.get_interpolated_Sentinel2_B4()) / (
                self.get_interpolated_Sentinel2_B5() +
                self.get_interpolated_Sentinel2_B4())
    labels = [f"rededge2_{i+1}" for i in range(coef.shape[2])]
    return coef, labels


def get_gndvi(self):
    """
        compute the Green Normalized Difference Vegetation Index
        DO NOT USE this except for test as Sentinel2 has not B9
    """
    coef = (self.get_interpolated_Sentinel2_B9() -
            self.get_interpolated_Sentinel2_B3()) / (
                self.get_interpolated_Sentinel2_B9() +
                self.get_interpolated_Sentinel2_B3())
    labels = [f"gndvi_{i+1}" for i in range(coef.shape[2])]
    return coef, labels


def get_soi(self) -> Tuple[np.ndarray, List[str]]:
    """
    compute the Soil Composition Index
    
    Functions get_interpolated_Sentinel2_B2() returns numpy array
     of size N * M * D where:
    - N the number of row, computed by iota2
    - M the number of columns, computed by iota2
    - D the number of dimension == the number of dates

    The output is a tuple of coef and labels.
    - Labels must be a list (even empty)
    - Coef is a numpy array, with an expected shape of N * M * X
    Where X is a dimension, which can be different to D, from 1 to infinite

    The user can force the output data type by converting it.
    
    The user must manage special cases like:
    - division by 0
    - division which result to near infinite or NaN

    Parameters
    ----------
    self:
        self is the only parameter for external functions
    """
    coef = (self.get_interpolated_Sentinel2_B11() -
            self.get_interpolated_Sentinel2_B8()) / (
                self.get_interpolated_Sentinel2_B11() +
                self.get_interpolated_Sentinel2_B8())
    labels = [f"soi_{i+1}" for i in range(coef.shape[2])]
    return coef, labels


def get_raw_data(self):
    coef, labels_refl = self.get_filled_stack()
    masks, label_masks = self.get_filled_masks()
    coef = np.concatenate((coef, masks), axis=2)
    labels = labels_refl + label_masks
    return coef, labels


def use_raw_data(self):
    coef = self.get_raw_Sentinel2_B2()
    labels = []
    return coef, labels


def use_exogeneous_data(self):
    coef = self.get_interpolated_Sentinel2_B2()
    labels = self.get_interpolated_dates()["Sentinel2"]
    exo_data = self.get_exogeneous_data()
    coef *= exo_data
    labels = ["exo_" + lab for lab in labels]
    return coef, labels


def use_raw_and_masks_data(self):
    """
    This function use raw data and masks
    
    This test function get the B2 from Sentinel2 and set to
    -1000 all pixels masked
    
    The output is converted to int16.
    """

    import numpy.ma as ma
    band = self.get_raw_Sentinel2_B2()
    mask = self.get_Sentinel2_binary_masks()
    coef = ma.masked_array(band, mask=mask)
    coef = coef.filled(-1000)
    return coef.astype("int16"), []


# ############################################################################
# DHI INDEX
# ############################################################################


def get_cumulative_productivity(self):
    print("cumul")
    coef = np.sum(self.get_interpolated_Sentinel2_NDVI(), axis=2)
    labels = ["cumul_prod"]
    return coef, labels


def get_minimum_productivity(self):
    print("min")
    coef = np.min(self.get_interpolated_Sentinel2_NDVI(), axis=2)
    labels = ["min_prod"]
    return coef, labels


def get_seasonal_variation(self):
    print("var")
    coef = (np.std(self.get_interpolated_Sentinel2_NDVI(), axis=2) /
            (np.mean(self.get_interpolated_Sentinel2_NDVI(), axis=2) + 1E-6))

    labels = ["var_prod"]
    return coef, labels


# ###########################################################################
# Functions for testing all sensors
# ###########################################################################


def test_index_sum_l5_old(self):
    coef = self.get_interpolated_Landsat5Old_B2(
    ) + self.get_interpolated_Landsat5Old_B4()
    labels = []
    return np.sum(coef, axis=2), labels


def test_index_l5_old(self):
    coef = self.get_interpolated_Landsat5Old_B2(
    ) + self.get_interpolated_Landsat5Old_B4()
    labels = []
    return coef, labels


def test_index_sum_l8_old(self):
    coef = self.get_interpolated_Landsat8Old_B2(
    ) + self.get_interpolated_Landsat8Old_B4()
    labels = []
    return np.sum(coef, axis=2), labels


def test_index_l8_old(self):
    coef = self.get_interpolated_Landsat8Old_B2(
    ) + self.get_interpolated_Landsat8Old_B4()
    labels = []
    return coef, labels


def test_index_sum_l8(self):
    coef = self.get_interpolated_Landsat8_B2(
    ) + self.get_interpolated_Landsat8_B4()
    labels = []
    return np.sum(coef, axis=2), labels


def test_index_l8(self):
    coef = self.get_interpolated_Landsat8_B2(
    ) + self.get_interpolated_Landsat8_B4()
    labels = []
    return coef, labels


def test_index_sum_s2_s2c(self):
    coef = self.get_interpolated_Sentinel2S2C_B02(
    ) + self.get_interpolated_Sentinel2S2C_B04()
    labels = []
    return np.sum(coef, axis=2), labels


def test_index_s2_s2c(self):
    coef = self.get_interpolated_Sentinel2S2C_B02(
    ) + self.get_interpolated_Sentinel2S2C_B04()
    labels = []
    return coef, labels


def test_index_sum_s2_l3a(self):
    coef = self.get_interpolated_Sentinel2L3A_B2(
    ) + self.get_interpolated_Sentinel2L3A_B4()
    labels = []
    return np.sum(coef, axis=2), labels


def test_index_s2_l3a(self):
    coef = self.get_interpolated_Sentinel2L3A_B2(
    ) + self.get_interpolated_Sentinel2L3A_B4()
    labels = []
    return coef, labels
