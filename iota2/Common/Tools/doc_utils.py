#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import collections
from iota2.Common.FileUtils import get_iota2_project_dir


def generate_list_steps_files(output_path: str) -> None:
    """
    Load configurations files and write corresponding
    steps rst files

    Parameters
    ----------

    output_path:
        path to write files
    """
    # Classif tutorial
    config_file = os.path.join(output_path, "examples",
                               "config_tutorial_classification.cfg")
    steps_file = os.path.join(output_path, "examples",
                              "steps_classification.txt")
    os.system(f"Iota2.py -config {config_file} -only_summary > {steps_file}")
    # Vecto tutorial
    config_file = os.path.join(output_path, "examples",
                               "config_tutorial_vectorization.cfg")
    steps_file = os.path.join(output_path, "examples",
                              "steps_vectorization.txt")
    os.system(f"Iota2.py -config {config_file} -only_summary > {steps_file}")
    # features maps
    config_file = os.path.join(output_path, "examples",
                               "config_tutorial_features_maps.cfg")
    steps_file = os.path.join(output_path, "examples",
                              "steps_features_maps.txt")
    os.system(f"Iota2.py -config {config_file} -only_summary > {steps_file}")
    # obia
    config_file = os.path.join(output_path, "examples",
                               "config_tutorial_obia.cfg")
    steps_file = os.path.join(output_path, "examples", "steps_obia.txt")
    os.system(f"Iota2.py -config {config_file} -only_summary > {steps_file}")


def generate_doc_files() -> None:
    """ Calls all functions to generate documentation"""
    iota2_dir = get_iota2_project_dir()
    output_path = os.path.join(iota2_dir, "doc", "source")
    if not os.path.exists(output_path):
        os.mkdir(output_path)
    create_doc_dataframe(output_path)
    generate_list_steps_files(output_path)


def create_doc_dataframe(output_path: str) -> None:
    """
    use a dataframe to manage the rst parameters table

    Parameters
    ----------
    output_path:
        path to write files
    """
    import iota2.configuration_files.default_config_parameters as mod
    from inspect import getmembers, isclass
    import pandas as pd
    attr_list = [
        fun for fun in getmembers(mod)
        if isclass(fun[1]) and fun[0].startswith("init")
    ]
    df_doc = pd.DataFrame(columns=('Builder', "Section", "Name", "Value",
                                   'Description', "Type_exp", "long_desc",
                                   "Mandatory"))
    ind = 0
    builders_list = []
    for attr in attr_list:
        param = attr[1]()
        for builder in param.builders:
            if builder not in builders_list:
                builders_list.append(builder)
            df_doc.loc[ind] = [
                builder, param.section, param.name,
                str(param.value), param.desc, param.type_exp.__name__,
                param.long_desc, param.builders[builder]
            ]
            ind += 1

    glob_chain = (".. list-table::\n" + " " * 6 + ":widths: auto\n" + " " * 6 +
                  ":header-rows: 1\n\n" + " " * 6 + "* - Name\n" + " " * 8 +
                  "- Default Value\n" + " " * 8 + "- Description\n" + " " * 8 +
                  "- Type\n" + " " * 8 + "- Name\n\n")

    check_list = {}
    for builder in builders_list:
        output_file = os.path.join(output_path, builder + "_builder.rst")
        ss_df = df_doc[df_doc["Builder"] == builder]
        sections = list(ss_df["Section"].unique())
        sections = sorted(sections)
        with open(output_file, "w") as out_file:
            out_file.write(f"{builder}\n")
            out_file.write("#" * len(builder) + "\n" * 2)
            for section in sections:
                chain = (".. list-table::\n" + " " * 6 + ":widths: auto\n" +
                         " " * 6 + ":header-rows: 1\n\n" + " " * 6 +
                         "* - Name\n" + " " * 8 + "- Default Value\n" +
                         " " * 8 + "- Description\n" + " " * 8 + "- Type\n" +
                         " " * 8 + "- Mandatory\n" + " " * 8 + "- Name\n\n")

                out_file.write(f".. _{builder}.{section}:\n\n")
                out_file.write(f"{section}\n")
                out_file.write("*" * (len(section)) + "\n\n")
                ss_df1 = ss_df[ss_df["Section"] == section]

                ss_df1 = ss_df1.drop(["Builder", "Section"], axis="columns")
                long_desc_dict = {}
                for row in ss_df1.iterrows():
                    # values[0] -> parameters name
                    # values[1] -> default value
                    # values[2] -> short description
                    # values[3] -> type
                    # values[4] -> long description
                    # values[5] -> mandatory
                    values = row[1].values
                    param_name = values[0]
                    if values[4] is not None:
                        long_desc_dict[values[0]] = [
                            values[4],
                            f".. _desc_{builder}.{section}.{values[0]}:",
                            f"{builder}.{section}.{values[0]}"
                        ]
                        param_name = f":ref:`{values[0]} <desc_{builder}.{section}.{values[0]}>`"
                        # param_name = f":ref:`{values[0]} <{values[0]}>`"
                    if values[0] in ["builders_paths", "module"]:
                        values[1] = "/path/to/iota2/sources"

                    ref_lab = (" " * 10 +
                               f".. _{builder}.{section}.{values[0]}:\n" +
                               " " * 6 + f"* - {param_name}\n" + " " * 8 +
                               f"- {values[1]}\n" + " " * 8 +
                               f"- {values[2]}\n" + " " * 8 +
                               f"- {values[3]}\n" + " " * 8 +
                               f"- {values[5]}\n" + " " * 8 +
                               f"- {param_name}\n\n" + "\n")
                    chain += ref_lab
                    if values[0] not in check_list:
                        check_list[values[0]] = (" " * 6 +
                                                 f"* - {values[0]}\n" +
                                                 " " * 8 + f"- {values[1]}\n" +
                                                 " " * 8 + f"- {values[2]}\n" +
                                                 " " * 8 + f"- {values[3]}\n" +
                                                 " " * 8 +
                                                 f"- {values[0]}\n\n")

                out_file.write(chain)
                out_file.write("\n" * 3)
                if long_desc_dict:
                    out_file.write("Notes\n")
                    out_file.write(f"{'='*len('Notes')}\n\n")
                    for param in long_desc_dict:
                        out_file.write(long_desc_dict[param][1] + "\n\n")
                        title = f":ref:`{param} <{long_desc_dict[param][2]}>`"
                        out_file.write(title + "\n")
                        out_file.write(f"{'-'*len(title)}\n")

                        # out_file.write(f"{'-'*len(param)}\n\n")
                        out_file.write(long_desc_dict[param][0])
                        out_file.write("\n\n")
        glob_output_file = os.path.join(output_path, "all_parameters.rst")
        dico_glob = collections.OrderedDict(sorted(check_list.items()))
        for _, value in dico_glob.items():
            glob_chain += value
        with open(glob_output_file, "w") as out_file:
            title = "All configuration parameters"
            out_file.write(title + "\n")
            out_file.write("#" * len(title) + "\n\n")
            out_file.write(glob_chain)
            out_file.write("\n")


if __name__ == "__main__":

    generate_doc_files()
