#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Merge some shapefile simplified and clip them according to an another shape.

"""

import sys
import os
import shutil
import logging

from osgeo import ogr
from osgeo.ogr import Layer

from iota2.Common import FileUtils as fut
from iota2.Common import OtbAppBank as oa
from iota2.VectorTools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


def init_grass(path, debuglvl, epsg="2154"):
    """
    Initialisation of Grass GIS in lambert 93.

    in :
        path : directory where create grassdata directory
    """
    global gscript

    # Grass folder Initialisation
    if not os.path.exists(os.path.join(path, "grassdata")):
        os.mkdir(os.path.join(path, "grassdata"))
    path_grassdata = os.path.join(path, "grassdata")

    # Init Grass environment

    gisbase = os.environ["GRASSDIR"]

    gisdb = os.path.join(path_grassdata)
    sys.path.append(os.path.join(gisbase, "etc", "python"))
    os.environ["GISBASE"] = gisbase

    # Overwrite and verbose parameters
    debugdic = {
        "critical": "0",
        "error": "0",
        "warning": "1",
        "info": "2",
        "debug": "3",
    }
    os.environ["GRASS_OVERWRITE"] = "1"
    os.environ["GRASS_VERBOSE"] = debugdic[debuglvl]

    # Grass functions import
    import grass.script.setup as gsetup
    import grass.script as gscript

    # Init Grass
    gsetup.init(gisbase, gisdb)

    # Delete existing location
    if os.path.exists(os.path.join(gisdb, "demolocation")):
        shutil.rmtree(os.path.join(gisdb, "demolocation"))

    # Create the location in Lambert 93
    gscript.run_command("g.proj",
                        flags="c",
                        epsg=epsg,
                        location="demolocation")

    # Create datas mapset
    if not os.path.exists(os.path.join(gisdb, "/demolocation/datas")):
        try:
            gscript.start_command(
                "g.mapset",
                flags="c",
                mapset="datas",
                location="demolocation",
                dbase=gisdb,
            )
        except IOError as err:
            raise Exception("Folder '%s' does not own to current user") % (
                gisdb) from err


def get_tiles_files(zone,
                    tiles,
                    folder,
                    id_tile_field,
                    tile_name_prefix,
                    localenv,
                    fieldzone="",
                    valuezone="",
                    driver="ESRI Shapefile",
                    logger=LOGGER):
    """
    Find tiles (grid file) which intersect a vector file geometry
    """
    for ext in ['.shp', '.dbf', '.shx', '.prj']:
        shutil.copy(os.path.splitext(zone)[0] + ext, localenv)

    zone = os.path.join(localenv, os.path.basename(zone))

    driver = ogr.GetDriverByName(driver)
    if isinstance(zone, str):
        try:
            shape = driver.Open(zone, 0)
        except IOError as err:
            raise Exception('%s is not a vector file' % (zone)) from err

        lyr_zone = shape.GetLayer()
    elif isinstance(zone, Layer):
        lyr_zone = zone
        zone = None
        zone = lyr_zone.GetName() + '.shp'
    else:
        raise Exception('Zone parameter must be a shapefile or layer object')

    tiles = driver.Open(tiles, 0)
    lyr_tiles = tiles.GetLayer()

    list_files_tiles = []

    # get zone geometry
    field_type = vf.getFieldType(zone, fieldzone)
    if field_type == int:
        lyr_zone.SetAttributeFilter(fieldzone + " = " + str(valuezone))
    elif field_type == str:
        lyr_zone.SetAttributeFilter(fieldzone + " = \'%s\'" % (valuezone))
    else:
        raise Exception('Field type %s not handled' % (field_type))

    if lyr_zone.GetFeatureCount() != 0:
        for feat_zone in lyr_zone:

            geom_zone = feat_zone.GetGeometryRef()
            nbinter = 0
            # iterate tiles to find intersection
            for feat_tile in lyr_tiles:
                geom_tile = feat_tile.GetGeometryRef()
                nb_tile = int(feat_tile.GetField(id_tile_field))
                if geom_tile.Intersects(geom_zone):
                    nbinter += 1
                    tilename = os.path.join(
                        folder, tile_name_prefix + str(nb_tile) + '.tif')
                    if os.path.exists(tilename):
                        list_files_tiles.append(tilename)
                    else:
                        logger.info('Tile file %s does not exist', nb_tile)

    return list_files_tiles


def merge_tile_raster(path,
                      clipfile,
                      fieldclip,
                      valueclip,
                      tiles,
                      tilesfolder,
                      tile_id,
                      tile_name_prefix,
                      out,
                      working_dir=None,
                      logger=LOGGER):
    """
    merge raster files which intersect a vector file
    (filter on a field [and value]x)
    """
    if working_dir:
        path = working_dir

    if not os.path.exists(
            os.path.join(out,
                         "tile_" + fieldclip + "_" + str(valueclip) + '.tif')):
        localenv = os.path.join(path, "tmp%s" % (str(valueclip)))
        if os.path.exists(localenv):
            shutil.rmtree(localenv)
        os.mkdir(localenv)

        # Find vector tiles concerned by the given zone
        list_tiles_files = get_tiles_files(clipfile, tiles, tilesfolder,
                                           tile_id, tile_name_prefix, localenv,
                                           fieldclip, valueclip)

        outraster = os.path.join(
            localenv, "tile_" + fieldclip + "_" + str(valueclip) + '.tif')

        logger.info('Raster mosaic of zone %s', str(valueclip))

        if len(list_tiles_files) != 0:
            tomerge = []
            for rasttile in list_tiles_files:
                shutil.copy(rasttile, localenv)
                tmptile = os.path.join(localenv, os.path.basename(rasttile))
                rasttiletmp = os.path.join(
                    localenv,
                    os.path.splitext(os.path.basename(rasttile))[0] +
                    '_nd.tif')
                bmappli = oa.CreateBandMathApplication({
                    "il":
                    tmptile,
                    "out":
                    rasttiletmp,
                    "exp":
                    'im1b1 < 223 ? im1b1 : 0'
                })
                bmappli.ExecuteAndWriteOutput()
                tomerge.append(rasttiletmp)

            if len(tomerge) != 0:
                srx, sry = fut.getRasterResolution(tomerge[0])
                fut.assembleTile_Merge(tomerge, (srx, sry), outraster, "Byte")

                logger.info('Raster mosaic "%s" done for zone %s', outraster,
                            str(valueclip))

                for rasttile in tomerge:
                    os.remove(rasttile)

                if os.path.exists(out):
                    if os.path.isdir(out):
                        shutil.copy(outraster, out)
                else:
                    raise Exception(
                        'Output folder %s for mosaic storage does not exist' %
                        (out))
            else:
                outraster = None
        else:
            raise Exception("No tiles or crown tile rasters does not exist "
                            "for the area %s of the clip file %s" %
                            (valueclip, os.path.basename(clipfile)))
    else:
        raise Exception('Raster mosaic "%s" already exists' % (str(valueclip)))

    return outraster
