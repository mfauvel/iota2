#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" Compute Zonal statistics on different geometries \
(polygon or buffered point) """

import os
import sys
import argparse
import shutil
from collections import OrderedDict
from zipfile import ZipFile
import re
import logging
from math import log, e
import osgeo
import gdal
import pandas as pad
import geopandas as gpad
from skimage.measure import label
from skimage.measure import regionprops
import rasterio
import fiona
from rasterio.mask import mask
import numpy as np

from iota2.VectorTools import vector_functions as vf
from iota2.VectorTools import checkGeometryAreaThreshField as checkGeom
from iota2.VectorTools import BufferOgr as bfo
from iota2.VectorTools import MergeFiles as mf
from iota2.VectorTools import split_vector as sv
from iota2.Common import FileUtils as fut
from iota2.Common import Utils
from iota2.simplification import nomenclature

LOGGER = logging.getLogger(__name__)


def get_maj(serie):
    """Identify majority class, rate difference of 2 first
    majority class and compute majority order:
    ----------
    serie
        Pandas Serie with class (as index) and rate value

    Return
    ------
    maj
        integer value of the majority class
    diffmaj
        float value of rate difference of 2 first majority class
    sortsdf
        Pandas Serie with class, rate and majority order
    """

    serie = serie.fillna(0)
    sortseries = serie.sort_values(ascending=False)

    maj = sortseries.index.values[0].split("_")[1]
    if len(serie) > 1:
        diffmaj = sortseries.iloc[0] - sortseries.iloc[1]
    else:
        diffmaj = 1

    sortseries = sortseries.to_frame()
    ordersmaj = pad.Series(np.arange(1,
                                     len(sortseries) + 1, 1),
                           index=sortseries.index)

    sortsdf = sortseries.merge(ordersmaj.to_frame(),
                               left_index=True,
                               right_index=True)

    sortsdf.columns = ['rate', 'order']

    return maj, diffmaj, sortsdf


def get_shannon(serie):
    """Compute Shannon index on Pandas Serie with class
    (as index) and rate value:
    ----------
    serie
        Pandas Serie with class (as index) and rate value

    Return
    ------
    diffmaj
        float value of Shannon index
    """

    lenserie = len(serie)
    serie = serie.dropna()

    logvalsum = 0
    if len(serie.index) != 0:
        for val in serie.values:
            logvalsum += np.log(float(val)) * val

        shan = logvalsum * -1 / np.log(lenserie)

    else:
        shan = 0

    return shan


def get_entropy(labels, base=None):
    """ Computes entropy of label distribution. """

    n_labels = len(labels)

    if n_labels <= 1:
        return 0

    _, counts = np.unique(labels, return_counts=True)
    probs = counts / n_labels
    n_classes = np.count_nonzero(probs)

    if n_classes <= 1:
        return 0

    ent = 0.

    # Compute entropy
    base = e if base is None else base
    for i in probs:
        ent -= i * log(i, base)

    return ent


def count_pixel_by_class(databand, fid=0, band=0, nodata=0):
    """Compute rates of unique values of a categorical raster
       and store them in a Pandas DataFrame:


    ----------
    databand : gdal raster file or osgeo.gdal.Dataset
        categorical raster

    fid : int
        FID value of feature of zonal vector (DataFrame storage)

    band : int
        band number of databand input parameter

    nodata : int
        nodata value of the raster / band

    Return
    ------
    classStats
        Pandas DataFrame

    classmaj
        integer value of the majority class

    posclassmaj
        ndarray of position of majority class
    """

    if isinstance(databand, np.ndarray):
        if databand.size != 0:
            data = databand
        else:
            raise Exception('Empty data of wrapped raster')

    else:
        if databand:
            if isinstance(databand, str):
                if os.path.exists(databand):
                    rastertmp = gdal.Open(databand, 0)
                else:
                    raise Exception('Raster file %s not exist' % (databand))

            elif isinstance(databand, osgeo.gdal.Dataset):
                rastertmp = databand

            else:
                raise Exception('Type of raster dataset not handled')

            banddata = rastertmp.GetRasterBand(band)
            data = banddata.ReadAsArray()

        else:
            raise Exception('Empty data of wrapped raster')

    img = label(data)
    counts = []

    col_names = ['value', 'count']

    if len(np.unique(img)) != 1 or np.unique(img)[0] != 0:
        try:
            dataclean = data[data != nodata]
            npcounts = np.array(np.unique(dataclean, return_counts=True)).T
            counts = npcounts.tolist()
        except RuntimeError:
            for reg in regionprops(img, data):
                counts.append([[
                    x for x in np.unique(reg.intensity_image) if x != nodata
                ][0], reg.area])

        if counts[0]:
            # test si counts a des valeurs !
            listlab = pad.DataFrame(data=counts, columns=col_names)
            # pourcentage
            listlab['rate'] = listlab['count'] / listlab['count'].sum()

            # classmaj
            classmaj = listlab[listlab['rate'] == max(
                listlab['rate'])]['value']
            classmaj = classmaj.iloc[0]

            posclassmaj = np.where(data == int(classmaj))

            # Transposition pour jointure directe
            listlabt = listlab.T
            classstats = pad.DataFrame(
                data=[listlabt.loc['rate'].values],
                index=[fid],
                columns=[str(int(x)) for x in listlabt.loc['value']])

    else:
        classstats = pad.DataFrame(index=[fid], columns=[])
        classmaj = 0
        posclassmaj = 0

    listlab = listlabt = data = None

    return classstats, classmaj, posclassmaj


def raster_stats(band, posclassmaj=None, postoread=None, nodata=0):
    """Compute descriptive statistics of a numpy array or a gdal raster:

    Parameters
    ----------
    band : gdal raster file or osgeo.gdal.Dataset
        raster on which compute statistics

    nbband : int
        band number of band input parameter

    posclassmaj : ndarray
        numpy array of position of majority class

    postoread : tuple
        col / row coordinates on which extract pixel value

    nodata : int
        nodata value of the raster / band

    Return
    ------
    mean, std, max, min
        float

    pixel value
        float
    """

    if isinstance(band, np.ndarray):
        if band.size != 0:
            data = band
        else:
            raise Exception('Empty data of wrapped raster')

    else:
        if band:
            if isinstance(band, str):
                if os.path.exists(band):
                    rastertmp = gdal.Open(band, 0)
                else:
                    raise Exception('Raster file %s not exist' % (band))

            elif isinstance(band, osgeo.gdal.Dataset):
                rastertmp = band

            else:
                raise Exception('Type of raster dataset not handled')

            banddata = rastertmp.GetRasterBand(band)
            data = banddata.ReadAsArray()

        else:
            raise Exception('Empty data of wrapped raster')

    if not postoread:
        img = label(data)
        if len(np.unique(img)) != 1 or np.unique(img)[0] != 0:

            data = data[posclassmaj]
            data = list(data[data != nodata])

            if data:
                mean = round(np.mean(data), 2)
                std = round(np.std(data), 2)
                maxval = round(np.max(data), 2)
                minval = round(np.min(data), 2)
                stats = (mean, std, maxval, minval)
            else:
                stats = (None, None, None, None)
        else:
            stats = (0, 0, 0, 0)

    else:
        stats = np.float(data[postoread[1], postoread[0]])

    return stats


def define_df(geoframe,
              idvals,
              paramstats,
              classes="",
              pixval=False,
              nbrate=0):
    """Define DataFrame (columns and index values)
    based on expected statistics and zonal vector

    Parameters
    ----------
    geoframe : geopandas.GeoDataFrame
        dataframe of input vector file

    idvals : list
        list of FID to analyse (DataFrame storage)

    paramstats : dict
        list of statistics to compute (e.g. {1:'stats', 2:'rate'})

    classes : nomenclature file
        nomenclature

    Return
    ------
    geopandas.GeoDataFrame

    """

    cols = []

    for param in paramstats:
        if paramstats[param] == "rate":
            if classes != "" and classes is not None:
                nomenc = nomenclature.Iota2Nomenclature(classes, 'cfg')
                desclasses = nomenc.HierarchicalNomenclature.get_level_values(
                    int(nomenc.getLevelNumber() - 1))
                for nrate in range(nbrate):
                    [
                        cols.append(str("b%s_%s" % (nrate + 1, x)))
                        for x, y, w, z in desclasses
                    ]

        elif paramstats[param] == "stats":
            [
                cols.append(x) for x in [
                    "meanb%s" % (param),
                    "stdb%s" % (param),
                    "maxb%s" % (param),
                    "minb%s" % (param)
                ]
            ]

        elif paramstats[param] == "statsmaj":
            [
                cols.append(x) for x in [
                    "meanmajb%s" % (param),
                    "stdmajb%s" % (param),
                    "maxmajb%s" % (param),
                    "minmajb%s" % (param)
                ]
            ]
        elif "stats_" in paramstats[param]:
            cl_ocs = paramstats[param].split('_')[1]
            [
                cols.append(x) for x in [
                    "meanb%sc%s" % (param, cl_ocs),
                    "stdb%sc%s" % (param, cl_ocs),
                    "maxb%sc%s" % (param, cl_ocs),
                    "minb%sc%s" % (param, cl_ocs)
                ]
            ]
        elif "val" in paramstats[param]:
            [cols.append("valb%s" % (param))]
        else:
            raise Exception("The method %s is not implemented") % (
                paramstats[param])

    statsgpad = gpad.GeoDataFrame(np.nan, index=idvals, columns=cols)
    geoframe = gpad.GeoDataFrame(pad.concat([geoframe, statsgpad], axis=1),
                                 geometry=geoframe['geometry'],
                                 crs=geoframe.crs)

    if pixval:
        for pix in range(list(paramstats.values()).count('val')):
            geoframe["pixval_b%s" % (pix + 1)] = np.nan

    if 'rate' in paramstats.values():
        for rate in range(list(paramstats.values()).count('rate')):
            geoframe["maj_b%s" % (rate + 1)] = np.nan
            geoframe["dmaj_b%s" % (rate + 1)] = np.nan
            geoframe["shan_b%s" % (rate + 1)] = np.nan
            geoframe["var_b%s" % (rate + 1)] = np.nan
            geoframe["svar_b%s" % (rate + 1)] = np.nan
            geoframe["pixval_b%s" % (rate + 1)] = np.nan
            geoframe["pmajo_b%s" % (rate + 1)] = np.nan

    return geoframe


def check_method_stats(rasters, paramstats, nbbands):
    """Store list of requested statistics in dict and
       check validity of in put rasters

    Parameters
    ----------

    rasters : list
        list of rasters to analyse

    paramstats : list
        list of statistics to compute
        (e.g. [[1,'stats'], [2, 'rate']] or ['val'])

    nbbands : int
        number of input rasters or bands of input raster

    Return
    ----------
    paramstats : dict
        list of statistics to compute (e.g. {1:'stats', 2:'rate'})

    """

    # Format requested statistics
    if isinstance(paramstats, list):
        # List of methods (bash)
        if ':' in paramstats[0]:
            paramstats = dict([(x.split(':')[0], x.split(':')[1])
                               for x in paramstats])

        # Unique method without band / raster number
        elif len(paramstats) == 1:
            # Build statistics method dictionary
            tmpdict = {}
            for idx in range(nbbands):
                tmpdict[idx + 1] = str(paramstats[0])
            paramstats = tmpdict

    # Check statistics methods validity
    for keys in paramstats:
        if 'stats_' in paramstats[keys]:
            paramstats[keys] = 'stats'

        if paramstats[keys] not in ('stats', 'statsmaj', 'rate', 'val'):
            raise Exception('The method %s is not implemented' %
                            (paramstats[0]))

    # check if byte raster when rate is requested
    nbrate = 0
    for keys in paramstats:
        if 'rate' in paramstats[keys]:
            typeraster = fut.get_image_pixel_type(rasters[int(keys) - 1])
            if typeraster not in [
                    'Byte', 'Int16', 'UInt16', 'UInt32', 'Int32'
            ]:
                raise Exception(
                    "Rate method is requested for raster '%s' "
                    "but not a raster with integer pixel "
                    "('Byte', 'Int16', 'UInt16', 'UInt32', 'Int32') "
                    "values provided (%s)" % (keys, typeraster))
            nbrate += 1

    # requested stats and band number ?
    maxband = max([int(x) for x in list(paramstats.keys())])
    if len(rasters) != 1:
        if nbbands < maxband:
            raise Exception("Band ids in requested stats and "
                            "number of input rasters "
                            "or bands number of input raster "
                            "do not correspond")

    # same extent and resolution of input rasters ?
    listres = []
    listextent = []
    if len(rasters) != 1:
        for raster in rasters:
            listres.append(abs(fut.getRasterResolution(raster)[0]))
            listextent.append(fut.getRasterExtent(raster))

    if listextent[1:] != listextent[:-1]:
        raise Exception("Input rasters must have same extent")

    if listres[1:] != listres[:-1]:
        raise Exception("Input rasters must have same spatial resolution")

    return paramstats, nbrate


def set_df_schema(vector, paramstats, vectorgeomtype, bufferdist=""):
    """Store list of raster or multi-band raster in a ndarray

    Parameters
    ----------

    paramstats : dict
        list of statistics to compute (e.g. {1:'stats', 2:'rate'})

    vectorgeomtype : int
        Type of geometry
        (http://portal.opengeospatial.org/files/?artifact_id=25355)

    bufferdist : int
        buffer size around Point vetor geometry


    Return
    ----------
    schema : dict / Fiona schema
        schema giving geometry type

    """
    with fiona.open(vector) as fvect:
        inproperties = fvect.schema['properties']

    # Value extraction
    if not bufferdist and vectorgeomtype in (1, 4, 1001, 1004):
        if 'val' in list(paramstats.values()):
            if vectorgeomtype == 1:
                schema = {'geometry': 'Point', 'properties': inproperties}
            elif vectorgeomtype == 4:
                schema = {'geometry': 'MultiPoint', 'properties': inproperties}
        else:
            raise Exception("Only pixel value extraction available "
                            "when Point geometry "
                            "without buffer distance is provided")

    # Stats extraction
    else:
        # Point geometry
        if vectorgeomtype in (1, 4, 1001, 1004):
            if vectorgeomtype == 1:
                schema = {'geometry': 'Point', 'properties': inproperties}
            elif vectorgeomtype == 4:
                schema = {'geometry': 'MultiPoint', 'properties': inproperties}

        # Polygon geometry
        elif vectorgeomtype in (3, 6, 1003, 1006):
            if vectorgeomtype == 3:
                schema = {'geometry': 'Polygon', 'properties': inproperties}
            elif vectorgeomtype == 6:
                schema = {
                    'geometry': 'MultiPolygon',
                    'properties': inproperties
                }
        else:
            raise Exception("Geometry type of vector file not handled")

    return schema


def rasters_in_array(rasters):
    """Store list of raster or multi-band raster in a ndarray

    Parameters
    ----------

    rasters : list
        list of rasters to analyse

    Return
    ----------
    ndarray

    """

    # Load raster files or geodataset in 3D numpy array
    for idx, raster in enumerate(rasters):
        data = fut.readRaster(raster, True)[0]
        if data.ndim == 2:
            data = data[None, :, :]

        if idx == 0:
            outdata = data
        else:
            outdata = np.concatenate((outdata, data))

    return outdata


def clip_raster_in_array(rasters,
                         vector,
                         fid,
                         gdalpath="",
                         gdalcachemax="9000",
                         systemcall=None,
                         path=""):
    """Clip raster and store in ndarrays

    Parameters
    ----------

    rasters : list
        list of rasters to analyse

    paramstats : dict
        list of statistics to compute (e.g. {1:'stats', 2:'rate'})

    vector : string
        vector file for cutline opetation

    fid : integer
        FID value to clip raster (cwhere parameter of gdalwarp)

    gdalpath : string
        gdal binaries path

    gdalcachemax : string
        gdal cache for wrapping operation (in Mb)

    systemcall : boolean
        if True, use os system call to execute gdalwarp
        (usefull to control gdal binaries version - gdalpath parameter)

    path : string
        temporary path to store temporary date if systemcall is True

    Return
    ----------
    boolean
        if True, wrap operation well terminated

    ndarray ndarrays

    """

    bands = []
    todel = []
    success = True

    # Get rasters resolution
    res = abs(fut.getRasterResolution(rasters[0])[0])

    # Get vector name
    vectorname = os.path.splitext(os.path.basename(vector))[0]

    for idx, raster in enumerate(rasters):
        '''
        # extraction avec rasterio uniquement (tester temps d'exécution)
        with fiona.open(vector) as src:
                    filtered = filter(lambda f: f['id'] == str(fid), src)
                    feature = next(filtered)
                    geometry = feature["geometry"]

                with rasterio.open(raster) as rast:
                    if idx == 0:
                        npbands, _ = mask(rast, [geometry], crop=True)
                    else:
                        band, _ = mask(rast, [geometry], crop=True)
                        npbands = np.concatenate((npbands, band))
        '''
        errormsg = ""
        try:
            if systemcall:
                tmpfile = os.path.join(
                    path, 'rast_%s_%s_%s' % (vectorname, str(fid), idx))
                cmd = '%sgdalwarp -tr %s %s -tap -q -overwrite -cutline %s '\
                      '-crop_to_cutline --config GDAL_CACHEMAX %s -wm %s '\
                      '-wo "NUM_THREADS=ALL_CPUS" '\
                      '-wo "CUTLINE_ALL_TOUCHED=YES" '\
                      '-cwhere "FID=%s" %s %s '\
                      '-ot Float32' % (os.path.join(gdalpath, ''),
                                       res,
                                       res,
                                       vector,
                                       gdalcachemax,
                                       gdalcachemax,
                                       fid,
                                       raster,
                                       tmpfile)
                Utils.run(cmd)
                todel.append(tmpfile)

            else:
                gdal.SetConfigOption("GDAL_CACHEMAX", gdalcachemax)
                tmpfile = gdal.Warp(
                    '',
                    raster,
                    xRes=res,
                    yRes=res,
                    targetAlignedPixels=True,
                    cutlineDSName=vector,
                    cropToCutline=True,
                    cutlineWhere="FID=%s" % (fid),
                    format='MEM',
                    warpMemoryLimit=gdalcachemax,
                    warpOptions=[["NUM_THREADS=ALL_CPUS"],
                                 ["CUTLINE_ALL_TOUCHED=YES"],
                                 ["GDALWARP_IGNORE_BAD_CUTLINE=YES"]])

            bands.append(tmpfile)
            todel = []

        except RuntimeError as err:

            errormsg = err
            success = False

        if not success:

            try:
                with fiona.open(vector) as src:
                    filtered = filter(lambda f: f['id'] == str(fid), src)
                    feature = next(filtered)
                    geometry = feature["geometry"]
                with rasterio.open(raster) as rast:
                    if idx == 0:
                        npbands, _ = mask(rast, [geometry], crop=True)
                    else:
                        band, _ = mask(rast, [geometry], crop=True)
                        npbands = np.concatenate((npbands, band))

                success = True

            except RuntimeError as err:
                errormsg = err
                success = False

    # store rasters in ndarray()
    if bands:
        npbands = rasters_in_array(bands)

    # Remove tmp rasters
    for filtodel in todel:
        os.remove(filtodel)

    return success, npbands, errormsg


def get_pos_class_maj(bands, methodstat, idxcatraster, nodata):
    """Extract statistics on numpy array and
       store them on a GeoPandas / Pandas dataframe

    Parameters
    ----------
    bands : ndarray
        raster bands or raster files store in numpy array

    methodstats : string
        statistics method ('statsmaj' or 'stats_*')

    idxcatraster : integer
        indice of the categorical raster to find class positions

    Return
    ----------
    ndarray or tuple of ndarrays

    """
    if methodstat == 'statsmaj':

        # Get band of categorical raster
        nbbandrate = int(idxcatraster - 1)
        bandrate = bands[nbbandrate]

        # Find majority class and return positions array
        _, _, posclass = count_pixel_by_class(bandrate, "", nbbandrate, nodata)

    elif 'stats_' in methodstat:

        # get class value to check
        reqclass = 'statsmaj'.split('_')[1]

        # get positions array of the class
        rastertmp = gdal.Open(bands[idxcatraster - 1], 0)
        data = rastertmp.ReadAsArray()
        posclass = np.where(data == int(reqclass))
        data = None

    return posclass


def compute_stats(bands,
                  paramstats,
                  dataframe,
                  idval,
                  nodata=0,
                  getcentroid=False,
                  higher_stats=False):
    """Extract statistics on numpy array and
       store them on a GeoPandas / Pandas dataframe

    Parameters
    ----------
    bands : ndarray
        raster bands or raster files store in numpy array

    paramstats : dict
        list of statistics to compute (e.g. {1:'stats', 2:'rate'})

    dataframe : Pandas Dataframe
        Pandas Dataframe

    idval : integer
        index value to store in dataframe

    nodata : float
        nodata value of input rasters

    multiraster : boolean
        True, if several input rasters, False if one input raster

    Return
    ----------
    GeoPandas or Pandas DataFrame

    """

    for param in paramstats:

        band = bands[int(param) - 1, :, :]
        nbband = int(param)
        classmaj = None

        # Statistics extraction
        if band.size != 0:
            methodstat = paramstats[param]

            # Categorical statistics
            if methodstat == 'rate':

                classstats, classmaj, posclassmaj = count_pixel_by_class(
                    band, idval, nbband, nodata)

                # manage classstats colum names (several rate stats)
                prefix = "b%s_" % (str(nbband))
                suffix = "_b%s" % (str(nbband))

                classstats.rename(columns=lambda x: prefix + x[:],
                                  inplace=True)

                if higher_stats:
                    # spatial variance
                    if band[band != nodata].size != 0:
                        classstats["svar%s" % (suffix)] = get_entropy(
                            band[band != nodata])
                    else:
                        classstats["svar%s" % (suffix)] = 0

                # Get centroid value
                if getcentroid:
                    classstats["pixval%s" % (suffix)] = band[int(
                        band.shape[0] / 2)][int(band.shape[1] / 2)]

                dataframe.update(classstats)

                # Add columns for values not in nomenclature file
                if list(classstats.columns) != list(dataframe.columns):
                    newcols = list(
                        set(list(classstats.columns)).difference(
                            set(list(dataframe.columns))))
                    dataframe = pad.concat([dataframe, classstats[newcols]],
                                           axis=1)

            elif methodstat == 'stats':

                cols = [
                    "meanb%s" % (int(param)),
                    "stdb%s" % (int(param)),
                    "maxb%s" % (int(param)),
                    "minb%s" % (int(param))
                ]

                dataframe.update(
                    pad.DataFrame(data=[raster_stats(band, nodata=nodata)],
                                  index=[idval],
                                  columns=cols))

            ### Descriptive statistics for majority class ###
            elif methodstat == 'statsmaj':
                if not classmaj:
                    if "rate" in list(paramstats.values()):
                        idxbdclasses = [
                            x for x in paramstats if paramstats[x] == "rate"
                        ]
                        idxbdclasses = int(nbband - 1)
                        posclassmaj = get_pos_class_maj(
                            bands, methodstat, idxbdclasses, nodata)
                    else:
                        raise Exception(
                            "No classification raster provided "
                            "to check position of majority class. "
                            "classification raster must be provided "
                            "just before statsmaj raster "
                            "(1:rate 2:statsmaj 3:rate 4:statsmaj)")

                cols = [
                    "meanmajb%s" % (int(param)),
                    "stdmajb%s" % (int(param)),
                    "maxmajb%s" % (int(param)),
                    "minmajb%s" % (int(param))
                ]

                dataframe.update(
                    pad.DataFrame(
                        data=[raster_stats(band, posclassmaj, nodata=nodata)],
                        index=[idval],
                        columns=cols))

            # Descriptive statistics for one class
            elif "stats_" in methodstat:
                if "rate" in list(paramstats.values()):
                    idxbdclasses = [
                        x for x in paramstats if paramstats[x] == "rate"
                    ][0]
                    posclass = get_pos_class_maj(bands, methodstat,
                                                 idxbdclasses, nodata)
                else:
                    raise Exception("No classification raster provided "
                                    "to check position of majority class")

                cols = [
                    "meanb%sc%s" % (int(param), reqclass),
                    "stdb%sc%s" % (int(param), reqclass),
                    "maxb%sc%s" % (int(param), reqclass),
                    "minb%sc%s" % (int(param), reqclass)
                ]

                dataframe.update(
                    pad.DataFrame(
                        data=[raster_stats(band, posclass, nodata=nodata)],
                        index=[idval],
                        columns=cols))
            band = None

    return dataframe


def extract_pixel_value(bands,
                        paramstats,
                        posx,
                        posy,
                        dataframe=None,
                        idval=0):
    """Extract pixel value and store it on a Pandas dataframe

    Parameters
    ----------
    rasters : list
        list of rasters to analyse

    bands : ndarray
        raster bands or raster files store in numpy array

    paramstats : dict
        list of statistics to compute (e.g. {1:'stats', 2:'rate'})

    posx : float
        Point x column coordinates

    posy : float
        Point y row coordinates

    dataframe : Pandas Dataframe
        Pandas Dataframe

    idval : integer
        index value to store in dataframe

    Return
    ----------
    GeoPandas or Pandas DataFrame

    """

    for param in paramstats:

        band = bands[int(param) - 1, :, :]

        if band.size != 0:
            methodstat = paramstats[param]
            if "val" in methodstat:
                cols = "valb%s" % (param)
                dataframe.update(
                    pad.DataFrame(
                        data=[raster_stats(band, None, (posx, posy))],
                        index=[idval],
                        columns=[cols]))

            band = None

    return dataframe


def format_df(geodataframe,
              schema,
              fidtodel,
              categorical=False,
              classes="",
              floatdec=2,
              length=5):
    """Format columns name and format of a GeoPandas DataFrame

    Parameters
    ----------
    geodataframe : GeoPandas DataFrame
        GeoPandas DataFrame

    schema : dict / Fiona schema
        schema giving colums name and format

    categorical : boolean
        if True, classes is required

    classes : Iota2Nomenclature
        Nomencalture description (Iota2Nomenclature)

    floatdec : integer
        length of decimal part of columns values

    intsize : integer
        length of integer part of columns values

    Return
    ----------
    GeoPandas DataFrame

    GeoPandas schema

    """

    # change column names with nomenclature file
    if categorical:
        # get multi-level nomenclature
        nomenc = nomenclature.Iota2Nomenclature(classes, 'cfg')
        desclasses = nomenc.HierarchicalNomenclature.get_level_values(
            int(nomenc.getLevelNumber() - 1))
        cols = [(str(x), str(z)) for x, y, w, z in desclasses]

        # rename columns with alias of nomenclature file
        for col in cols:
            geodataframe.rename(columns={col[0]: col[1]}, inplace=True)

    # change columns type
    props = schema['properties']
    cols = list(geodataframe.columns)
    formats = []
    for col in cols:
        if col != 'geometry':
            if col in props.keys():
                formats.append((col, props[col]))
            elif re.search("^maj_b.", col) or re.search("^pixval_b.", col):
                formats.append((col, 'int:4'))
            else:
                formats.append((col, 'float:%s.%s' % (length, floatdec)))

    schema['properties'] = OrderedDict(formats)

    # drop unused index
    geodataframe.drop(fidtodel, inplace=True)

    return geodataframe, schema


def export_df(geodataframe, output, schema):
    """Export a GeoPandas DataFrame as a
    vector file (shapefile, sqlite and geojson)

    Parameters
    ----------
    geodataframe : GeoPandas DataFrame
        GeoPandas DataFrame

    output : string
        output vector file

    schema : dict / Fiona schema
        schema giving colums name and format

    """

    geodataframe = geodataframe.fillna(0)

    convert = False
    outformat = os.path.splitext(output)[1]
    if outformat == ".shp":
        driver = "ESRI Shapefile"
    elif outformat == ".geojson":
        driver = "GeoJSON"
    elif outformat == ".sqlite":
        driver = "ESRI Shapefile"
        convert = True
    else:
        raise Exception("The output format '%s' is not handled" %
                        (outformat[1:]))

    if not convert:
        geodataframe.to_file(output,
                             driver=driver,
                             schema=schema,
                             encoding='utf-8')
    else:
        outputinter = os.path.splitext(output)[0] + '.shp'
        geodataframe.to_file(outputinter,
                             driver=driver,
                             schema=schema,
                             encoding='utf-8')
        output = os.path.splitext(output)[0] + '.sqlite'
        Utils.run('ogr2ogr -f SQLite %s %s' % (output, outputinter))


def zonal_stats(path,
                rasters,
                params,
                output,
                paramstats,
                classes="",
                bufferdist=None,
                nodata=0,
                gdalpath="",
                systemcall=None,
                gdalcachemax="9000",
                higher_stats=False,
                working_dir=None,
                logger=LOGGER):
    """Compute zonal statistitics (descriptive and categorical)
       on multi-band raster or multi-rasters
       based on Point (buffered or not) or Polygon zonal vector

    Parameters
    ----------
    path : string
        working directory

    rasters : list
        list of rasters to analyse

    params : list
        list of fid list and vector file

    output : vector file (sqlite, shapefile and geojson)
        vector file to store statistitics

    paramstats : list
        list of statistics to compute (e.g. {1:'stats', 2:'rate'})

            - paramstats = {1:"rate", 2:"statsmaj",
                            3:"statsmaj", 4:"stats",
                            2:stats_cl}
            - stats : mean_b, std_b, max_b, min_b
            - statsmaj : meanmaj, stdmaj, maxmaj, minmaj of majority class
            - rate : rate of each pixel value (classe names)
            - stats_cl : mean_cl, std_cl,
                         max_cl, min_cl of one class
            - val : value of corresponding pixel
                    (only for Point geometry and without other stats)

    classes : nomenclature file
        nomenclature

    bufferdist : int
        in case of point zonal vector : buffer size

    gdalpath : string
        path of gdal binaries (for system execution)

    systemcall : boolean
        if True, wrapped raster are stored in working dir

    gdalcachemax : string
        gdal cache for wrapping operation (in Mb)

    """

    if working_dir:
        path = working_dir

    logger.info("Begin to compute zonal statistics for vector file %s", output)

    if systemcall and not gdalpath:
        logger.info(
            "Please provide gdal binaries path when systemcall is set to true")
        raise IOError(
            "Please provide gdal binaries path when systemcall is set to true")

    if not os.path.exists(path):
        raise IOError(
            "Working directory '%s' does not exist, please create it." %
            (path))

    for idraster, rast in enumerate(rasters):
        srcrast = gdal.Open(rast)
        if srcrast.GetProjection() == "":
            raise Exception('raster %s has no projection defined' %
                            (idraster + 1))
        srcrast = None

    # Get bands or raster number
    if len(rasters) != 1:
        nbbands = len(rasters)
    else:
        nbbands = fut.getRasterNbands(rasters[0])

    resraster, _ = fut.getRasterResolution(rasters[0])

    # Prepare and check validity of statistics methods and input raster
    paramstats, nbrate = check_method_stats(rasters, paramstats, nbbands)

    # Check buffer validity
    if bufferdist:
        if float(bufferdist) <= 2 * float(resraster):
            raise ValueError("Buffer distance '%s' m is too short "
                             "(> 2 x input raster resolution) " % (bufferdist))

    # Get vector file and FID list
    if len(params) > 1:
        vector, idvals = params
    else:
        vector = params[0][0]
        idvals = params[0][1]

    # if no vector subsetting (all features)
    fullfid = vf.getFidList(vector)
    if not idvals:
        idvals = fullfid
        novals = []
    else:
        novals = [x for x in fullfid if x not in idvals]

    # vector open and iterate features and/or buffer geom
    vectorname = os.path.splitext(os.path.basename(vector))[0]
    vectorgeomtype = vf.getGeomType(vector)
    vectorbuff = None

    # Buffer Point vector file
    if 'val' in list(paramstats.values()):
        bufferdist = resraster * 2

    if vectorgeomtype in (1, 4, 1001, 1004):
        vectorbuff = os.path.join(path, vectorname + "buff.shp")
        _ = bfo.bufferPoly(vector, vectorbuff, bufferDist=bufferdist)
        vectgpad = gpad.read_file(vector)
        vector = vectorbuff
    else:
        vectgpad = gpad.read_file(vector)

    # Prepare schema of output geopandas dataframe (geometry type and columns)
    schema = set_df_schema(vector, paramstats, vectorgeomtype, bufferdist)

    # Prepare statistics columns of output geopandas dataframe
    if vectorgeomtype in (1, 4, 1001, 1004) and 'rate' in list(
            paramstats.values()):
        stats = define_df(vectgpad,
                          idvals,
                          paramstats,
                          classes,
                          True,
                          nbrate=nbrate)
    else:
        stats = define_df(vectgpad, idvals, paramstats, classes, nbrate=nbrate)

    if "fid" in [x.lower() for x in vf.getFields(vector)]:
        raise ValueError("FID field not allowed. "
                         "This field name is reserved by gdal binary.")

    for idval in idvals:
        # creation of wrapped rasters

        success, bands, err = clip_raster_in_array(rasters, vector, idval,
                                                   gdalpath, gdalcachemax,
                                                   systemcall, path)

        if success:
            if 'val' in list(paramstats.values()):
                posx = posy = int(float(bufferdist) / float(resraster))
                stats = extract_pixel_value(bands, paramstats, posx, posy,
                                            stats, idval)
            else:
                if vectorgeomtype in (1, 4, 1001, 1004):
                    stats = compute_stats(bands, paramstats, stats, idval,
                                          nodata, True)
                else:
                    stats = compute_stats(bands, paramstats, stats, idval,
                                          nodata)
        else:
            print("gdalwarp problem for feature %s (%s) : "
                  "statistic computed with rasterio" % (idval, err))

    # compute Shannon
    if 'rate' in list(paramstats.values()):
        listofrates = [
            int(key) for (key, value) in paramstats.items() if value == 'rate'
        ]
        for rate in listofrates:
            statstmp = stats.filter(regex='^b%s_.' % (rate), axis=1)
            stats['maj_b%s' % (rate)] = statstmp.apply(lambda x: get_maj(x)[0],
                                                       axis=1)
            if higher_stats:
                stats['shan_b%s' % (rate)] = statstmp.apply(
                    lambda x: get_shannon(x), axis=1)
                stats['var_b%s' % (rate)] = statstmp.apply(
                    lambda x: np.var(x.dropna()), axis=1)
                stats['dmaj_b%s' % (rate)] = statstmp.apply(
                    lambda x: get_maj(x)[1], axis=1)
                stats['pmajo_b%s' % (rate)] = statstmp.apply(
                    lambda x: get_maj(x)[2].iloc[0]["rate"], axis=1)

                for col in statstmp.columns:
                    stats["omaj_" + col.split("_")[1]] = statstmp.apply(
                        lambda x: get_maj(x)[2].loc[col]["order"], axis=1)

    # Prepare columns name and format of output dataframe
    if "rate" in list(paramstats.values()) and classes != "":
        stats, schema = format_df(stats, schema, novals, True, classes)
    else:
        stats = stats.reindex(sorted(stats.columns), axis=1)
        stats, schema = format_df(stats, schema, novals)

    # exportation
    export_df(stats, output, schema)

    logger.info("Zonal statistics stored in %s", output)
    print("Zonal statistics stored in %s" % (output))


def prod_formatting(invector,
                    classes,
                    outvector="",
                    prod=None,
                    higher_stats=False):
    """Convert zonal statistics output vector file to
    a vector file with specific aliases

    Parameters
    ----------
    invector : OGR vector file
        input OGR vector file (computed by zonal_stats function)

    classes : config file
        config file describing class aliases

    outvector : OGR vector file
        OGR vector file

    prod : string
        "oso" or "carhab" production

    higher_stats : boolean
        if true, inpute vector file as hiher stats (majority order and so on)
    """

    nomenc = nomenclature.Iota2Nomenclature(classes, 'cfg')
    desclasses = nomenc.HierarchicalNomenclature.get_level_values(
        int(nomenc.getLevelNumber() - 1))
    sortalias = [[x, str(z)] for x, y, w, z in desclasses]

    listfields = vf.getFields(invector)

    exp = ""
    for code, alias in sortalias:
        if "b1_%s" % (code) in listfields:
            exp += "CAST(b1_%s AS NUMERIC(6,2)) AS %s, " % (code, alias)
        if higher_stats:
            if "omaj_%s" % (code) in listfields:
                exp += "CAST(%s AS INTEGER(2)) AS %s, " % ("omaj_%s" %
                                                           (code), "ormaj_%s" %
                                                           (code))

    layerin = os.path.splitext(os.path.basename(invector))[0]
    if outvector == "":
        outvector = os.path.splitext(invector)[0] + '_tmp.shp'
        layerout = layerin + "_tmp"
    else:
        layerout = os.path.splitext(os.path.basename(outvector))[0]

    gdal.SetConfigOption('CPL_LOG', '/dev/null')
    if prod == "oso":
        command = "ogr2ogr -lco ENCODING=UTF-8 -overwrite -q "\
            "-f 'ESRI Shapefile' -nln %s -sql "\
            "'SELECT CAST(cat AS INTEGER(4)) AS Classe, "\
            "CAST(meanmajb3 AS INTEGER(4)) AS Validmean, "\
            "CAST(stdmajb3 AS NUMERIC(6,2)) AS Validstd, "\
            "CAST(meanmajb2 AS INTEGER(4)) AS Confidence, %s"\
            "CAST(area AS NUMERIC(10,2)) AS Aire "\
            "FROM %s' "\
            "%s %s" % (layerout, exp, layerin, outvector, invector)

    elif prod == "carhab":
        command = "ogr2ogr -lco ENCODING=UTF-8 -overwrite -q "\
            "-f 'ESRI Shapefile' -nln %s -sql "\
            "'SELECT %s CAST(maj_b1 AS INTEGER(4)) AS Major, "\
            "CAST(pmajo_b1 AS NUMERIC(6,2)) AS Pourc_majo, "\
            "CAST(meanmajb2 AS NUMERIC(6,2)) AS Conf_maj_m, "\
            "CAST(stdmajb2 AS NUMERIC(6,2)) AS Conf_maj_e, "\
            "CAST(meanb3 AS NUMERIC(6,2)) AS Conf_moy "\
            "FROM %s' "\
            "%s %s" % (layerout, exp, layerin, outvector, invector)

    Utils.run(command)
    if invector != outvector:
        for ext in ['.shp', '.dbf', '.shx', '.prj', '.cpg']:
            try:
                shutil.copy(
                    os.path.splitext(outvector)[0] + ext,
                    os.path.splitext(invector)[0] + ext)
                os.remove(os.path.splitext(outvector)[0] + ext)
            except IOError:
                print("not possible to create output file %s (copy problem)",
                      os.path.splitext(invector))


def compute_zonal_stats(path,
                        inr,
                        shape,
                        params,
                        output,
                        classes="",
                        bufferdist="",
                        nodata=0,
                        gdalpath="",
                        chunk=1,
                        byarea=False,
                        cache="1000",
                        systemcall=True,
                        prod=None,
                        higher_stats=False,
                        logger=LOGGER):
    """
    Invoke zonal_stats function
    """

    if os.path.exists(output):
        raise IOError("Output file '%s' already exists please delete it" %
                      (output))

    geomtype = vf.getGeomType(shape)
    if geomtype != -2147483647:
        if geomtype not in (1, 4, 1001, 1004):
            if os.path.splitext(shape)[1] == ".shp":
                tmp = os.path.join(path, "tmp.shp")
                checkGeom.checkGeometryAreaThreshField(shape, 1, 0, tmp)

                for ext in ['.shp', '.dbf', '.shx', '.prj', '.cpg']:
                    try:
                        shutil.copy(
                            os.path.splitext(tmp)[0] + ext,
                            os.path.splitext(shape)[0] + ext)
                        os.remove(os.path.splitext(tmp)[0] + ext)
                    except IOError:
                        logger.info("Problem to copy / remove file")

            else:
                raise Exception("Only shapefile allowed for input vector file")
        else:
            for param in params:
                if 'val' in param and bufferdist:
                    raise ValueError(
                        "'val' method not allowed with buffer parameter, "
                        "choose another method ('stats', 'statsmaj', 'rate')")

    else:
        raise Exception("Geometry of this vector file not handled")
    chunks = sv.split_vector_in_features(shape, path, chunk, byarea)
    for block in chunks:
        zonal_stats(path, inr, block, output, params, classes, bufferdist,
                    nodata, gdalpath, systemcall, cache, higher_stats)

    if prod:
        if prod in ("carhab", "oso"):
            prod_formatting(output,
                            classes,
                            prod=prod,
                            higher_stats=higher_stats)
        else:
            print("Only OSO (\"oso\") and CarHAB (\"carhab\")"
                  "formatting available")


def merge_sub_vector(listofchkofzones,
                     outpath,
                     classes="",
                     outbase="departement_",
                     outzip=True,
                     prod=None,
                     logger=LOGGER):
    """
    Merge vector files computed with zonal stats function ,
    format its field and compress it.
    """

    zoneval = listofchkofzones[0].split(
        '_')[len(listofchkofzones[0].split('_')) -
             1:len(listofchkofzones[0].split('_'))]
    outfile = os.path.join(outpath, outbase + zoneval[0] + '.shp')

    logger.info("Production of vector file %s", outfile)

    mf.mergeVectors_layer(listofchkofzones[1], outfile)

    if os.path.exists(outfile):
        for subzone in listofchkofzones[1]:
            for ext in ['.shp', '.dbf', '.shx', '.prj', '.cpg']:
                try:
                    os.remove(os.path.splitext(subzone)[0] + ext)
                except IOError:
                    print("Problem on remove chunked files")
        if prod:
            prod_formatting(outfile, classes, outfile, prod=prod)

        if outzip:
            outzip = os.path.splitext(outfile)[0] + '.zip'
            compress_shape(outfile, outzip)


def compress_shape(shapefile, outzip, logger=LOGGER):
    """
    Compress shapefile in Zip format
    """
    with ZipFile(outzip, 'w') as myzip:
        for ext in ['.shp', '.dbf', '.shx', '.prj', '.cpg']:
            try:
                myzip.write(
                    os.path.splitext(shapefile)[0] + ext,
                    os.path.basename(os.path.splitext(shapefile)[0] + ext))
            except IOError:
                logger.info("Commpression of output file failed")


if __name__ == "__main__":
    if len(sys.argv) == 1:
        PROG = os.path.basename(sys.argv[0])
        print('      ' + sys.argv[0] + ' [options]')
        print("     Help : ", PROG, " --help")
        print("        or : ", PROG, " -h")
        sys.exit(-1)
    else:
        USAGE = "usage: %prog [options] "
        PARSER = argparse.ArgumentParser(
            description="Extract shapefile records",
            formatter_class=argparse.RawTextHelpFormatter)
        PARSER.add_argument("-wd",
                            dest="path",
                            action="store",
                            help="working dir",
                            required=True)
        PARSER.add_argument("-inr",
                            dest="inr",
                            nargs='+',
                            help="input rasters list (classification ,"
                            ", validity and confidence)",
                            required=True)
        PARSER.add_argument("-nodata",
                            dest="nodata",
                            action="store",
                            help="nodata value of input raster(s)",
                            default=0,
                            type=int)
        PARSER.add_argument("-shape",
                            dest="shape",
                            action="store",
                            help="shapefiles path",
                            required=True)
        PARSER.add_argument("-output",
                            dest="output",
                            action="store",
                            help="vector output with statistics",
                            required=True)
        PARSER.add_argument("-gdal",
                            dest="gdal",
                            action="store",
                            help="gdal 2.2.4 binaries path "
                            "(problem of very small features "
                            "with lower gdal version)",
                            default="")
        PARSER.add_argument("-chunk",
                            dest="chunk",
                            action="store",
                            help="number of feature groups",
                            default=1,
                            type=int)
        PARSER.add_argument("-byarea",
                            action='store_true',
                            help="split vector features where sum of areas of "
                            "each split tends to be the same",
                            default=False)
        PARSER.add_argument("-params",
                            dest="params",
                            nargs='+',
                            help="1:rate 2:statsmaj 3:statsmaj "
                            "4:stats, 2:stats_cl \n"
                            "left side value corresponds to band "
                            "or raster number \n"
                            "right side value corresponds "
                            "to the type of statistics \n"
                            "stats: statistics of the band "
                            "(mean_b, std_b, max_b, min_b) \n"
                            "statsmaj: statistics of the band "
                            "for pixel corresponding to the majority class \n"
                            "(meanmaj, stdmaj, maxmaj, minmaj). "
                            "Need to provide a categorical raster "
                            "(rate statistics) \n"
                            "rate: rate of each pixel value "
                            "(classe names) \n"
                            "stats_cl: statistics of the band "
                            "for pixel corresponding to the required class \n"
                            "(mean_cl, std_cl, max_cl, min_cl). "
                            "Need to provide a categorical raster "
                            "(rate statistics) \n"
                            "val: value of corresponding pixel "
                            "(only for Point geom)",
                            default='1:stats')
        PARSER.add_argument("-classes",
                            dest="classes",
                            action="store",
                            help="",
                            default="")
        PARSER.add_argument("-buffer",
                            dest="buff",
                            action="store",
                            help="",
                            default="")
        PARSER.add_argument("-syscall",
                            action='store_true',
                            help="If True, use system call of gdalwrap binary",
                            default=False)
        PARSER.add_argument("-gdal_cache",
                            dest="cache",
                            action="store",
                            help="",
                            default="1000")
        PARSER.add_argument("-prod",
                            dest="prod",
                            action='store',
                            help="Choose type of production : carhab or oso",
                            default=False)
        PARSER.add_argument(
            "-higher_stats",
            action='store_true',
            help=
            "If True, compute majority order and confidence for each classes",
            default=False)

        args = PARSER.parse_args()
        compute_zonal_stats(args.path, args.inr, args.shape, args.params,
                            args.output, args.classes, args.buff, args.nodata,
                            args.gdal, args.chunk, args.byarea, args.cache,
                            args.syscall, args.prod, args.higher_stats)
