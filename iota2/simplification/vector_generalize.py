#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Vectorisation and simplification of a raster file with grass library
"""

import shutil
import sys
import os
import time
import logging

from iota2.Common import Utils
from iota2.Common import FileUtils as fut
from iota2.VectorTools import DeleteDuplicateGeometriesSqlite as ddg
from iota2.VectorTools import checkGeometryAreaThreshField as checkGeom
from iota2.VectorTools import vector_functions as vf
from iota2.VectorTools import AddFieldArea as afa
from iota2.VectorTools import DeleteField as df
from iota2.VectorTools import BufferOgr as buff

LOGGER = logging.getLogger("distributed.worker")


def init_grass(path, debuglvl, epsg="2154"):
    """
    Initialisation of Grass GIS in lambert 93.

    in :
        path : directory where create grassdata directory
        debuglvl : DEBUG level
    """
    global gscript

    # Grass folder Initialisation
    if not os.path.exists(os.path.join(path, "grassdata")):
        os.mkdir(os.path.join(path, "grassdata"))
    path_grassdata = os.path.join(path, "grassdata")

    # Init Grass environment

    gisbase = os.environ["GRASSDIR"]

    gisdb = os.path.join(path_grassdata)
    sys.path.append(os.path.join(gisbase, "etc", "python"))
    os.environ["GISBASE"] = gisbase

    # Overwrite and verbose parameters
    debugdic = {
        "critical": "0",
        "error": "0",
        "warning": "1",
        "info": "2",
        "debug": "3",
    }
    os.environ["GRASS_OVERWRITE"] = "1"
    os.environ["GRASS_VERBOSE"] = debugdic[debuglvl]

    # Grass functions import
    import grass.script.setup as gsetup
    import grass.script as gscript

    # Init Grass
    gsetup.init(gisbase, gisdb)

    # Delete existing location
    if os.path.exists(os.path.join(gisdb, "demolocation")):
        shutil.rmtree(os.path.join(gisdb, "demolocation"))

    # Create the location in Lambert 93
    gscript.run_command("g.proj",
                        flags="c",
                        epsg=epsg,
                        location="demolocation")

    # Create datas mapset
    if not os.path.exists(os.path.join(gisdb, "/demolocation/datas")):
        try:
            gscript.start_command(
                "g.mapset",
                flags="c",
                mapset="datas",
                location="demolocation",
                dbase=gisdb,
            )
        except IOError as err:
            raise Exception("Folder '%s' does not own to current user") % (
                gisdb) from err        

def get_driver_from_ext(vector):

    ext = os.path.splitext(vector)[1]
    if ext == ".shp":
        driver = "ESRI Shapefile"
    elif ext == ".sqlite":
        driver = "SQLite"
    elif ext == ".gpkg":
        driver = "GPKG"
    else:
        print("the vector file format is not handled")
        driver = None 

    return driver

def get_extension_from_driver(driver):
    
    if driver == "ESRI Shapefile":
        ext = ".shp"
    elif driver == "SQLite":
        ext = ".sqlite"
    elif driver == "GPKG":
        ext = ".gpkg"
    else:
        print("the driver does not exist. ESRI Shapefile driver will be used")
        ext = ".shp"

    return ext

                                         
def topological_polygonize(
    path,
    raster,
    angle,
    out="",
    outformat="SQLite",
    debulvl="info",
    epsg="2154",
    working_dir=None,
    clipfile="",
    clipfield="",
    clipvalue="",
    bufferclip="",
    logger=LOGGER,
):
    """
    Topological polygonization of a raster
    (interface to r.to.vect Grass function)
    """

    if working_dir:
        path = working_dir
    timeinit = time.time()

    extout = get_extension_from_driver(outformat)  
    
    if out == "":
        if outformat != "":
            out = os.path.splitext(raster)[0] + extout
        else:
            out = os.path.splitext(raster)[0] + ".shp"
    elif get_driver_from_ext(out) != outformat:
        print("output driver and output file extension are not compatible.  ESRI Shapefile driver will be used")
        out = os.path.splitext(raster)[0] + ".shp"

    if not os.path.exists(out) and os.path.exists(raster):
        print("Polygonize of raster file %s" % (os.path.basename(raster)))
        logger.info("Polygonize of raster file %s", os.path.basename(raster))
        # local environnement
        localenv = os.path.join(
            path, "tmp%s" % (os.path.basename(os.path.splitext(raster)[0])))
        if os.path.exists(localenv):
            shutil.rmtree(localenv)
        os.mkdir(localenv)

        init_grass(localenv, debulvl, epsg)

        # classification raster import
        gscript.run_command("r.in.gdal",
                            flags="e",
                            input=raster,
                            output="tile",
                            overwrite=True)
        gscript.run_command("r.null", map="tile@datas", setnull=0)

        timeimport = time.time()
        logger.info(" ".join([
            " : ".join(
                ["Classification raster import",
                 str(timeimport - timeinit)]),
            "seconds",
        ]))

        # manage grass region
        gscript.run_command("g.region", raster="tile")

        if angle:
            # vectorization with corners of pixel smoothing
            gscript.run_command(
                "r.to.vect",
                flags="sv",
                input="tile@datas",
                output="vectile",
                type="area",
                overwrite=True,
            )

        else:
            # vectorization without corners of pixel smoothing
            gscript.run_command(
                "r.to.vect",
                flags="v",
                input="tile@datas",
                output="vectile",
                type="area",
                overwrite=True,
            )

        timevect = time.time()
        logger.info(" ".join([
            " : ".join(
                ["Classification vectorization",
                 str(timevect - timeimport)]),
            "seconds",
        ]))

        # Export vector file
        if outformat == "ESRI Shapefile":
            outgrass = "ESRI_Shapefile"
        gscript.run_command("v.out.ogr",
                            input="vectile",
                            output=out,
                            format=outgrass)

        if clipfile:
            clippedvectorpath = os.path.dirname(out)
            idxchar = os.path.basename(out).rindex('_')
            tmpname = os.path.basename(out)[:idxchar]

            tmpout = os.path.join(localenv, "toclip%s"%(extout))
            vf.copy_vector(out, localenv, "toclip%s"%(extout))

            clip_vector_file(path,
                             tmpout,
                             clipfile,
                             clipfield,
                             clipvalue,
                             outpath=clippedvectorpath,
                             buffersize=bufferclip,
                             prefix=tmpname,
                             outformat=outformat)

        timeexp = time.time()
        logger.info(" ".join([
            " : ".join(["Vectorization exportation",
                        str(timeexp - timevect)]),
            "seconds",
        ]))

        shutil.rmtree(localenv)

        vf.checkValidGeom(out, outformat)
        vf.checkEmptyGeom(out, outformat)

    else:
        logger.info("Output vector %s file already exists",
                    os.path.basename(out))

    return out


def generalize_vector(
    path,
    vector,
    paramgene,
    method,
    mmu="",
    ncolumns="cat",
    out="",
    outformat="SQLite",
    debulvl="info",
    epsg="2154",
    working_dir=None,
    clipfile=None,
    snap=1,
    logger=LOGGER,
):
    """
    Topological generalization of a vector file
    (interface to v.generalize Grass function)
    """
    
    if working_dir:
        path = working_dir

    timeinit = time.time()
    
    # driver
    driver = get_driver_from_ext(vector)
    extout = get_extension_from_driver(outformat)  
    
    if out == "":
        if outformat != "":
            out = os.path.splitext(vector)[0] + "_%s%s" % (method, extout)
        else:
            out = os.path.splitext(vector)[0] + "_%s.shp" % (method)
    elif get_driver_from_ext(out) != outformat:
        print("output driver and output file extension are not compatible.  ESRI Shapefile driver will be used")
        out = os.path.splitext(vector)[0] + "_%s.shp" % (method)
        
    if not os.path.exists(out) and os.path.exists(vector):
        logger.info("Generalize (%s) of vector file %s", method,
                    os.path.basename(vector))

        # layer
        layer = vf.getLayerName(vector, driver)

        # local environnement
        localenv = os.path.join(path, "tmp%s" % (layer))
        if os.path.exists(localenv):
            shutil.rmtree(localenv)
        os.mkdir(localenv)
        init_grass(localenv, debulvl, epsg)
        
        # remove non "cat" fields
        for field in vf.getFields(vector, driver):
            if field != "cat":
                df.deleteField(vector, field)

        gscript.run_command(
            "v.in.ogr",
            flags="e",
            input=vector,
            output=layer,
            columns=["id", ncolumns],
            snap=snap,
            overwrite=True,
        )

        try:
            gscript.run_command(
                "v.generalize",
                input="%s@datas" % (layer),
                method=method,
                threshold="%s" % (paramgene),
                output="generalize%s"%(method),
                overwrite=True,
            )
        except ValueError as err:
            raise Exception(
                "Something goes wrong with generalization parameters "
                "(method '%s' or input data)" % (method)) from err

        if mmu != "":
            gscript.run_command(
                "v.clean",
                input="generalize%s"%(method),
                output="cleanarea",
                tool="rmarea",
                thres=mmu,
                type="area",
            )
            
            if outformat == "ESRI Shapefile":
                outgrass = "ESRI_Shapefile"            
            gscript.run_command("v.out.ogr",
                                input="cleanarea",
                                output=out,
                                format=outgrass)
        else:
            if outformat == "ESRI Shapefile":
                outgrass = "ESRI_Shapefile"
            gscript.run_command("v.out.ogr",
                                input="generalize%s"%(method),
                                output=out,
                                format=outgrass)

        timedouglas = time.time()
        logger.info(" ".join([
            " : ".join([
                "Douglas simplification and exportation",
                str(timedouglas - timeinit),
            ]),
            "seconds",
        ]))

        # clean geometries
        tmp = os.path.join(localenv, "tmp%s"%(extout))
        checkGeom.checkGeometryAreaThreshField(out, 1, 0, tmp, outformat)
        
        vf.copy_vector(tmp, os.path.dirname(out), os.path.basename(out))
        
        shutil.rmtree(localenv)

    else:
        logger.info("Output vector file already exists")

    return out


def get_field_values(shpfile, field):
    """Return list of unique values of a given field
    """
    classes = []
    dsshp = vf.openToRead(shpfile)
    layer = dsshp.GetLayer()
    for feature in layer:
        val = feature.GetField(field)
        if val not in classes:
            classes.append(val)

    return classes


def clip_vector_file(path,
                     vector,
                     clipfile,
                     clipfield="",
                     clipvalue="",
                     outpath="",
                     prefix="",
                     buffersize="",
                     working_dir=None,
                     outformat="",
                     logger=LOGGER):
    """
    Clip input vector file with another vector file
    (filtered on field and field value)
    """
    if working_dir:
        path = working_dir

    timeinit = time.time()
    
    extout = get_extension_from_driver(outformat)
    extin = os.path.splitext(vector)[1]
    driverin = vf.getDriver(vector)
    driverclip = vf.getDriver(clipfile)
    extclip = os.path.splitext(clipfile)[1]
    
    if outpath == "":
        out = os.path.join(os.path.dirname(vector),
                           "%s_%s%s" % (prefix, str(clipvalue), extout))
    else:
        out = os.path.join(outpath, "%s_%s%s" % (prefix, str(clipvalue), extout))

    epsgin = vf.get_vector_proj(vector, driverin)
    if vf.get_vector_proj(clipfile, driverclip) != epsgin:
        logger.error(
            "Land cover vector file and clip file projections are different "
            "please provide a clip file with same projection "
            "as Land cover file (EPSG = %s)", epsgin)
        sys.exit(-1)

    # clean input geometries
    tmp = os.path.join(path, "tmp%s"%(extin))
    checkGeom.checkGeometryAreaThreshField(vector, 1, 0, tmp, driverin)

    # copy file
    vf.copy_vector(tmp, os.path.dirname(vector), os.path.basename(vector))
        
    if not os.path.exists(out):

        if clipfile is not None:
            logger.info(
                "Clip vector file %s with %s (%s == %s)",
                os.path.basename(vector),
                os.path.basename(clipfile),
                clipfield,
                clipvalue,
            )

            # local environnement
            localenv = os.path.join(path, "tmp%s" % (str(clipvalue)))

            if os.path.exists(localenv):
                shutil.rmtree(localenv)
            os.mkdir(localenv)

            vf.copy_vector(clipfile, localenv)
            clipfile = os.path.join(localenv, os.path.basename(clipfile))
            
            if vf.getNbFeat(clipfile) != 1:
                clip = os.path.join(localenv, "clip%s"%(extclip))
                layer = vf.getFirstLayer(clipfile, driverclip)
                field_type = vf.getFieldType(os.path.join(localenv, clipfile),
                                             clipfield, driverclip)

                if field_type == str:
                    command = "ogr2ogr -f \"%s\" -sql \"SELECT * FROM %s "\
                        "WHERE %s = '%s'\" %s %s" % (driverclip,
                                                     layer,
                                                     clipfield,
                                                     clipvalue,
                                                     clip,
                                                     clipfile)
                    Utils.run(command)
                elif field_type in (int, float):
                    command = "ogr2ogr -f \"%s\" -sql \"SELECT * FROM %s "\
                        "WHERE %s = %s\" %s %s" % (driverclip,
                                                   layer,
                                                   clipfield,
                                                   clipvalue,
                                                   clip,
                                                   clipfile)
                    Utils.run(command)
                else:
                    raise Exception("Field type %s not handled" % (field_type))
            else:
                clip = os.path.join(path, clipfile)
                logger.info(
                    "'%s' shapefile has only one feature "
                    "which will used to clip data", clip)

            # buffer
            extclip = get_extension_from_driver(driverclip)
            if buffersize != "":
                clipbuff = clip.replace(extclip, "_buff%s"%(extclip))
                buff.bufferPoly(clip, clipbuff, buffersize, driverclip)
                clip = clipbuff
                logger.info("buffer of clip %s file is now ready", clip)

            # clip
            clipped = os.path.join(localenv, "clipped%s"%(extout))

            command = "ogr2ogr -f \"%s\" -skipfailures -select "\
                "cat -clipsrc %s %s %s" % (outformat, clip, clipped, vector)

            Utils.run(command)

        else:
            clipped = os.path.join(localenv, "merge%s"%(extout))

        timeclip = time.time()
        logger.info(" ".join([
            " : ".join(["Clip final shapefile",
                        str(timeclip - timeinit)]),
            "seconds",
        ]))

        # Delete duplicate geometries
        ddg.deleteDuplicateGeometriesSqlite(clipped, outformat)
        vf.copy_vector(clipped, localenv, "clean")

        timedupli = time.time()
        logger.info(" ".join([
            " : ".join(
                ["Delete duplicated geometries",
                 str(timedupli - timeclip)]),
            "seconds",
        ]))

        # Check geom
        vf.checkValidGeom(os.path.join(localenv, "clean%s"%(extout)), outformat)

        # Add Field Area (hectare)
        afa.addFieldArea(os.path.join(localenv, "clean%s"%(extout)), 10000)

        vf.copy_vector(os.path.join(localenv, "clean%s"%(extout)), os.path.dirname(out), os.path.basename(out))

        shutil.rmtree(localenv)

        timeclean = time.time()
        logger.info(" ".join([
            " : ".join([
                "Clean empty geometries and compute areas (ha)",
                str(timeclean - timedupli),
            ]),
            "seconds",
        ]))

    else:
        logger.info("Output vector file '%s' already exists", out)
