#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Search entities and neighbors (crown) entities according to an area (from tile)
"""

import sys
import os
import time
import shutil
import pickle
from itertools import chain
import logging

import gdal
import ogr
import numpy as np
from skimage.measure import regionprops
from skimage.future import graph

from iota2.Common import FileUtils as fut
from iota2.Common.rasterUtils import roi_raster

LOGGER = logging.getLogger("distributed.worker")


def cell_coords(feature, transform):
    """
    Generate pixel coordinates of square feature
    corresponding to raster transform.

    in :
        feature : feature from grid (osgeo format)
        transform : coordinates from raster
            transform[0] = Xmin;             // Upper Left X
            transform[1] = CellSize;         // W-E pixel size
            transform[2] = 0;                // Rotation, 0 if 'North Up'
            transform[3] = Ymax;             // Upper Left Y
            transform[4] = 0;                // Rotation, 0 if 'North Up'
            transform[5] = -CellSize;        // N-S pixel size
    out :
        cols_xmin : cols_xmin of cell
        cols_xmax : cols_xmax of cell
        cols_ymin : cols_ymin of cell
        cols_ymax : cols_ymax of cell
    """

    geom = feature.GetGeometryRef()
    ring = geom.GetGeometryRef(0)
    xpts = []
    ypts = []

    # get coordinates of cell corners
    for point in range(ring.GetPointCount()):
        xpts.append(ring.GetPoint(point)[0])
        ypts.append(ring.GetPoint(point)[1])

    # Convert geo coordinates in col / row of raster reference
    cols_xmin = int((min(xpts) - transform[0]) / transform[1])
    cols_xmax = int((max(xpts) - transform[0]) / transform[1])
    cols_ymin = int((max(ypts) - transform[3]) / transform[5])
    cols_ymax = int((min(ypts) - transform[3]) / transform[5])

    return cols_xmin, cols_xmax, cols_ymin, cols_ymax


def list_tiles_id(raster, feature):
    """
        entities ID list of tile

        in :
            raster : bi-band raster (classification - clump)
            outpath : out directory
            feature : feature of tile from shapefile

        out :
            tile_id : list with ID

    """

    # Classification and Clump opening
    datas_classif, _, _, _, transform_classif = fut.readRaster(raster, True, 1)
    datas_clump, _, _, _, _ = fut.readRaster(raster, True, 2)

    # Get coordinates of square feature corresponding to raster transform
    cols_xmin_decoup, cols_xmax_decoup, \
        cols_ymin_decoup, cols_ymax_decoup = cell_coords(
            feature, transform_classif)

    # subset raster data array based on feature coordinates
    tile_classif = datas_classif[cols_ymin_decoup:cols_ymax_decoup,
                                 cols_xmin_decoup:cols_xmax_decoup]
    tile_id_all = datas_clump[cols_ymin_decoup:cols_ymax_decoup,
                              cols_xmin_decoup:cols_xmax_decoup]

    del datas_classif, datas_clump

    # entities ID list of tile (except nodata and sea)
    tile_id = np.unique(
        np.where(((tile_classif > 1) & (tile_classif < 250)), tile_id_all,
                 0)).tolist()

    # delete 0 value
    tile_id = [int(x) for x in tile_id if x != 0]

    return tile_id


def tiles_ids_extent(ids_list, params, xsize, ysize, neighbors=False):
    """
        Compute geographical extent of tile entities

        in :
            ids_list : entities ID list
            params : Skimage regioprops output (entities individual extent)
            xsize : Cols number
            ysize : Rows number
            neighbors : neighbors entities computing (boolean)

        out :
            geographical extent of tile entities
    """
    subparams = {x: params[x] for x in ids_list}
    ids_extents = list(subparams.values())

    mincol = min([y for x, y, z, w in ids_extents])
    minrow = min([x for x, y, z, w in ids_extents])
    maxcol = max([w for x, y, z, w in ids_extents])
    maxrow = max([z for x, y, z, w in ids_extents])

    if not neighbors:
        if minrow > 0:
            minrow -= 1
        if mincol > 0:
            mincol -= 1
        if maxrow < ysize:
            maxrow += 1
        if maxcol < xsize:
            maxcol += 1

    return [minrow, mincol, maxrow, maxcol]


def search_crown_tile(inpath,
                      raster,
                      clump,
                      grid,
                      outpath,
                      ngrid=-1,
                      working_dir=None,
                      logger=LOGGER):
    """

        in :
            inpath : working directory with datas
            raster : name of raster
            ram : ram for otb application
            grid : grid name for serialisation
            out : output path
            ngrid : tile number

        out :
            raster with normelized name (tile_ngrid.tif)
    """

    if working_dir:
        inpath = working_dir

    begintime = time.time()

    if os.path.exists(os.path.join(outpath, "tile_%s.tif" % (ngrid))):
        logger.error("Output file '%s' already exists",
                     os.path.join(outpath, "tile_%s.tif" % (ngrid)))
        sys.exit()

    rasterfile = gdal.Open(clump, 0)
    clump_band = rasterfile.GetRasterBand(1)

    xsize = rasterfile.RasterXSize
    ysize = rasterfile.RasterYSize
    clump_array = clump_band.ReadAsArray()
    clump_props = regionprops(clump_array)
    rasterfile = clump_band = clump_array = None

    # Get extent of all image clumps
    params = {x.label: x.bbox for x in clump_props}

    timeextents = time.time()
    logger.info(" ".join([
        " : ".join([
            "Get extents of all entities",
            str(round(timeextents - begintime, 2))
        ]), "seconds"
    ]))

    # Open Grid file
    driver = ogr.GetDriverByName("ESRI Shapefile")
    shape = driver.Open(grid, 0)
    grid_layer = shape.GetLayer()

    all_tiles = False
    # for each tile
    for feature in grid_layer:

        if ngrid is None:
            ngrid = int(feature.GetField("FID"))
            all_tiles = True

        # get feature FID
        idtile = int(feature.GetField("FID"))

        # feature ID vs. requested tile (ngrid)
        if idtile == int(ngrid):
            logger.info("Tile : %s", idtile)

            # manage environment
            if not os.path.exists(os.path.join(inpath, str(ngrid))):
                os.mkdir(os.path.join(inpath, str(ngrid)))

            # entities ID list of tile
            tiles_ids = list_tiles_id(raster, feature)

            # if no entities in tile
            if len(tiles_ids) != 0:

                timentities = time.time()
                logger.info(" ".join([
                    " : ".join([
                        "Entities ID list of tile",
                        str(round(timentities - timeextents, 2))
                    ]), "seconds"
                ]))
                logger.info(" : ".join(
                    ["Entities number", str(len(tiles_ids))]))

                # tile entities bounding box
                list_extent = tiles_ids_extent(tiles_ids, params, xsize, ysize,
                                               False)
                timeextent = time.time()
                logger.info(" ".join([
                    " : ".join([
                        "Compute geographical extent of entities",
                        str(round(timeextent - timentities, 2))
                    ]), "seconds"
                ]))

                # Extract classification raster on tile entities extent
                raster_extract = os.path.join(inpath, str(ngrid),
                                              "tile_%s.tif" % (ngrid))
                if os.path.exists(raster_extract):
                    os.remove(raster_extract)

                xmin, ymax = fut.pix_to_geo(raster, list_extent[1],
                                            list_extent[0])
                xmax, ymin = fut.pix_to_geo(raster, list_extent[3],
                                            list_extent[2])

                roi_polygon = [[xmin, ymax], [xmax, ymax], [xmax, ymin],
                               [xmin, ymin], [xmin, ymax]]
                roi_raster(raster, raster_extract, roi_polygon)

                timeextract = time.time()
                logger.info(" ".join([
                    " : ".join([
                        "Extract classification raster "
                        "on tile entities extent",
                        str(round(timeextract - timeextent, 2))
                    ]), "seconds"
                ]))

                # Crown entities research
                dsrast = gdal.Open(raster_extract)
                idx = dsrast.ReadAsArray()[1]
                graph_connectivity = graph.RAG(idx.astype(int), connectivity=2)

                # Create connection duplicates
                listelt = []
                for elt in graph_connectivity.edges():
                    if elt[0] > 301 and elt[1] > 301:
                        listelt.append(elt)
                        listelt.append((elt[1], elt[0]))

                # group by tile entities id
                topo = dict(fut.sortByFirstElem(listelt))

                # Flat list and remove tile entities
                flatneighbors = set(
                    chain(*list(
                        dict((key, value) for key, value in list(topo.items())
                             if key in tiles_ids).values())))
                timecrownentities = time.time()
                logger.info(" ".join([
                    " : ".join([
                        "List crown entities",
                        str(round(timecrownentities - timeextract, 2))
                    ]), "seconds"
                ]))

                # Crown raster extraction
                if flatneighbors:
                    list_extentneighbors = tiles_ids_extent(
                        flatneighbors, params, xsize, ysize, False)
                    xmin, ymax = fut.pix_to_geo(raster,
                                                list_extentneighbors[1],
                                                list_extentneighbors[0])
                    xmax, ymin = fut.pix_to_geo(raster,
                                                list_extentneighbors[3],
                                                list_extentneighbors[2])

                    crown_raster = os.path.join(inpath, str(ngrid),
                                                "crown_%s.tif" % (ngrid))
                    if os.path.exists(crown_raster):
                        os.remove(crown_raster)

                    roi_polygon = [[xmin, ymax], [xmax, ymax], [xmax, ymin],
                                   [xmin, ymin], [xmin, ymax]]
                    roi_raster(raster, crown_raster, roi_polygon)

                    shutil.copy(
                        crown_raster,
                        os.path.join(outpath, "crown_%s.tif" % (ngrid)))

                else:
                    shutil.copy(
                        raster, os.path.join(outpath,
                                             "crown_%s.tif" % (ngrid)))

                timeextractcrown = time.time()
                logger.info(" ".join([
                    " : ".join([
                        "Extract classification raster "
                        "on crown entities extent",
                        str(round(timeextractcrown - timecrownentities, 2))
                    ]), "seconds"
                ]))

                with open(
                        os.path.join(inpath, str(ngrid),
                                     "listid_%s" % (ngrid)), 'wb') as pickfile:
                    pickle.dump([tiles_ids + list(flatneighbors)], pickfile)

                shutil.copy(
                    os.path.join(inpath, str(ngrid), "listid_%s" % (ngrid)),
                    os.path.join(outpath, "listid_%s" % (ngrid)))
                shutil.rmtree(os.path.join(inpath, str(ngrid)),
                              ignore_errors=True)

            if all_tiles:
                ngrid += 1
