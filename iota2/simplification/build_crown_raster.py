#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Create a raster of entities and neighbors entities according to an area (from tile).
"""

import os
import shutil
import pickle
import logging
import numpy as np

from iota2.Common import FileUtils as fut
from iota2.Common import Utils

LOGGER = logging.getLogger("distributed.worker")


def manage_blocks(crowns_path,
                  tilenumber,
                  blocksize,
                  inpath,
                  outpath,
                  working_dir=None,
                  logger=LOGGER):
    """
    merge raster files
    """
    if working_dir:
        inpath = working_dir

    tomerge = []
    for paths, _, files in os.walk(crowns_path):
        for crown in files:
            if "crown_" + str(
                    tilenumber) + ".tif" in crown and "aux.xml" not in crown:
                shutil.copy(os.path.join(paths, crown), inpath)
                crown_source = fut.readRaster(os.path.join(inpath, crown))
                row, col = int(crown_source[1]), int(crown_source[0])
                crown_source = None
                intervalx = np.arange(0, col, blocksize)
                intervaly = np.arange(0, row, blocksize)

                with open(
                        os.path.join(crowns_path, "listid_%s" % (tilenumber)),
                        'rb') as fpickle:
                    listid = pickle.load(fpickle)

                nbblock = 0
                for intery in intervaly:
                    for interx in intervalx:
                        outputtif = os.path.join(
                            inpath,
                            "crown%sblock%s.tif" % (tilenumber, nbblock))
                        xmin, ymin = fut.pix_to_geo(
                            os.path.join(inpath, crown), interx, intery)
                        xmax, ymax = fut.pix_to_geo(
                            os.path.join(inpath, crown), interx + blocksize,
                            intery + blocksize)

                        cmd = "gdalwarp -overwrite "\
                            "-multi --config GDAL_CACHEMAX 9000 "\
                            "-wm 9000 -wo NUM_THREADS=ALL_CPUS -te " + str(
                                xmin) + " " + str(ymax) + " " + str(
                                    xmax) + " " + str(ymin)
                        cmd = cmd + " -ot UInt32 " + os.path.join(
                            inpath, crown) + " " + outputtif

                        Utils.run(cmd)

                        idx_info = fut.readRaster(outputtif, True, 2)
                        idx = idx_info[0]
                        labels_info = fut.readRaster(outputtif, True, 1)
                        labels = labels_info[0]
                        spx, spy = labels_info[4][1], labels_info[4][1]
                        masknd = np.isin(idx, listid[0])
                        masked_labels = labels * masknd
                        out_raster = os.path.join(
                            inpath, "crown%sblock%s_masked.tif" %
                            (tilenumber, nbblock))
                        fut.arraytoRaster(masked_labels, out_raster, outputtif)

                        if os.path.exists(out_raster):
                            tomerge.append(out_raster)

                        os.remove(outputtif)

                        nbblock += 1

                # Mosaic
                out = os.path.join(inpath, "tile_%s.tif" % (tilenumber))
                if tomerge:
                    fut.assembleTile_Merge(tomerge, (spx, spy), out, ot="Byte")
                    shutil.copy(out, outpath)

                    # remove tmp files
                    os.remove(os.path.join(inpath, crown))
                    os.remove(out)
                    os.remove(os.path.join(paths, crown))
                    os.remove(
                        os.path.join(crowns_path, "listid_%s" % (tilenumber)))
                    for fileblock in tomerge:
                        os.remove(fileblock)

                    logger.info('Crown raster of tile %s is now ready',
                                tilenumber)
                else:
                    logger.info('No crown raster of tile %s', tilenumber)
