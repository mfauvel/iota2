#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import sys
import argparse
import math

from iota2.Common import Utils
from iota2.Common import FileUtils as fu
from iota2.VectorTools import vector_functions as vf
from iota2.VectorTools import splitByArea as sba


def split_vector_in_features(vectorpath,
                             outputPath,
                             chunk=1,
                             byarea=False,
                             getfiles=False,
                             sqlite3_query_limit=1000):
    """Split FID list of a list of vector files in equal groups:

    Parameters
    ----------
    vectorpath : string
        vector file or folder of vector files

    outputPath : string
        path to chunked vector files

    chunk : integer
        number of FID groups

    byarea : boolean
        split vector features where sum
        of areas of each split tends to be the same

    Return
    ----------
    list of FID list and vector file

    """

    if os.path.isdir(vectorpath):
        listvectors = fu.FileSearch_AND(vectorpath, True, "shp")
    else:
        listvectors = [vectorpath]

    params = []

    for vect in listvectors:

        listfid = vf.getFidList(vect)
        if byarea:
            vectorgeomtype = vf.getGeomType(vect)

            if vectorgeomtype in (3, 6, 1003, 1006):
                listid = sba.getFidArea(vect)
            else:
                raise Exception("Geometry type is not adapted"
                                "to compute features areas")
            statsclasses = sba.getFeaturesFolds(listid, chunk)
            listfid = []
            for elt in statsclasses[0][1]:
                listfid.append([x[0] for x in elt])
        else:
            listfid = [listfid[i::chunk] for i in range(chunk)]
            listfid = list(filter(None, listfid))

        if len(listfid) == 1:
            outfile = os.path.splitext(
                os.path.basename(vect))[0] + "_stats.shp"
            params.append((vect, listfid[0], os.path.join(outputPath,
                                                          outfile)))
        else:
            for idchunk, fidlist in enumerate(listfid):
                outfile = os.path.splitext(
                    os.path.basename(vect))[0] + '_chk' + str(idchunk) + ".shp"
                params.append(
                    (vect, fidlist, os.path.join(outputPath, outfile)))

    if getfiles:
        for elt in params:
            lyrname = os.path.splitext(os.path.basename(elt[0]))[0]
            nb_sub_split = int(math.ceil(len(elt[1]) / sqlite3_query_limit))
            sub_fids = fu.splitList(elt[1], nb_sub_split)
            sub_fids_clause = []
            for fids in sub_fids:
                sub_fids_clause.append("(FID in ({}))".format(", ".join(
                    map(str, fids))))
            fid_clause = " AND ".join(sub_fids_clause)
            sql_clause = "SELECT * FROM %s WHERE %s" % (lyrname, fid_clause)

            Utils.run('ogr2ogr -f "ESRI shapefile" -sql "%s" %s %s' %
                      (sql_clause, elt[2], elt[0]))

    return params


if __name__ == "__main__":
    if len(sys.argv) == 1:
        PROG = os.path.basename(sys.argv[0])
        print('      ' + sys.argv[0] + ' [options]')
        print("     Help : ", PROG, " --help")
        print("        or : ", PROG, " -h")
        sys.exit(-1)
    else:
        USAGE = "usage: %prog [options] "
        PARSER = argparse.ArgumentParser(
            description="Split Vector file in n chunks",
            formatter_class=argparse.RawTextHelpFormatter)
        PARSER.add_argument("-invect",
                            dest="invect",
                            action="store",
                            help="Input vector file",
                            required=True)
        PARSER.add_argument("-outpath",
                            dest="outpath",
                            action="store",
                            help="Output path to store chunk vector file",
                            required=False)
        PARSER.add_argument("-chunk",
                            dest="chunk",
                            action="store",
                            help="chunks number",
                            required=True,
                            type=int)
        PARSER.add_argument("-byarea",
                            action='store_true',
                            help="If True, "
                            "split vector features where sum of areas of "
                            "each split tends to be the same",
                            default=False)
        PARSER.add_argument("-write",
                            action='store_true',
                            help="If True, "
                            "write output vector splits",
                            default=False)

        args = PARSER.parse_args()
        split_vector_in_features(args.invect, args.outpath, args.chunk,
                                 args.byarea, args.write)
